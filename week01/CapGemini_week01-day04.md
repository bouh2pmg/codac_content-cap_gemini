---
module:			Cap Gemini
title:			day 04
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d04
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn in your main function unless it has been explicitly asked for.)

#warn(You are only allowed to use the write function to do the following exercises.)

#hint(Remember that your exercises should never crash. To prevent that\, you should always test every functionality case (like empty strings, for example).)

#hint(Remember\, it is always better to create your repository at the beginning of the day and to turn in your work on a regular basis.
Do not forget the permissions rights!)





#newpage


#Exercise 00 - my_init #hfill 1pt

**Turn in**: pool\_c\_d04/ex\_00/ex\_00.c#br

Write a "my_init" function that takes an integer pointer as parameter. This function must not return anything but the value of the variable passed as parameter must be set to 42.#br

It must be prototyped as follows:

	void my_init(int *);

#terminal(cat main.c

	\#include <stdio.h>

	void		my_init(int *);
	int			main()
	{
		int 	i;

		i = 0;
		my_init(&i);
		printf("%d#cr", i);
	}
#prompt cc *.c -o ex00
#prompt ./ex00
42)





#newpage

#Exercise 01 - my_swap #hfill 3pts

**Turn in**: pool\_c\_d04/ex\_01/ex\_01.c#br

Write a "my_swap" function that takes two integer pointers as parameters and swaps the value of those two variables. It does not return anything.#br

It must be prototyped as follows:

	void my_swap(int *, int *);

#terminal(cat main.c

	\#include <stdio.h>

	void		my_swap(int *, int *);
	int			main()
	{
		int 	nb1 = 42;
		int 	nb2 = 21;

		printf("%d %d#cr", nb1, nb2);
		my_swap(\&nb1, _&nb2);
		printf("%d %d#cr", nb1, nb2);
	}
#prompt cc *.c -o ex01
#prompt ./ex01
42 24
24 42)


#Exercise 02 - my_putstr #hfill 3pts

**Turn in**: pool\_c\_d04/ex\_02/ex\_02.c#br

Write a "my_putstr"  function that takes a string as parameter and displays it on the standard output.#br

It must be prototyped as follows:

	void my_putstr(char *str);





#Exercise 03 - my_strlen #hfill 2pts

**Turn in**: pool\_c\_d04/ex\_03/ex\_03.c#br

Write a "my_strlen" function that takes a string as parameter and returns its size.
For example, my_strlen("Geckos") must return 6.#br

It must be prototyped as follows:

	int my_strlen(char *str);

#terminal(cat main.c

	int		my_strlen(char *str);
	int			main()
	{
		printf(%d, my_strlen("Geckos"));
	}
#prompt cc *.c -o ex03
#prompt ./ex03
6)

#hint(A string always ends with a "\\\\0".)





#Exercise 04 - my_sort_int_tab #hfill 3pts

**Turn in**: pool\_c\_d04/ex\_04/ex\_04.c#br

Write a "my\_sort\_int\_tab" function that takes an integer table as its first parameter and the size of the table as a second parameter.
The function must sort the table in ascending order.#br

It must be prototyped as follows:

	void my_sort_int_tab(int *tab, int size);





#Exercise 05 - my_getnbr #hfill 6pts

**Turn in**: pool\_c\_d04/ex\_05/ex\_05.c#br

Write a "my_getnbr" function which takes as parameter a string that represents a number in base 10 ("0123456789") and returns an integer.#br

It must be prototyped as follows:

	int my_getnbr(char *str);

#terminal(cat main.c

	\#incude <stdio.h>

	void		my_getnbr(char *str);
	void		print_my_getnbr(char *str)
	{
		printf("%s => %d\\n", my_getnbr(str));
	}
	int			main()
	{
		print_my_getnbr("--42");
		print_my_getnbr("+---+--++---+---+---+-42");
		print_my_getnbr("42a43");
		print_my_getnbr("-a-a42");
		print_my_getnbr("1100000000000000000042");
		print_my_getnbr("-1000000000000000000042");
		return (0);
	}

#prompt cc *.c -o ex05
#prompt ./ex05
--42 => 42
+---+--++---+---+---+-42 => 42
42a43 => 42
-a-a42 => 42
1100000000000000000042 => 0
-1000000000000000000042 => 0)

#hint(The last 2 examples do not fit an integer...)




#Exercise 06 - my_show_array #hfill 3pts

**Turn in**: pool\_c\_d04/ex\_06/ex\_06.c#br

Write a "my\_show\_array" function that takes an array of strings as parameter and displays the strings from the shortest to the longest. 
The function also takes the size of the array as a second parameter.#br

If multiple strings have the same length, order them from the one the greatest ASCII value to the least. If there are still some equalities, you must display them according to their indexes.
For example, "you" and "uoy" are perfectly equal, so if tab[0] = "you", you must display "you" before "uoy".#br

Every string must be followed by a "#cr".

#warn(Remember that there must always be a null terminated byte in a string.)

It must be prototyped as follows:

	void my_show_array(char **tab, int size);

#newpage

Here is how you could test your function:

#terminal(cat main.c

	void		my_show_array(char **tab, int size);
	int 		main()
	{
		char 	tab[3][15] = 
		{
			"not prepared !"\,
			"You"\,
			"are"
		};
		char	tab2[5][5] =
		{
			"ab"\,
			"ba"\,
			"ca"\,
			"Da"\,
			"aE"
		};
		char	*f[3] =
		{
			tab[0]\, tab[1]\, tab[2]
		};
		char	*f2[5] =
		{
			tab2[0]\, tab2[1]\, tab2[2]\, tab2[3]\, tab2[4]
		};

		my_show_array(f, 3);
		printf("____#cr")
		my_show_array(f2, 5);
		return (0);
	}

#prompt cc *.c -o ex_06
#prompt ./ex_06
You
are
not prepared!
____
ca
ab
ba
aE
Da)