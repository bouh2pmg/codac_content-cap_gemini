---
module:			Cap Gemini
title:			day 02
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d02
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(It is a good practice to create the turn in directory at the beginning of the day and turn in your work at regular interval.)





#Exercise 00 - Turn in
<!-- !!! "rendu" directory  -->
If you haven't already done so, go to your home and create a directory called "Rendu". 
Go inside and create today's turn-in directory by using blih.
Now, using git, clone it.

#hint(mkdir; cd)

#hint(Do not forget the permissions in your repository!)





#Exercise 01 - ls #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_01/my_ls.sh#br

Create a "my_ls.sh" file.
This file must be a bash script and when executed, it should list the current directory's files by using the "ls" command. 
You must pass some arguments to ls in order for your script to show the hidden files in addition to regular files, and everything should be in long format.

#terminal(touch test
#prompt bash my_ls.sh
total 8
#array(l r r r r r r l, 

drwxrwxr-x #next 2  #next user #next group #next 4096#next Jun 18 #next 15:07 #next .
drwxr-xr-x #next 35 #next user #next group #next 4096 #next Jun 18 #next 15:07 #next . 
-rw-rw-r-- #next 1  #next user #next group #next 7 	  #next Jun 18 #next 15:07 #next my\\_ls.sh
-rw-rw-r-- #next 1  #next user #next group #next 0    #next Jun 18 #next 15:07 #next test)
)

#hint(man ls)




#Exercise 02 - Permissions #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_02/my_ls.sh#br

Copy your "my_ls.sh" file and add the necessary permission(s) to it.
You must add something to your file so that it can be run like this:

#terminal(./my_ls.sh)

#hint([shebang](https://lmddgtfy.net/?q=shebang%20unix) ; man chmod)





#Exercise 03 - Hello world #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_03/hello.sh#br

Write a bash script that displays "Hello world!", followed by a line break when run.

#terminal(bash hello.sh
Hello world!)

#hint(man echo)





#Exercise 04 - Cat #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_04/my_cat.sh#br

Write a my_cat.sh bash script that shows the content of a file called "coding" when run.

#terminal(echo Hello > coding
#prompt bash my_cat.sh
Hello)

#hint(man cat)




#Exercise 05 - Cat 2 #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_05/my_cat.sh#br

Copy your file my_cat.sh and modify it so it takes the file to be shown as its first parameter.

#terminal(echo Hello > test
#prompt bash my_cat.sh test
Hello)





#Exercise 06 - Wc #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_06/my_wc.sh#br

Write a my_wc.sh bash script that takes a file in parameter. 
This script must show the number of words in the file, followed by the file's name.  

#terminal(echo "Welcome to this new day of pool"> test
#prompt bash my_wc.sh test
7 test)

#hint(man wc)





#Exercise 07 - Grep #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_07/my_grep.sh#br

Write a my_grep.sh bash script that takes 2 parameters: a word and a file.
The script must show every line in the file that contains the word passed as parameter.

#terminal(bash my_grep.sh test file
Hello\, this is a test.
Probably the coolest test ever.)

#hint(man grep)





#Exercise 08 - Sed #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_08/replace.sh#br

Write a replace.sh bash script that takes 3 parameters: a file and two words. 
The script must replace all  of the first word's occurrences with the second word in the file.

#terminal(cat test
Hello to my friends.
#prompt bash replace.sh test friends buddies
#prompt cat test
Hello to my buddies.)

#hint(man sed)




#newpage

#Exercise 09 - Let's create directories #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_09/createDir.sh#br

Write a bash script that automaticallys create today's exercises directory.
If a directory already exists, no error should appear and the missing ones must be created.

#terminal(ls
#prompt ./createDir.sh 5
#prompt ls
ex_01/ 
ex_02/
ex_03/ 
ex_04/
ex_05/
#prompt ./createDir.sh 15
#prompt ls
ex_01/ 
ex_02/
ex_03/ 
ex_04/
ex_05/
ex_06/ 
ex_07/
ex_08/ 
ex_09/
ex_10/
ex_11/ 
ex_12/
ex_13/ 
ex_14/
ex_15/)





#Exercise 10 - Mr Clean #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_10/mr_clean.sh#br

Write a mr_clean bash script that "cleans" your current depository. "clean" means deleting all files ending with a "~" (emacs temporary files).

#warn(The script must be recursive. It must clean your current directory and all sub-directories.)

#terminal(ls -R
.:
folder1/ folder2/ folder3/ test0 test0~#br

./folder1:
test1 test1~#br

./folder2:
test2 test2~#br

./folder3:
test3 test3~ test4
#prompt ../mr_clean.sh
#prompt ls -R
.:
folder1/ folder2/ folder3/ test0#br

./folder1:
test1#br

./folder2:
test2#br

./folder3:
test3 test4)





#newpage

Now that your day is done, check all of your exercises at least 5 times.
After that you can combine yesterday's bonus and today's ex08.#br

If you still have some free time, you should read the C norm (available on the intranet) that you will have to follow starting from tomorrow.#br

We also highly recommend watching the "C in 29 Minutes" videos before tomorrow.  





#Bonus 01 #hfill 1pt
**Turn in**: pool\_c\_d02/ex\_11/get\_user\_default.sh
**Restriction**: use of the command "sed"#br

Write a shell script that takes a username as its first parameter and displays its home and starting shell like in the example.
If any error occurs, your script must return an error code and display the message, "Error.", followed by a newline on the error output.

#terminal(./get_user_default.sh
Error.
#prompt ./get_user_default.sh root
Home Directory: /root
Default Shell: /bin/bash
#prompt ./get_user_default.sh notExistingUser
Error.)





#Bonus 02 #hfill 2pts
**Turn in**: pool\_c\_d02/ex\_12/my_push.sh#br

Write a shell script "my_push.sh" that adds files to a commit, commits the modifications and then pushes the commit on the server.
This script must take at least one parameter which is the commit message.
If no other parameters are given, you must add every files to the commit. 
Every other paramters after the first one are the files we want to add to the commit.
If no paramters are given, you must display "No commit message, no add and no push.".