---
module:			Cap Gemini
title:			day 03
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d03
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn in your main function unless it has been explicitly asked for.)

#warn(You are only allowed to use the write function to do the following exercises.)

#hint(Remember\, it is always better to create your repository at the beginning of the day and to turn-in your work on a regular basis.
Do not forget the permissions rights!)





#newpage

#Exercise 01 - Alpha #hfill 2pts

**Turn in**: pool\_c\_d03/ex\_01/alpha.c #br

Write a function that displays the alphabet in uppercase letters on a single line, in ascending order starting with the letter "A" and followed by a "#cr". #br

The function must be prototyped as follows:

	void alpha();

**Example**:
#terminal(cat main.c

	void	alpha();
	void	main()
	{
		alpha();
		return (0);
	}

#prompt cc *.c  -o ex01
#prompt ./ex01
ABCDEFGHIJKLMNOPQRSTUVWXYZ)

#hint(man 2 write; man ascii)

#hint(What is a prototype? Why is it used here?
Oh... And also... All developers are lazy... No exceptions!)





#Exercise 02 - Reverse Alpha #hfill 1pt

**Turn in**: pool\_c\_d03/ex\_02/revalpha.c#br

Write a function that displays the alphabet in uppercase letters on a single line, in descending order starting with the letter "Z" and followed by a "#cr".#br

The function must be prototyped as follows:

	void revalpha();

**Example**:
#terminal(cc *.c  -o ex02
#prompt ./ex02
ZYXWVUTSRQPONMLKJIHGFEDCBA)





#Exercise 03 - True loop #hfill 2pts

**Turn in**: pool\_c\_d03/ex\_03/true_loop.c#br

Write a __my_true_loop__ function that takes a number as parameter, as many "+" as this parameter, and is followed by a "#cr".#br

The function must be prototyped as follows:

	void my_true_loop(unsigned int n);


**Example**:
#terminal(cat main.c

	void 	my_true_loop(unsigned int);
	int 	main()
	{
		my_true_loop(5);
		return (0);
	}
#prompt cc *.c -o ex03
#prompt ./ex03
+++++)





#Exercise 04 - Conditions #hfill 2pts

**Turn in**: pool\_c\_d03/ex\_04/conditions.c#br

Write a __conditions__ function that takes a number as parameter and prints "+" if the number is greater than 0, "-" if the number is less than 0, and "0" otherwise.#br

The function mustbe prototyped as follows:

	void conditions(int n);


#terminal(cat main.c

	void 	conditions(int n);
	int 	main()
	{ 
		conditions(-564);
		conditions(564);
		conditions(0);
		return (0);
	}
#prompt cc *.c -o ex04
#prompt ./ex04
-+0#prompt)





#Exercise 05 - my_aff_comb #hfill 3pts
**Turn in**: pool\_c\_d03/ex\_05/my\_aff\_comb.c#br

Write a function that displays all the different combinations of three different digits in the ascending order.#br

The function shall be prototyped as follows:

	void	my_aff_comb();

**Example**:
#terminal(cc *.c -o ex05
#prompt ./ex05
012\, 013\, 014\, 015\, 016\, 017\, 018\, 019\, 023\, ...\, 789#prompt)

#warn(In this example\, all the results are not shown but\, of course\, you must display all of the different combinations.)

#hint(987 is missing because 789 is already there.#br 999 is missing because that number's digits are all the same.)





#Exercise 06 - my_putnbr #hfill 5pts

**Turn in**: pool\_c\_d03/ex\_06/my_putnbr.c#br

Write a function "my_putnbr" that takes a number as parameter and prints it on the standard output.#br

The function must be prototyped as follows:

	void my_putnbr(int n);

**Example**:
#terminal(cat main.c

	void 	my_putnbr(int n);
	int 	main()
	{
		my_putnbr(42);
		my_putnbr(-42);
		return (0);
	}
#prompt cc *.c -o ex06
./ex06
42-42#prompt)





#Exercise 07 - my_aff_comb2 #hfill 2pts

**Turn in**: pool\_c\_d03/ex\_07/my\_aff\_comb2.c#br

Write a function that displays all of the different combinations of two numbers between 0 and 99, in ascending order.#br

The function must be prototyped as follows:

	void	my_aff_comb2();

**Example**:
#terminal(cc *.c -o ex07
#prompt ./ex07
00 01\, 00 02\, 00 03\, 00 04\, 00 05\, ...\, 01 99\, 02 03\, ...\, 98 99#prompt)





#Exercise 08 - my_aff_combn #hfill 3pts

**Turn in**: pool\_c\_d03/ex\_08/my\_aff\_combn.c#br

Write a function that displays all of the different combinations of n digits in ascending order, like in the following example.#br

The function must be prototyped as follows:

	void	my_aff_combn(int n);

**Example**:
#terminal(cat main.c

	void 	my_aff_combn(int n);
	int 	main()
	{
		my_aff_combn(2);
		return (0);
	}
#prompt cc *.c -o ex08
#prompt ./ex08
01\, 02\, 03\, 04\, 05\, ...\, 79\, 89)