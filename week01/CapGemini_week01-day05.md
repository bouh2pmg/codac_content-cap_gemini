---
module:			Cap Gemini
title:			day 05
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d05
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process\, and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn-in your main function unless it has been explicitly asked for.)

#warn(You are only allowed to use the write function to do the following exercises.)

#hint(Remember that your exercises should never crash. To prevent that\, you should always test every functionality case (like empty strings\, for example).)

#hint(Remember\, it is always better to create your repository at the beginning of the day and to turn-in your work on a regular basis.
Do not forget the permissions rights!)





#newpage

#Exercise 01 – array_sum #hfill 2pts

**Turn in**: pool\_c\_d05/ex\_01/ex_01.c#br

Write an __array_sum__ function that takes an integer array and its size as parameter, and returns the sum of the integers contained in the array.#br

It must be prototyped as follows:

	int array_sum(int *tab, int size);

#terminal(cat main.c

	\#include <stdio.h>

	int 	array_sum(int *tab\, int size);
	int 	main()
	{
		int tab[3] = {3\, 4\, 5};
		printf("%d\\n"\, array_sum(tab\, 3));
		return (0);
	}

#prompt cc *.c -o ex01
#prompt ./ex01
12)



#newpage

#Exercise 02 – my_revstr #hfill 3pts

**Turn in**: pool\_c\_d05/ex\_02/ex_02.c#br

Write a __my_revstr__ function that takes a string as parameter and reverses it.#br

It must be prototyped as follows:

	 char *my_revstr(char *);

#terminal(cat main.c

	\#include <stdio.h>

	int 		my_revstr(char *str);
	int 		main()
	{
		char 	str[6] =  "Hello";
		printf("%s\\n"\, my_revstr(str));
		return (0);
	}

#prompt cc *.c -o ex02
#prompt ./ex02
olleH)





#newpage

#Exercise 03 – my_strcmp #hfill 4pts

**Turn in**: pool\_c\_d05/ex\_03/ex_03.c#br

Write a __my_strcmp__ function that takes two strings as parameters and reproduces the behavior of the "strcmp" function. 

#warn(Some __strcmp__ functions only return (-1, 0 or 1). That is not the behavior that we are expecting.)

It must be prototyped as follows:

	 int my_strcmp(char *str1, char *str2);


#hint(man strcmp)





#Exercise 04 – my_strupcase #hfill 2pts

**Turn in**: pool\_c\_d05/ex\_04/ex_04.c#br

Write a __my_strupcase__ function that takes a string as parameter and returns the string with every letter in uppercase.#br

It must be prototyped as follows:

	 char *my_strupcase(char *);





#Exercise 05 – my\_str\_isalpha #hfill 2pts

**Turn in**: pool\_c\_d05/ex\_05/ex_05.c#br

Write a __my\_str\_isalpha__ function that takes a string as parameter and returns 1 if the string passed as parameter contains only alphabetical characters. Otherwise it should return 0.#br

It must be prototyped as follows:

	 int my_str_isalpha(char *);

#terminal(cat main.c

	\#include <stdio.h>

	int 		my_str_isalpha(char *str);
	int 		main()
	{
		char 	str1[6] =  "Hello";
		char 	str2[7] =  "Pony42";
		char 	str3[3] =  "42";

		printf("%d\\n"\, my_str_isalpha(str1));
		printf("%d\\n"\, my_str_isalpha(str2));
		printf("%d\\n"\, my_str_isalpha(str3));
		return (0);
	}

#prompt cc *.c -o ex05
#prompt ./ex05
1
0
0)





#newpage

#Exercise 06 – my_strstr #hfill 3pts

**Turn in**: pool\_c\_d05/ex\_06/ex_06.c#br

Write a __my_strstr__ function that reproduces the behavior of the "strstr" function.

It must be prototyped as follows:

	int my_strstr(char *, char *);

#hint(man strstr)





#Exercise 07 – my_putnbr #hfill 1pt

**Turn in**: pool\_c\_d05/ex\_07/ex_07.c#br

Write a __my_putnbr__ function that takes a number as parameter and displays it on the standard output.#br

It must be prototyped as follows:

	 int my_putnbr(int);


#terminal(cat main.c

	int 		my_putnbr(int n);
	int 		main()
	{
		my_putnbr(42);
		my_putnbr(-42);
		return (0);
	}

#prompt cc *.c -o ex07
#prompt ./ex07
42-42#prompt)





#Exercise 08 – my_putnbr\_base #hfill 3pts

**Turn in**: pool\_c\_d05/ex\_08/ex_08.c#br

Write a __my\_putnbr\_base__ function that takes a number as it first parameter and a string as it second parameter. Its second parameter represents a base and displays the number in the given base. 
The function must always return the number passed as parameter.#br

It must be prototyped as follows:

	 int my_putnbr_base(int nbr, char *base);

#terminal(cat main.c

	int 		my_putnbr_base(int n, char *base);
	int 		main()
	{
		my_putnbr_base(42, "0123456789");
		my_putchar('#cr');
		my_putnbr_base(42, "0123456789ABCDEF");
		my_putchar('#cr');
		my_putnbr_base(42, "9876543210");
		my_putchar('#cr');
		my_putnbr_base(42, "+-*/%()-_=");
		return (0);
	}

#prompt cc *.c -o ex09
#prompt ./ex08
42
2A
57
%\\*)