---
module:			Cap Gemini
title:			day 01
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool\_c\_d01
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#Exercise 01 - Intranet
Connect to the [Epitech Intranet](https://intra.epitech.eu) using the login and password given to you, then sign up for the C Pool's activities.
Now connect to [your mailbox](www.microsoftonline.com) and read the confirmation e-mails you received.

#warn(If you do not sign up on the intranet you will NOT be graded.)




#newpage

#Exercise 02 - Console
All of this pool's exercises will take place in the console (or "terminal").
A terminal does not have any graphical interface.
Everything is done from command line.
The __man__ command, for example, can provide more information about other commands.#br

Use the __ls__ and __cd__ commands to show the files that are in the current directory and to move from one directory to another.

#hint(man ls; help cd)





#Exercise 03 - Create directory
<!-- !!! "rendu" directory  -->
To help get you organized, you are going to create a directory named __Rendu__.
The idea is to put all of your projects that you will be working on in that repository so that it is easier for us, and for you, to find your work.






#Exercise 04 - Git
To turn in your exercises, you must use the "git" version control software.
To do this, you must first install it on your computer.

#hint(man apt-get; [git doc](http://git-scm.com/doc))





#Exercise 05 - Blih
To turn in your exercises, you also need the blih script.
It will let you create your "turn in" directories.
You can find blih at [this link](https://intra-bocal.epitech.eu/index.php?pgid=docvisu&m_doc_id=162).
If you encounter any problems, you can also find blih [here](http://pkg.bocal.org/pub/blih/).
Take the latest version available.

#hint(The file is compressed. You can decompress it with the tar or gunzip commands.)

The "mv" command moves and renames files.
Move blih.py in the /usr/bin folder and rename it in "blih".

#hint(man mv)

#warn(If the "blih" command does not work and shows an error like "python3.3: No such file or directory"\, you will have to change the shebang in order to specify the correct path to the python binary (version 3.3 or more). 
        _python --version_
        [shebang](https://lmddgtfy.net/?q=shebang%20unix))





#Exercise 06 - IDE
Create a file named __test__ in your home directory and write "Les Entrechats" inside. You can display the content of the file with the cat command. 
There are many ways to write in a file, but this time, you must use the Emacs text editor.

#hint(\~ represents the path to your home directory; man touch)





#Exercise 07 - Repository

Using blih, create a new repository called __pool\_c\_gecko__ and give the ACL reading permission to the user, "gecko".

#hint(Check the "How to turn in" doc [here](https://intra.epitech.eu/file/public/rendu/) and maybe [here](https://intra-bocal.epitech.eu/index.php?pgid=docvisu&m_doc_id=162).)





#Exercise 08 - SSH Key
Thanks to blih, you just created your first git repository. This folder exists on the correction server and you must now clone a copy on your computer.
In order to communicate with the Epitech server, and for this server to accept your connection, you must have an ssh key.
In your home, make a .ssh directory and go inside. Then generate an ssh key called "id_rsa_coding".

#hint(mkdir ; ssh-keygen)





#Exercise 09 - SSH Key Upload
To communicate with the server, you must send your public ssh key to the server by using blih.
Once you have finished this step, it is still necessary to tell your machine which key to use with which server. You have to create a ~/.ssh/config file and write the right configuration (this file must have the rights 600).

#hint(All of the instructions concerning sending the key and the content of the config file are in the documentation that was given in exercise 7.)





#Exercise 10 - Cloning
If the previous exercises have been done correctly, you can now retrieve the folder you created on the server by using git.
The repository address is: git@git.epitech.eu:/login_x/repository_name.
If you've succeeded, git should give you the following message:

#terminalNoPrompt([...]
        warning: You appear to have cloned an empty repository.
        [...])


#hint(man git-clone)

#warn(If git tells you that you don't have the rights\, wait 5 minutes for the key to be uploaded to the server. If it still doesn't work\, redo exercises 8 and 9.)





#Exercise 11 - Turn-in #hfill 5pts
Using blih, create a new repository called "pool_c_d01".
In order for your work to be graded, you must enable it to be read (reading permission).
Add the read rights to the "gecko" and "ramassage-tek" users.
You will always need to give the read rights to those two users for each of your repositories.

#hint(Do not forget to add the read rights to each day's repository, or else you WON'T be graded.)





#Exercise 12 - Copy
Copy the "test" file you created in exercise 6 inside the "pool\_c\_d01" turn-in directory.

#hint(man cp)





#Exercise 13 - Add
For the following exercises, you must use git to send the "test" file to the server.
First, you must add this file to the local git repository so that it can be tracked.
This step must be done only one time for each new file you wish to add to the git repository.

#hint(man git-add)





#Exercise 14 - Commit
Once your file has been added, you must save the changes locally by using git's "commit" command.
Remember to always write a suitable commit message so that you can easily find older versions of your work.
For our purposes, you can write "Added test file".

#hint(man git-commit)





#Exercise 15 - Push #hfill 10pts
Now that your changes have been saved locally, you must complete one last step. You must synchronize your local repository with the one on the server in order for those who have the right permissions to access your modifications.
This step is done with git's "push" command.

#hint(man git-push)





#Exercise 16 - Z #hfill 5pts
**Turn in**: pool\_c\_d01/ex\_01/my\_z
#br
In your git directory, make another folder, "ex\_01". Then create a file called "my\_z", in which you will write the letter "z", followed by a newline.

#terminal(cat -e my\_z
        z$)

#hint(Use Emacs.)

#hint(Do not forget to turn in your work! You will not be reminded again.)





#Bonus #hfill 2pts
**Turn in**: pool\_c\_d01/ex\_02/my\_init\_repo.sh#br

Write a shell script that takes the name of a repository as its first parameter, and automates all of the actions needed to initiate a repository that you have completed today (creating the repository with blih and giving the right permissions to the right users). 
Your script must then clone the repository in your current folder.
If an error occurs during a command execution, the script should stop right after the error without printing anything.#br

After it is completed and fully functional, you may, if you wish, place your script in a folder that is specified in your PATH variable so it can be more easily used. Or you can just add an alias in your shell configuration file. (You will first need to look at what your PATH variable is and how it's used by your shell or how to add an alias). Regardless of your choice, you will also need to change your file's permissions so it can be executed. We will see this tomorrow, so try to get ahead by yourself!

#warn(Don’t forget to check the probable errors that your script could have (number of arguments\, validity of your commands\,...).)

Done? Good!
Did you really check everything properly?
Yes? Do it again, just to be on the saferside.
After that, if you still have some free time, take some time to learn more about Linux and some of the basics commands, shell configuration, and personalization.

#hint(man env
        Check your __.bashrc__ file.)
