---
module:		Cap Gemini
title:		day 02
subtitle:	JDBC
background:  	'../background/EpitechCapBackground.png'

repository: 	jdbc_d02
repoRights:	gecko
language:	Java

groupSize:	1
noBonusDir:	true
noErrorMess:	true
author:		Vincent TRUBESSET
version:	1.0
---

**Introduction**
Today, we are going to dive deeper in JDBC as we will get into transactions manipulations, statements conditions, table joins, SQL functions and general optimisation.
You will use the same database as yesterday. #br

**Prerequisites**
You must finish the previous day before tackling this one, as the base concepts of JDBC were approached there. #br

#warn(We strongly advise you to review your yesterday’s work before starting today’s.)

#hint(Read each exercise until the end before doing it!)

# Exercise 01 #hfill 2pts
**File to hand in**: jdbc_d02/ex_01/ex_01.java #br
Once you established a connection with your database, turn off auto-commit and insert 2 rows in your database using separate statements: one should contain syntax errors.
Via exception handling, discard your changes in case of an error. Otherwise, save them in your database.
Display your table contents. #br

#hint(Look up commit & rollback.)

# Exercise 02 #hfill 1pt
**File to hand in**: jdbc_d02/ex_02/ex_02.java #br
Do exercise 1 using a `Savepoint` set before your first statement.

# Exercise 03 #hfill 3pts
**File to hand in**: jdbc_d02/ex_03/ex_03.java #br
With the help of Batch Processing, insert 3 different rows in your table `Learner`, then modify the name of the second one to "John Smith", and finally remove the first one.
Show your operations performed successfully.

# Exercise 04 #hfill 2pts
**File to hand in**: jdbc_d02/ex_04/ex_04.java #br
Do exercise 3 using `PreparedStatements`.

# Exercise 05 #hfill 3pts
**File to hand in**: jdbc_d02/ex_05/ex_05.java #br
**Restriction**: You are not allowed to use a String for the date. #br
Create a new table `IdentityCard` in your database, containing the fields `lastName`, `firstName`, `gender`, `dateOfBirth`, `placeOfBirth`, `height` and `learnerID`.
Then, insert a row in this new table with "Smith" and "John" respectively as last name and first name.
You must retrieve the `learnerID` by querying the `Learner` table.
Display the table contents to verify your operation has been successful. #br

#hint(`d` keyword ;\))

#newpage

# Exercise 06 #hfill 2pts
**File to hand in**: jdbc_d02/ex_06/ex_06.java #br
Write a program that will add many different Identity Cards, determine the corresponding Learners and add them only if they are missing.
Show the content of both tables. #br

#warn(Don’t forget the `learnerID`.)

# Exercise 07 #hfill 1pt
**File to hand in**: jdbc_d02/ex_07/ex_07.java #br
Retrieve a Learner named "John Smith" using his Identity Card.

# Exercise 08 #hfill 2pts
**File to hand in**: jdbc_d02/ex_08/ex_08.java #br
Retrieve all the Learners born in a city with a name shorter than 8 characters.

# Bonus exercise #hfill 3pts
**File to hand in**: jdbc_d02/ex_bonus/ex_bonus.java #br
Using an XML table containing the fields `id` and `data`, upload the XML file given below in your database.
Then retrieve the XML data using the `id` and print it in the console. #br

#warn(You must determine the right data type for the `data` field.)

```
<?xml version="1.0"?>
<Employee>
	<id>42</id>
	<first>John</first>
	<last>Smith</last>
	<Salary>1337</Salary>
	<Dob>16-12-1993</Dob>
</Employee>
```

___