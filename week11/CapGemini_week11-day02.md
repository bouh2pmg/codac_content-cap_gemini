---
module:			Cap Gemini
title:			day 03
subtitle:		SQL
background:		'../background/EpitechCapBackground.png'

repository: 	Advanced_SQL_Day
repoRights:		gecko
language:		SQL
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

All websites use databases, which are used in particular for authentication, but also for hierarchizing information, for statistics,... since it is necessary to keep as many elements as possible.#br

We will take an interest in how these data can be stored and we will find methods to extract the information conveniently and quickly.#br

First, to refresh your mempry, watch some explanatory videos about what is SQL and why using it:

* [What are databases and why do you need them?](https://www.youtube.com/watch?v=Q2GMtIuaNzU)
* [Database literacy: Why use a database?](https://www.youtube.com/watch?v=yeVHLTkIXB8)

#warn(For each exercise\, a comparison will be done between your result\, and the expected result. If at least one character is different (it could be a line at the end of your result)\, the result is wrong.#br

We will use the MD5 algorithm to do so because:

* tell you if you are right or not without giving you the answer\,
* correct your exercises faster\, because comparing 32 characters is faster (with use of encryption) than comparing a long text.

)

#hint(What is the MD5 algorithm?
Have a look at some documentation:
[wikipedia](http://en.wikipedia.org/wiki/MD5)\, [gohacking](http://www.gohacking.com/what-is-md5-hash/)\, [makeuseof](http://www.makeuseof.com/tag/md5-hash-stuff-means-technology-explained/)\, [accuhash](http://www.accuhash.com/what-is-md5.html)\, [Public_key_fingerprint](http://en.wikipedia.org/wiki/Public_key_fingerprint)\,...)





#Exercise 0

Install the following prerequisites, if not already done:

* __Apache__ $\\ge$ 2.2
You should already have Apache installed. If not, please refer to the proper documentation.

* __PHP__ $\\ge$ 5.2 (Apache module)
You should already have PHP installed. If not, please refer to the proper documentation.

* __MySQL__ $\\ge$ 5.6
You should already have MySQL installed. If not, please refer to the proper documentation.

* __PhpMyAdmin__ $\\ge$ 4.2.0
Refer to this [documentation](http://docs.phpmyadmin.net/en/latest/setup.html).

* __MySQL Workbench__ $\\ge$ 6.3
Refer to this [documentation](https://www.mysql.com/products/workbench/).

You are now ready to begin. 
Try to discover these tools for a few minutes and remember the queries you made two weeks ago. It will probably help you understand what comes next.#br

#warn(Today\, you will be guided and will work in an incremental mode.
So if you do not pass an exercise\, you won’t be able to continue.)

#hint(Please read carefully each instructions!)






#Exercise 1 #hfill 2pts

**Turn in**: Advanced_SQL_Day/ex_01/#br
Create a new database, called __open_data_ratp__; we are going to use some RATP data about public transport users.
Your database must be set with the "utf8_general_ci" collation.

#hint(Each time you will have to choose a charset or a collation\, "utf8_general_ci" will not always be the better\, but will probably be a very good choice.)

Create a new user called __open_data_login__ which password is "open_data_password", with all rights on the database you have created.#br

#terminal(mysqldump -u open_data_login -p 'open_data_password' --skip-comments open_data_ratp | tee exercise_1.sql
	/\\*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT \\*/;
	/\\*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS \\*/;
	/\\*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION \\*/;
	/\\*!40101 SET NAMES utf8 \\*/;
	/\\*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE \\*/;
	/\\*!40103 SET TIME_ZONE='+00:00' \\*/;
	/\\*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS\, UNIQUE_CHECKS=0 \\*/;
	/\\*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS\, FOREIGN_KEY_CHECKS=0 \\*/;
	/\\*!40101 SET @OLD_SQL_MODE=@@SQL_MODE\, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' \\*/;
	/\\*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES\, SQL_NOTES=0 \\*/;
	/\\*!40103 SET TIME_ZONE=@OLD_TIME_ZONE \\*/;#br
	/\\*!40101 SET SQL_MODE=@OLD_SQL_MODE \\*/;
	/\\*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS \\*/;
	/\\*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS \\*/;
	/\\*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT \\*/;
	/\\*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS \\*/;
	/\\*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION \\*/;
	/\\*!40111 SET SQL_NOTES=@OLD_SQL_NOTES \\*/;

)

#hint(So far\, your database is empty\, but there is already a lot of lines.
Imagine when you will have data\, you could have thousands of lines.
It’s really crazy!)

#hint(Check the commands __mysqldump__ and __tee__ if you want to learn some shell features.)

Do you remember the use of __MD5__?
So, if the last command is correct, the file "exercise_1.sql" must have been created and must be filled with the last result shown. Is it ok? Let’s go.#br

#terminal(md5sum *
a039ffa2642542d5ff25b2926b4eb7db exercise_1.sql)

#warn(Don’t delete your "exercise_1.sql" file\, but move it into the turn-in directory to know if you are right or not.)


 


#newpage

#Exercise 2 #hfill 1pt

**Turn in**: Advanced_SQL_Day/ex_02/#br

Download 2 datasets relatives to RATP from the intranet:

- Incoming traffic in 2013
- Incoming traffic in 2014

To check if you have downloaded the proper files, here is a MD5 hash:
#terminal(md5sum trafic*
54a7108f8d31d930c87341ce01ff7410  trafic_entrant2013.xls
275f8d14fe17bd6e89ee55d43feb7883  trafic_entrant2014.xls)

When it’s done, and because we have to verify your work (one more time), put these files without renaming them into the turn-in directory.#br

You should take a look at your downloaded files. 
Open it to see what the content is. 
It is data, but not easily exploitable as is (because no SQL).

#warn(For all the next MD5 given in this subject\, you have to put the concerned files in your turn-in directory; we will not repeat it from now\, but you have to!)






#newpage

#Exercise 3 #hfill 5pts

**Turn in**: Advanced_SQL_Day/ex_03/#br

Since you have documents which do not allow you to use SQL queries (not practical for statistics purposes), you need to create a structure (many tables) to store data properly. 

#warn(You have to read and execute exactly what is written\, or else that will not work. 
Open one of the documents you have downloaded.
It should help you to understand that you are doing.)

##Table "cities"

The column "Ville" should represent a table that contains the same value many times (in this case "Paris", but not only).
Databases have to be built with the minimum size to store a maximum of data. That means that we'll create a table called "cities" which will contain only one occurrence of each value.#br

Almost every tables must have an unique identifier (remember the videos in the introduction).
It’s the case of this one.
The first column name must be called "id", it’s a "INT(11)" data type and has attributes "Primary Key (PK)", "Not Null (NN)", "Unsigned data type (UN)" and "Auto incremental (AI)".
The second column must be called "name", it’s a "VARCHAR(45)" data type and has attributes "Not Null (NN)", "Unique Index (UQ)" and have a collation "utf8_general_ci".

#terminal(mysqldump -u open_data_login -p'open_data_password' --skip-comments open_data_ratp cities | tee exercise_3_cities.sql | md5sum
0d234be3d92e084702068366368931b7  -)


##Table "stations"

The columns "Réseau", "Station", "Trafic" and "Arrondissement pour Paris" should represent a table which contains all these values.
Create a table called "stations" with plenty columns:

- the first column name must be called "id", it’s a "INT(11)" data type and has attributes "Primary Key (PK)", "Not Null (NN)", "Unsigned data type (UN)" and "Auto incremental (AI)",

- the second column must be called "network", it’s a "ENUM('Métro','RER')" data type and has attribute "Not Null (NN)",

- the third column must be called "name", it’s a "VARCHAR(45)" data type and has attribute "Not Null (NN)",

- the fourth column must be called "city_id", it’s a "INT(11)" data type and has attribute "Unsigned data type (UN)",

- the fifth column must be called "traffic", it’s a "BIGINT(20)" data type and has attributes "Not Null (NN)" and "Unsigned data type (UN)",

- the sixth column must be called "year", it’s a "YEAR(4)" data type and has attribute "Not Null (NN)",

- the seventh column must be called "district", it’s a "TINYINT(2)" data type and has attribute "Unsigned data type (UN)".

Moreover, you have to add an "Index" key called "city_id" to the field "city_id".

#terminal(mysqldump -u open_data_login -p'open_data_password' --skip-comments open_data_ratp stations | tee exercise_3_stations.sql | md5sum
33cc403936bd8c39f8f757db53249b77  -)


##Table "lines"

Like the table "cities", there are a lot of multiple values in the column "Ligne de correspondances RATP".
We will create a table called "lines" to reference each line only once. #br

There are two columns:

- the first column name must be called "id", it’s a "INT(11)" data type and has attributes "Primary Key (PK)", "Not Null (NN)", "Unsigned data type (UN)" and "Auto incremental (AI)",
- the second column must be called "name", it’s a "CHAR(4)" data type and has attributes "Not Null (NN)" and "Unique Index (UQ)".

#terminal(mysqldump -u open_data_login -p'open_data_password' --skip-comments open_data_ratp lines | tee exercise_3_lines.sql | md5sum
25dd6580908a7b79ed496982b201a18a  -)



##Table "connections"

There must be a table called "connections" without column "id".
In this particular case, you’ll see why later, we should use a table which allows storing the column "Ligne de correspondances RATP" with the following exclusive method:

- the first column name must be called "station_id", it’s a "INT(11)" data type and has attributes "Not Null (NN)" and "Unsigned data type (UN)",
- the second column must be called "line_id", it’s a "INT(11)" data type and has attributes "Not Null (NN)" and "Unsigned data type (UN)".

Moreover, you have to add a "Unique Index (UQ)" to the both fields in the same time and called "station_id". 
It means that each couple of values will be unique in this table.
You also have to add an "Index" key called "line_id" to the field "line_id".

#terminal(mysqldump -u open_data_login -p'open_data_password' --skip-comments open_data_ratp connections | tee exercise_3_connections.sql | md5sum
b96c1934817cd2a38b5fc1998ccfc96d  -)




#newpage

#Exercise 4 #hfill 2pts

**Turn in**: Advanced_SQL_Day/ex_04/ #br

Now, to finish [half of this subject], you have to add some foreign keys:

- Table "stations", field "city_id", constraint called "stations_ibfk_1", set null on delete and no action on update.

- Table "connections", field "station_id", constraint called "connections_ibfk_1", cascade on delete and no action on update.

- Table "connections", field "line_id", constraint called "connections_ibfk_2", cascade on delete and no action on update.

#terminal(mysqldump -u open_data_login -p'open_data_password' --skip-comments open_data_ratp | tee exercise_4.sql | md5sum
49567bc9cda53f3c24eb135700e1650e  -)

You should try to display an "enhanced entity-relationship model".
It is [almost] always used by developers, because it is very visual and allows to have an overview of the database conception and the data organization.

#hint([This](https://dev.mysql.com/doc/workbench/en/wb-tutorials.html) could help.)

#hint(__PhpMyAdmin__ is a web interface database manager.
We recommend you to take a look at it and see how to redo Part 1 with it.
You don’t need to put any more in the turn-in directory.
You just have to take the time to discover this Web interface.
You will probably use PhpMyAdmin more often than the other one.)





#Exercise 4 #high(1)/#smallText(2) #hfill 2pts

First, download "script_import.tgz" and "database_structure.sql".
Then, execute these command lines:

#terminal(cat database_structure.sql | mysql -u open_data_login -p'open_data_password'
#prompt tar xf script_import.tgz
#prompt php composer.phar install
#prompt php populate_open_data.php)

Your "open_data_ratp" database should now be up-to-date.#br

The procedures you are now going to create will be recorded in your database.
At the end of the day, be sure to execute the command line below to turn-in your work:

#terminal(mysqldump -u open_data_login -p'open_data_password' --routines --skip-comments --no-data --no-create-info open_data_ratp > all_procedures.sql)

#hint(We warmly recommend to do it after each exercise (this command will overwrite the previous turn-in each time you execute it).)






#Exercise 5 #hfill 2pts
 
**Turn in**: Advanced_SQL_Day/ex_05/all_procedures.sql #br 

Create a procedure named "retrieveCityNameByPK" which takes as a parameter a city’s id and returns the name of the associated city.
The expected column is named "cityName". 

#terminal(echo 'CALL retrieveCityNameByPK(42)' | mysql -u open_data_login -p'open_data_password' open_data_ratp | md5sum
9c24a0b56c70882d2a0af6bf85f5bf0f  -)






#Exercise 6 #hfill 2pts

**Turn in**: Advanced_SQL_Day/ex_06/all_procedures.sql #br

Create a procedure named "sumOfTrafficForDistrict" which takes a district as a parameter and returns the total amount of traffic for it.
The expected column is named "trafficForDistrict".

#terminal(echo 'CALL sumOfTrafficForDistrict(13)' | mysql -u open_data_login -p'open_data_password' open_data_ratp | md5sum
9380ca5aae4cd2d3236433205cd7c282  -)





#Exercise 7 #hfill 2pts

**Turn in**: Advanced_SQL_Day/ex_07/all_procedures.sql #br

Create a procedure "matchForStationName" which takes a string as a parameter and returns a table with all the station names that contain the string, sorted alphabetically in ascending order.
The expected column is "stationName".

#terminal(echo 'CALL matchForStationName("de")' | mysql -u open_data_login -p'open_data_password' open_data_ratp | md5sum
89f234f723109f15398c886e1460c4a6  -)





#newpage

#Exercise 8 #hfill 2pts

**Turn in**: Advanced_SQL_Day/ex_08/all_procedures.sql #br

Create a procedure "isStationNameInParis" which takes a station name as an argument and returns "true" or "false" depending on whether the station is in Paris or not.
The column is named "isInParis".

#terminal(echo 'CALL isStationNameInParis("Vincennes")' | mysql -u open_data_login -p'open_data_password' open_data_ratp | md5sum
aa23f61c1b3c3516b27f3e736f9e0e26  -)





#Exercise 9 #hfill 2pts

**Turn in**: Advanced_SQL_Day/ex_09/all_procedures.sql #br

Create a procedure named "countStationsByLineName" which takes no argument and returns a table of transit line names in ascending order and for each transit line name, the total amout of stations. The expected columns are named "lineName" and "numberOfStations".

#terminal(echo 'CALL countStationsByLineName()' | mysql -u open_data_login -p'open_data_password' open_data_ratp | md5sum
051f5b87a981da280d6d88dcf1d33f38  -)


Part 3 (Bonus PhpMyAdmin)
	You have to drop the database then retry to do all the "Part 1" (the 4 exercises) with "PhpMyAdmin". You don’t need to put anything in the turn-in directory. -->
