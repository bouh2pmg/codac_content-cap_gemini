---
module:			Cap Gemini
title:			day 01
subtitle:		JDBC
background:  	'../background/EpitechCapBackground.png'

repository: 	jdbc_d01
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Vincent TRUBESSET
version:		1.0
---

**Introduction**
JDBC stands for Java DataBase Connectivity, which is a standard Java API for connectivity between the Java programming language and a wide range of databases.
It can access any kind of tabular data, especially data stored in a Relational Database. It works with Java on a variety of platforms, such as Windows, Mac OS, and the various versions of UNIX. #br

The JDBC library includes APIs for tasks that are commonly associated with database usage such as:
- establishing a connection
- creating and executing SQL statements
- viewing and modifying the resulting records #br

If you do not understand this introduction, take some time to do your own research on Google. #br

**Prerequisites**
Java and a database management system must be installed on your system.
You must create a database prior to using JDBC. You are free to name it. #br

#hint(Remember to always close your resources\, even if an errror happened.)

#newpage

# Exercise 01 #hfill 3pts
**File to hand in**: jdbc_d01/ex_01/ex_01.java #br
Create a Java application and connect to your database via JDBC.
Verify the connection has been successfully established. #br

#hint(Save the code from this exercise as you will need it as a base template for the next ones.)

# Exercise 02 #hfill 3pts
**File to hand in**: jdbc_d01/ex_02/ex_02.java #br
Using JDBC in your previously created app, create a new table `Learner` in your database, containing the fields `name`, `age`, `city`. Display all the tables from your database. #br

#warn(You have to determine the right data types for these fields.)

#newpage

# Exercise 03 #hfill 2pts
**File to hand in**: jdbc_d01/ex_03/ex_03.java #br
Re-run the code from exercise 2. It should fail, find out precisely why: you have to display the error code and message. #br

#hint(Exceptions!)

# Exercise 04 #hfill 3pts
**File to hand in**: jdbc_d01/ex_04/ex_04.java #br
Take the app template from exercise 1. 
Append to it the code needed to insert a new row in your table. Then, display the table contents via the ResultSet object. #br

#hint(Save the code from this exercise.)

# Exercise 05 #hfill 2pts
**File to hand in**: jdbc_d01/ex_05/ex_05.java #br
Do exercise 3 using a `PreparedStatement` to insert a row.

# Exercise 06 #hfill 3pts
**File to hand in**: jdbc_d01/ex_06/ex_06.java #br
Starting with the code from exercise 4, retrieve the row you just inserted and change its values.
Display the table contents to verify your operation has been successful.

# Exercise 07 #hfill 1pts
**File to hand in**: jdbc_d01/ex_07/ex_07.java #br
Do exercise 6 but remove the row instead of updating it.

# Exercise 08 #hfill 2pts
**File to hand in**: jdbc_d01/ex_08/ex_08.java #br
Using the SQL keyword `WHERE`, retrieve the second row and display its values.

# Bonus exercise #hfill 2pts
**File to hand in**: jdbc_d01/ex_bonus/ex_bonus.java #br
Insert a row in your table using the `ResultSet` object. #br

___
