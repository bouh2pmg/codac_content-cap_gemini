---
module:			Cap Gemini
title:			Application Architecture
subtitle:		how to design applications
background:		'../background/EpitechCapBackground.png'

repository: 	architecture
repoRights:		gecko
language:		none
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

Building applications requires specific code organization to be readable, maintainable and reusable.
We talk about **application architecture**.#br

The bigger the application, the more you need to focus on architecture, and the more gain you will receive.
Architecture is in fact the cornerstone of application development, and is a very important and valued skill.
A good architecture guarantees applications sustainable in the long run, whereas a bad architecure (or even worse, a complete lack of architecture) ensures the application will disappear with its creator, and will not be maintained or upgraded.

##Separation

Application architecture is mainly about the way things are separated.
This is common sense to separate topics:

- In any company, accounting and R&D offices are often physically disjointed.
- Tidying one's house, cleaning material and camping equipment are placed apart.
- In a computer, the input (such as a keyboard), the output (such as a monitor), the memory and the computation unit are clearly dissociated.
- It is a common practise (and an excellent habit) to store seperately data and applications on a hard drive.
- xHTML (containing the data) and CSS (describing the way it is displayed) are written in separated files.#br

One can rely on several criteria to discriminate things:

- their nature (like in the client/server architecture),
- their function (like in the MVC model),
- the skills required to manage these things (xHTML falls into web development, whereas CSS falls into web design).#br

There are many reasons why topics must be cleverly separated in application developpement:

- teams can work in parallel,
- one can replace a part of the application by another one without re-coding the entire application,
- debugging requires to know only a small piece of the code,
- a component of the software can be extracted and reused,
- task distribution among components is made easier, avoiding redundance or illogical organization.

#warn(Answer today's questions in a txt file\, whose name is given for each exercise.)



#newpage

#Packages #hfill 2pts

**File to hand in**: pool_java_d11/architecture/packages.txt#br

The first notable element of architecture is the breakdown in packages.
As you've already seen with Swing and Jersey, Java allows you to group classes in packages.
The objective is to have coherent entities that embed similar functionalities.#br


Thanks to packages, it is easy to have a person work on a specific part of the application (the GUI in the Swing example), or to completly modify this part (replacing the package by another one), or reuse this very package in different applications without further development (Swing is used in millions of applications!).#br

Find out what those packages do, and on what criteria the included classes got clustered:

- java.util
- java.io
- java.math
- java.sql
- javax.crypto

#hint(Check the [official documentation](https://docs.oracle.com)!)

Find a couple of examples in which you could have to build your own packages.






#newpage

#Client / server #hfill 2pts

**File to hand in**: pool_java_d11/architecture/client-server.txt#br

As far as web is concerned, distant communication implies a specific breakdown. 
There are several ones, but the client/server is overall the most common (peer-to-peer is another one).#br

The server is a distant machine that centralizes some information, and the client is the machine on which data is displayed.#br

Let's consider an online chess game.
Write down in a file what should be managed by the client, and what should be managed by the server, and explain why, among:

- pairing opponents,
- displaying the board,
- computing captures,
- allowing or refusing displacement,
- choosing the color of the GUI.

#hint(There is not a unique solution to that.)

In this very file, add the information the server sends to the client, and the information the client sends to the server.





#Front / back #hfill 0 pt

Another extremely common practice (almost mandatory in web development) is to separate front office (publicly accessible) from back office (private, to manage the application).

#warn(The front and back offices are often coded using different technologies.)

The front office reads data (generally from a database), whereas the back office feeds this database.









#Layers #hfill 4pts

**File to hand in**: pool_java_d11/architecture/layers.txt#br

A very common division for Java application (which could be considered as a generalisation of the client/server architecture) is to highlight 3 different layers:

- the presentation layer, which includes the user interface,
- the business layer, that handles all the treatment on data,
- the data layer, which manages data access.

This also applies to other development fields, such as web development.
For example, xHTML (data) and CSS (presentation) are often completed with javascript (business).

#warn(These layers\, despite being very common\, are just one way to organize layers. There are many other ways to do so.)

Go back to your Java projects from week 08 and find how you could have layered your application.
Imagine a file organization for these projects (by describing the different folders and the files they contain).

#imageCenter(layers.png, 500px)






#API #hfill 0pt

To communicate between the different elements of an application (whether between the client and the server, or between two different layers), one needs to speak a common language, as simple as possible.
It is called an **Application Programming Interface**.

#hint(API are uniformised\, and there are different kinds of API.
REST API are very common\, but you don't have to understand exactly what it is.)

Watch carefully [this video](https://youtu.be/7YcW25PHnAA).
Understand it.

#hint(Browse the web. You will find many and many interesting videos about API. Such as [this one](https://youtu.be/s7wmiS2mSXY).)







#MVC #hfill 8pts


**Files to hand in**: pool_java_d11/architecture/Model.java
#space(2.9) pool_java_d11/architecture/View.java
#space(2.9) pool_java_d11/architecture/Controller.java#br

A derivate from the 3-layer architecture is the MVC pattern.

- the Model layer handles the state of the application. To make it short, it is about data.
- the View layer is the representation of the user interface.
There are usually many views in an application.
A view can query the model, but is not supposed to change the state.
- the Controller layer manages user actions. It is the place where data is processed.
The Controller receives user requests and translates them into actions that the Model should take. Then it selects the appropriate View to display the response.


#imageCenter(MVC.png, 300px)

As an example, let's consider a HTTP request.
Here is a sequence diagram of a single request/response pair:

	Browser                View              Controller   	          Model
	---+---                -+--              ----+-----               --+--
	    HTTP Request       
	   +---------------------------------------->+                      
	   |                                         | update model  	    
	   |                                         +--------------------->+
	   |                                         | 	      return status |
	   |                                         +<---------------------+
	   |                             select view |         		        
	   |                    +<-------------------+                      
	   |                    | query state
	   |                    +------------------------------------------>+
	   |		            |                              return state |
	   |                    +<------------------------------------------+
	   |      HTTP Response |
	   +<-------------------+
 
#warn(It is sometimes hard to figure out where a specific piece of the application is supposed to go. Especially dividing the Model from the Controller... As a rule of thumb\, the Controller should be as minimal as possible; the Model should provide all the behavior it can without handling user actions or output details.)

Starting from your Currency Converter project from week 08, rewrite your code to fit a MVC pattern.
You must have 3 classes, one for each part of the model.

#hint(You need to take some time to write down a chart similar to the one above.)

#warn(Don't watch [this video](https://www.youtube.com/watch?v=dTVVa2gfht8)\, it's too much of an help!)

#hint(Due to the size of the application\, you only have 3 classes. For big applications\, you obviously have way more classes.)
