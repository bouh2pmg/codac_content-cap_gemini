---
module:			Cap Gemini
title:			week13-day02
subtitle:		JEE - Web Frameworks
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w13\_d02
repoRights:		gecko
language:		Java, XML, JSP
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

By now, you should know how to create a Maven project, chose an archetype, and setup a web server. Today, we will use a Web Framework to ease the development of recurrent tasks, like binding Java classes to database tables, setting up functions to handle routes and requests, (de)serializing requests / responses etc...
As an example, we are going to use Spring Framework. #br

[Spring](https://spring.io/) is a Web Framework developped by [Pivotal](https://pivotal.io/about) that aims to ease web application development by providing several tools. We'll cover some of them today so that you don't have to setup everything by hand every time you need to develop a webapp.

#newpage
#Step 01 - Setup

As we did before, setup the whole project using maven:

- Create a Maven project in IntelliJ
- Choose maven-webapp archetype (the same we used before)
- Let IntelliJ download and create everything
- Create a __java__ folder in the automatically created __main__ folder
- Create a new __org.kreognenberg__ package in this __java__ folder
- Create two packages, __model__ and __controller__ in the newly created package
- Open your pom.xml file and add those dependencies:
    - org.springframework:spring-core:4.3.10.RELEASE
    - org.springframework:spring-context:4.3.10.RELEASE
        - Excluding __commons.logging__
    - org.springframework:spring-web:4.3.10.RELEASE
    - org.springframework:spring-webmvc:4.3.10.RELEASE
    - org.slf4j:jcl-over-slf4j:1.7.25
    - ch.qos.logback:logback-classic:1.2.3
    - javax.servlet:jstl:1.2
    - javax.servlet.jsp:jsp-api:2.2
        - With the scope set to __provided__
    - javax.servlet:servlet-api:3.1.0
        - With the scope set to __provided__
    - javax.validation:validation-api:1.1.0.Final
    - org.hibernate:hibernate-validator:5.4.1.Final
    - javax.inject:javax.inject:1
- Add a __sourceDirectory__ tag inside the __build__ tag and make it point to your __java__ folder
- Add the __maven-compiler-plugin__ plugin in the __build__ tag
- Create a Run/Debug configuration in IntelliJ targetting GlassFish
- Run and observe the delightful HelloWorld.

If any of these steps seems unfamiliar to you, go back to yesterday's exercises or read the related documentation!

#newpage
#Step 02 - MVC - Reminder

As you might have noticed, we added __spring-webmvc__ as a dependency. This means that we are going to organize our application following the MVC architectural pattern. So, as a reminder:

#imageCenterCorners(png/MVC\_\_Main\_0.png, 384px, 0.1)

Here, __Controller__, __View__, and __Model__ are packages. The __View__ holds everything that is shown to the user:

- The graphical data itself, like sprites, fonts and colors
- The positions of everything shown
- The different user input handlers for clicks and keybindings

The __Controller__ holds the functions required to handle what happens on user input, as well as data integrity verification functions:

- Handlers for clicks that verify where the user clicked before continuing
- Handlers for keybindings actually binding a key to a function
- Functions to verify data, authorization to click on a menu, etc...

The __Model__ holds the actual data, in its simplest form. For instance, a TicTacToe grid has two representations:

- A graphical one, the view, which we explained just before.
- A non-graphical one, holding a list of ints, each int representing the content of a cell (0 for blank, 1 for X, 2 for O for example), but __nothing related to how it is shown to the user__.

Following this architecture, it becomes really clear what should go where. No view should directly know how the model is implemented. It just needs positions and graphical information to work. No model should know how it is shown, it is not its purpose. The model only holds simple data.
This separation has the advantage of allowing you to completely abstract what the action of actually showing something to your user is.
For instance, we could imagine a scenario where you want a first GridView written for Windows, using DirectX libraries and graphical resources, another GridView for Unix, based on OpenGL, and another one for the web, based on any web view engine. For such a scenario, you wouldn't want to change your model for every view. Maybe you don't even know how your model is going to be shown, because you are just developping a library, not an actual program, and you don't want your users to have to implement the model again ; it should be exactly the same. #br

In order to simplify this pattern, MVC relies heavily on a really simple design pattern (__design__, not __architectural__), the __Observer Pattern__.

#imageCenterCorners(png/MVC\_\_Abstract\_2.png, 384px, 0.1)

If you know a bit of UML, this graph should be self-explanatory. If you don't just remember that a __Subject__ holds a list of __Observer__, accessible via the __attach__ and __detach__ functions. The __Observer__ has an update function, that receives a __ChangeEvent__ holding everything related to the change (what cell the user clicked on and what symbol did he want, for our TicTacToe example). The __Subject__ also has a __notify__ function: this function simply creates a __ChangeEvent__ related to the change, and calls the __update__ method on each of its registered observers. Quite simple indeed. #br

Let's look at a complete example with our TicTacToe game:

#imageCenterCorners(png/MVC\_\_Example\_1.png, 384px, 0.1)

Here, everything falls right into place:

- The GridView, a specific way to show the grid, holds a list of GridCells, a somewhat complicated object holding graphical data about a Cell.
- The MainController, which receives user input (clicks), checks if the cell is correct before forwarding to the model.
- The Grid, a simple class holding a list of ints, nothing complicated, just data.

We clearly see that the GridView and the Grid don't know each other directly, they are not __tightly coupled__. This is exactly what we tend to achieve when following Object Oriented Programming: separation of use cases. The only link that is made between the View and the Model is in the abtract classes __Subject__ and __Observer__: a very abstract link, describing how a __Subject__ can notify its __Observer__.
From here, one could develop another View, maybe a WebGridView using web components to show the __Grid__, __without modifying anything in the Grid code!__ To use this new view, one just has to call __grid.attach(webGridView)__, and the Observer Pattern takes care of registering this new Observer. Now, when you call the __MainController__ handler, the __Grid__ does exactly the same thing, but with one more __Observer__ in its __observers__ list: both the GridView and the new WebGridView are updated independently.

#hint(Take time to understand this pattern\, it is used almost everywhere you have something to show on the screen. Even when you don't plan on another view being implemented for your models\, __separating concerns is extremely important__. You never know who will continue your work\, or if you'll need to implement another view in the future ; having such easy abstractions helps reuse the code\, whatever the case.)

#hint(Here we use ChangeEvent\, GridCell\, and other fictional classes. They just represent what you might actually use in your applications. The ChangeEvent represents a way\, but not the only way\, to reprensent a change in the model\, but it could be anything else that your GridView could understand. Events are just a common way to handle such ... events\, because they usually are really simple to fill with whatever data you need. The GridCell class represents what will hold the graphical data of your application\, but again\, it could be anything else containing assets.)

#newpage
#Step 03 - Spring MVC

Now that you know what MVC stands for, let's adapt it to our brewery. Web MVC is a bit different than 'standard' MVC, but still really close:

- The Controller represents the classes that will receive the requests from the user's browser: litterally their 'input', but web-flavoured.
- The Model is the same thing we discussed earlier: classes that hold the data (the User class, exempt from anything view-related)
- The View is our JSP page, not actually knowing our User model, but instead using what the Controller sends back in the response.

The main difference here is that the Controller sort of plays the role of the Observer, both receiving input, and sending back the response. This doesn't really change anything.
Another difference is that we are going to use a fourth package, the __Service__ package, that could be merged with the __Controller__ package, but is not, because it describes another abstraction level: the link between the Controller and the Database. The same way your view shouldn't know your models, your Controller shouldn't know how your data is stored. Your Service does. This way, you could change your Database driver (from SQL to PostgreSQL, or even MongoDB) without changing your controller!

So, for our brewery:

- Create a new __service__ package in __org.kreognenberg__
- Copy your User class from yesterday into the __model__ package
- Add an empty (for now) __UserService__ interface in the __service__ package
- Add an empty (for now) __UserServiceImpl__ class in the __service__ package that implements UserService
- Add an empty (for now) __UserController__ class in the __controller__ package
- Create a __resources__ folder (yes, another one) inside your __webapp__ folder
- Create a __core__ folder inside the new __resources__ folder
- Create two folders, __css__ and __js__ in the __core__ folder
- Create a __jsp__ folder inside the __WEB-INF__ folder
- Create two folders, __fragments__ and __users__, inside the __jsp__ folder
- Create two xml files, __applicationContext.xml__ and __dispatcher-servlet.xml__ in your __webapp/WEB-INF__ folder

Your project structure should look like this:

#imageCenterCorners(png/project\_structure.png, 72px, 0.1)

Right, so we have the basic structure. We will fill everything later on ; for now, open the __web.xml__ file and replace the contents with this:

    <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                            http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
        version="3.1">
        <!-- Your config goes here -->
    <web-app>

This imports the updated version of the web-app api. Then, find in the docs how to:

- Set the __contextConfigLocation__ __context-param__ to __/WEB-INF/applicationContext.xml__
- Add a __org.springframework.web.context.ContextLoaderListener__ to a __listener__ tag
- Declare a servlet called __dispatcher__, pointing to \
  __org.springframework.web.servlet.DispatcherServlet__, that __loads on startup__
- Add a __servlet mapping__ mapping __/__ to the __dispatcher__ servlet

#hint(You should be able to find some example configuration online. Try not to copy-paste\, typing helps memory. Just be sure to understand each step\, so that you can follow later.)

Open the __dispatcher-servlet.xml__ file, and replace the contents with this:

    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:context="http://www.springframework.org/schema/context"
        xmlns:mvc="http://www.springframework.org/schema/mvc"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">
        <!-- Your config will go here -->
    </beans>

This imports every xml schema to properly configure spring. Then, find how to:

- Tell Spring to use __annotation-driven__ 'mode'
- Add 3 __component-scan__ targetting each of your packages
- Create a __org.springframework.web.servlet.view.InternalResourceViewResolver__ __bean__, with a __prefix__ set to __/WEB-INF/jsp__ and a __suffix__ to __.jsp__
- Create a __resource mapping__ mapping __/resources/** __ to the __location __ __/resources/__

Now, open __applicationContext.xml__ and replace the contents with this:

    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">
        <-- This stays empty for now -->
    </beans>

We just finished setting everything up! Rejoice!

#newpage
#Step 04 - Services

Let's start actually building our app. We'll start with the services. They are the layer that communicates with your __Data Layer__ (i.e what stores your data and how to access it). For the sake of simplicity, we are going to use a __HashMap__ here.
Open __UserService.java__ and add two function signatures:

    User register(User newUser);

That checks if the user already exists and returns null if it does, or the new user otherwise, and

    User logUser(String username, String password);

That looks for the user, checks the passwords, and returns null in case of error, or the user otherwise.

When this is done, open __UserServiceImpl.java__, and implement those two methods. Then, add the __Service__ annotation to the __UserServiceImpl__ class. That tells Spring our class is a service, and allows us to link everything.

#hint(We should probably throw some kind of exception instead of returning null on error cases here\, but since we are only using a HashMap, we'll go for the easy solution. We'll see proper error handling later on.)

#hint(IntelliJ should complain about you not implementing your interface before you do so. Try to press Alt+Enter on the red-underlined text.)

We just defined two simple methods that we expose through an interface. Here, we use a HashMap, but later on, we are going to use a database. Because we separated concerns, we will just need to implement another child of UserService, and tell Spring to use that one instead. But that's for later.

#newpage
#Step 05 - Controller

#hint(Don't forget to use the internet to search for the keywords used here. Each emphasized word is so for a reason.)

We are now going to complete the __UserController__ class. This class will handle every user-related request coming from our webapp.

- Add the __Controller__ annotation to the __UserController__ class. This makes Spring able to discover it almost automatically. (remember the component-scan we setup earlier in xml)
- Add a private field of __UserService__ type with the __Autowired__ annotation. This tells Spring to automatically find the best implementation of this interface and set the field to an instance of it. This is part of the __dependency injection layer__ of Spring. For now we only have one implementation, our __UserServiceImpl__, so it will be used.

#hint(IntelliJ should underline the annotation. Try Alt+Enter again and select the first option.)

- Add a __RequestMapping__ binding __/__ __GET__ to the following function. The model parameter is what contains data sent in the request, but we'll come back to that later. For now, just return a string __representing a redirection to /userForm__.

    `public String index(Model model)`

- Add a __RequestMapping__ binding __/userForm GET__ to the following function. You should just return a new __ModelAndView__ initialized with a __viewName__, __modelName__ and __modelObject__. __Take note of the strings you pass to the constructor__

    `public ModelAndView userForm()`

- Add a __RequestMapping__ binding __/users POST__ to the following function. This function receives the form data from the user, parses the response, fills a User object, and calls our UserService to register a new User. But fear not, Spring does most of the work here. We don't give you the full signature here ; you'll have to figure out how to receive a __ModelAttribute__. Remember to also receive the __model__, you will fill it with your user in order to display its info in the view. The returned string should __represent a userInfo view__

    `public String addUser(...)`

Done! For the java part that is. Now that we have setup Spring and defined our application specific methods, we can implement the views that will render everything to the user.

#newpage
#Step 06 - Views

We are going to use some very basic JSTL and HTML combination. You already know HTML, and you're going to use it the same way you did before, but enhanced with java-like code __directly inside your page__. This allows us to easilly develop the dynamic part of the webpage, like printing a list of objects received from a database, link user input to our java code, and much more. First, create our views:

- Create three files, __index.jsp__ in the __jsp__ folder,  __userForm.jsp__ and __userInfo.jsp__ in the __users__ folder
- Create two files, __header.jsp__ and __footer.jsp__ inside the __fragments__ folder

#hint(Earlier\, when we were declaring route handlers in the Controller\, you might have noticed IntelliJ complaining about a view not being found. Check again.)

#hint(You can hold the Ctrl key and hover over symbols to show information about their definition. If you click\, you'll be taken to the actual definition. This is a good way to verify if you linked everything correctly between your xml / java / jsp: if IntelliJ can make the link\, so should your webapp. Try it on a String returned in the Controller methods.)

For the __userInfo__ page, you don't really need anything else than html and a little bit of JSP. Find how to __reference variables__, and maybe how to __test expressions__ (if, choose, etc).

#hint(If you correctly filled the model with an attribute of type User in your Controller\, IntelliJ should autocomplete everything related to that attribute when you type something inside a JSP code block.)

For the __userForm__ page, search for __spring:form__, and create a simple form to add a user.

#hint(Don't forget to check if the __modelAttribute__ attribute on your form points to the right object. The Ctrl key should work.)

For the __header.jsp__ and __footer.jsp__, just put what you need to include css / js libs and use __jsp:include__ to include them.
Last but not least, design an index page with links to the other pages. Modify your controller so that the index() method returns the index, and adapt the rest of the class to your needs.

The design is up to you, but I recommend using Bootstrap or MDL (Material Design Light), which are excellent css libraries. I find MDL nicer, but it's a bit more difficult to use.

#newpage
#Conclusion

We just used Spring to alleviate a huge part of our application. Maybe you don't see it yet, because we didn't do anything really interesting, nor did we use a tenth of what Spring has to offer, but you still might have noticed that we just setup some xml files, and Spring did the tedious work of linking every part together. Tomorrow, we will see how we can use an actual database and integrate it directly with Spring.
