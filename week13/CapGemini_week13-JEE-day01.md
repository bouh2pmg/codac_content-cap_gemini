---
module:			Cap Gemini
title:			week13-day01
subtitle:		JEE - Real world setup
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w13\_d01
repoRights:		gecko
language:		Java, XML
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

Now that you have a basic understanding of the whole web development stack, we are going to introduce you to what a real world project looks like. 
During the week, we will discover how to use Java Enterprise Edition to build a complete web project.
#br

Today, we will discover what a build system is, using Maven as an example.
#br

Several steps will be presented to you ; be sure to understand each of them before continuing to the next one.

#warn(Be careful! Each step depends on the previous one...)

All your files must be tested on your local host.



#newpage
#Foreword

As we have told you before, the architecture of a project is at least as important as the code or the graphic design, if not more. 
As such, we are going to look at how to use real world technologies to architecture your project.#br

Java uses Maven as a build system.
Maven consists of XML files to descibe how you want to organize and package your application. It is quite easy to understand, albeit hard to master.
But fear not, younglings, the [documentation](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html) is once again here to save the day!
#br

Another way to structure your application is to use an Integrated Development Environment (IDE).
Up until now, you have been using emacs or vi(m) or whatever text editor you are used to ; but as their name suggests, those programs are __text editors__, they are good at editing text, but not so much a organizing things, handling dependencies, getting packages from remote servers etc...
You can of course setup your (increasingly huge) editors to match your specific desires, but this might take a while, and you might need to update them really often; in other words, quite a tedious task.
#br

To solve this problem, we use IDEs.
Those programs might look like a text editor at first, but they bundle all you need to architecture your program correctly, manage dependencies, compile, deploy, export, and more. Java is an excellent example of technology which almost __requires__ an IDE, especially when it comes to Java Enterprise Edition. 
And guess what? This is exactly what we are going to use.
#br

Some of you may already know some IDEs like Eclipse, NetBeans, or IntelliJ IDEA.
I personnally recommend [IntelliJ](https://www.jetbrains.com/idea/?fromMenu) ; it is fast, as lightweight as such an IDE can be, fully cross platform, and made especially for Java developpers. It handles perfectly all the different tools you will have to use, has a large plugin base, huge community, and JetBrains has a ton of resources for their IDEs, with an [excellent documentation](https://www.jetbrains.com/idea/documentation/).
#br

We won't force you to use IntelliJ if you are already familiar with another IDE, but the subjects will make references to menus and settings related to IntelliJ.
If you still want to use something else, so be it, but you are probably missing a great toolset.


#hint(Depending on your operating system\, you may have an official package to install JetBrains' products. Check it out\, as it is easier to maintain a package than a downloaded zip file.)

#hint(For instance\, on ArchLinux\, all their IDEs are on AUR\, so you just have to yaourt intellij and select the ultimate edition.)

#hint(If you chose to use IntelliJ\, be sure to [register a student account](https://www.jetbrains.com/student/) if you have one to have access to the paid version for free.)

#hint(I chose IntelliJ as an example because it has everything you need and is quite simple to use. Nevertheless\, the other IDEs you might use should have the same functionalities explained in their respective documentation. 
Use the same keywords I use to browse the docs if you need\, you should find everything you need.)



#newpage

#Step 01 - Setting up IntelliJ

Create a Maven project in IntelliJ. When you are on the JDK and Archetypes page, after selecting Maven in the New Project Wizard, verify that a list of archetypes appears.
If not (or it seems to be loading for ever), go to:

	File > Settings (Ctrl+Alt+S) > Build, Execution, Deployment > Build Tools > Maven
and set VM Options for importer to -Xmx1280m ; this will allow Maven to use more memory to store archetypes.

#imageCenterCorners(png/settings.png, 384px, 0.1)

Once the list is loaded, check the create archetype box, and select the

    org.apache.maven.archetypes:maven-archetypes-quickstart

archetype and finish setting up your project. Do not hesitate to browse the documentation, you are almost certain to find what you need.#br

Once you have a complete project setup with the default code that compiles and runs, you can go to the next step.




#Step 02 - Maven

If you've followed the previous step, you might have some questions about Maven ; let's answer some of them.#br

Maven is a build system for a lot of techologies. It automates the tedious task of organizing your repository, creating config files, modules, downloading dependencies, and more. 
The archetype you selected is the simplest one, which gives you a default Java project, with a main function and everything ready to start a fresh application.#br

Open the _pom.xml_ file and check the contents.
Everything should be quite clear, and you should understand pretty much everything, especially if you already know XML. 
Once again, read the [documentation](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html).

#hint(Take enough time to understand the different configuration files that are present.)

#newpage
#Step 03 - WebApp

Now that you are a bit more familiar with Maven, we will create an actual JavaEE project, using the same method.
Create a new project, but this time, select the

    org.apache.maven.archetypes:maven-archetypes-webapp
archetype. After everything downloads, you should have a JavaEE Web Application project setup.
Take some time to explore the different folders and files Maven has created for you, imagine creating all of this by hand, and praise build systems!#br

For now, you won't be able to run your application.
We need to setup one more tool: a WebServer to host the application.
We are going to use [GlassFish](https://javaee.github.io/glassfish/documentation) here, but if you prefer [Tomcat](http://tomcat.apache.org/), you can use it ; they are quite similar.

#newpage
#Step 04 - GlassFish

Now that you have a project, you need to setup GlassFish to host your application.
Start by installing GlassFish following their [documentation](https://javaee.github.io/glassfish/documentation).#br

Once it is installed, create a Run / Debug configuration in IntelliJ that points to GlassFish and your source files. 
You can check [here](https://www.jetbrains.com/help/idea/run-debug-configuration-glassfish-server.html) how to do it.
Once again, follow the links in the documentation if you are not sure how to do it, and be sure to understand what you do!#br

If you pay attention to the bottom of the Edit Run / Debug configurations window, you'll notice that IntelliJ tells you if something is wrong with your configuration, and even tries to help you with the Fix button. Use it!



#Step 05 - Mini Project

You should now have a complete startup project to deploy a JavaEE Web Application!
Do not hesitate to modify the generated __index.jsp__ file, it is the equivalent of _index.html_, but for JavaEE.
The actual language used inside the JSP will be covered in another day, but feel free to look around, it is not very hard to use, and quite powerful!#br

As usual, the documentation for the different tools we just setup is quite comprehensive and generally well written. 
For instance, there are a plethor of little tools and menus integrated in IntelliJ that help you deploy the application more easily ; find how to refresh your files __without__ restarting your server and notice the time you spare! #br

During this week, we will explore several tools to ease the development of a web application.
To illustrate the different facets of such an application, we are going to develop a website to help Kreognenberg, a famous brewmaster, manage his brewery.#br

Start by creating a new __register.jsp__ file right next to your __index.jsp__ and fill it with a simple registration form:

 - An input for the username
 - An input for the email
 - An input for the password
 - A dropdown list with different kinds of beer
 - Radio buttons for the gender
 - Checkboxes for the visitor's interest (Buy beer, Brew some beer, look around...)
 - A submit button

Make a link in your index that points to your __register.jsp__ page.

#hint(You are free to organize and design these pages to your liking\, but be sure to have at least one of each elements so that your page structure resembles the one we'll follow.)





#Step 06 - Linking HTML and Java together

Now that we have a form, we need to be able to get what the user types in.
Create a __org.kreognenberg.model__ package in your __main__ folder, right next to your __webapp__ folder, and create a __User.java__ Class inside. The User class will group together all the fields of our registration form:

#imageCenterCorners(png/user.png, 128px, 0.1)

#hint(Make sure the name of the User fields correspond to the names of the html
    elements you have created!)

You just created a Java Bean!
This is how we call Java objects that are destined to make the link between your view (what is show to the user) and you model (the actual data).
Later, we will use these Beans to ease the link between the Views, the Java Models, and the database.




#Step 07 - CDI

Now, to actually link your JSP / HTML code to your Java code, we are going to create some interfaces that expose functions to handle our user.
Create a new __org.kreognenberg.cdi__ package and create a UserSession.java file inside where you define a UserSession interface (not class, interface!). For now, this interface will expose a simple function:

    String welcomeUser(User user);

Once this interface is created, create a Java class under the same package called UserSessionImpl that implements the UserSession interface. Fill in the function so that it returns a String composed of a greeting message and the name of the user passed as parameter.

#hint(In order to make Java discover your beans\, you need to create a beans.xml file in your __webapp/WEB-INF__ directory. Look in the New context menu of IntelliJ when you right-click on WEB-INF\, you should find what you need.)



#Step 08 - Servlets

Now, we need something to map our HTTP requests to our Java code. To do that, we use [Servlets](http://docs.oracle.com/javaee/6/tutorial/doc/bnafd.html).#br

Create a SignupServlet in a __org.kreognenberg.servlets__ package that handles post requests to the "/welcome" path and update your __register.jsp__ file so that the form action points to that path.

#warn(Be careful how you setup and link everything together. JavaEE has strict configuration file handling\, but no errors are going to be thrown if you don't setup everything correctly\, you'll just get a 404 or 500 HTTP error. Look for documentation about servlet-mappings in _web.xml_.)

Your post handler should write some of the information you sent through the form back into the response object. Make use of the UserSession interface we made earlier.

#warn(You will need Dependency Injection to do this correctly. Look for articles about CDI in the documentation.)




#Conclusion

You now have a really basic web application that handles basic forms, links, and can manipulate the data sent through requests. Later this week, we will see how to ease the creation of all the configuration files and Java objects we had to write to handle or website.#br

For now, feel free to fiddle with the configuration, the servlets, and read the docs!
