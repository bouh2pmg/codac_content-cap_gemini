---
module:			Cap Gemini
title:			week13-day04
subtitle:		JEE - Routes, Filters, and the REST
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w13\_d04
repoRights:		gecko
language:		Java, XML, JSP
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

During the week, we learned how to organize a project, use a Framwork, and develop dynamic web pages thanks to Java EE and Spring. This is great, and it is what you should expect from a modern web developer ; but good technologies do not necessarily means good application! For instance, there are a lot of ways to design your API (Application Programming/Public Interface).#br
As its name states, an API is __the public part of your application__, meaning all the functions you want to allow your users to call. This is the general situation, in Web development, API means something more specific: the set of routes that are going to be used. For our brewery, we defined some simple routes to retrieve and handle our data, but we didn't particularly __design__ our API.#br
As stated before, there are several ways to design an API, but the most common are:

- Not actually designing it, just creating a route every time we need something specific.
- Organizing routes by category (/users/doSomething)
- REST

As you might have guessed, we are going to use REST.

REST (Representational State Transfer) is just a way to say "my API is stateless, always returns the same data representation (Json, XML, etc...), and has semantic routes (organized)". Our API is a simili-REST one, as we had not seen what REST was yet ; we're going to fix that. As we are using HTTP requests, most of the job is done for us: an HTTP Request has no state, allows us to regroup everything under a specific __path__ (/users/userForm), and can return Json.

#newpage
#Step 01 - RESTify

So, what do we have to do in order to have a REST API? Well, the routes we already have are almost RESTful, but we are going to modify some of them following those principles:

- Each __resource__ must have its related routes
- Those routes must have an explicit path (/users for users, /ingredients for ingredients etc...)
- Each resource must have a set of operations exposed with its associated HTTP Method (GET, PUT, POST, DELETE)
    - POST -> Create a new instance and insert it
    - PUT -> Update an existing instance
    - GET -> Every route that gets a resource or a list of resources
    - DELETE -> Every route that deletes a resource or a list of resources
- Unless necessary, the paths must be the same for all those operations, i.e:
    - /users/ with POST     -> add
    - /users/ with GET      -> get all users (care for Authorization, as discussed yesterday)
    - /users/ with PUT      -> update a user
    - /users/ with DELETE   -> delete a user

#hint(You can of course use __path parameters__\, to delete a user for instance\, where the route would be /users/:id with DELETE method ; the point being that the main part of the path\, without the params\, is still the same. Be smart!)

#warn(Be careful\, each method has its way of transfering data. POST and PUT use the body of the request\, whereas GET and DELETE use query params. Design your routes accordingly.)

You should have the exact same set of routes as before, maybe with additional routes, but nothing less. As usual, data integrity is to be verified, as well as Authorization.#br
This way, if another developer uses your API, he doesn't really have to read any docs, he knows what to call, because everything is __sematically organized__. There are always exceptions related to the technology we use, like the route that returns the userForm. This is because we are mixing REST and MVC, and sometimes, the two collide. But if we were developing a web service, without any view, just the API, all of our routes would be RESTful, because the whole point of our API would be to be used by different people (like [the imgur API](https://apidocs.imgur.com/)).

#newpage
#Step 02 - Route filters

Another important part of an API, after the design itself is decided, are __Route Filters__. Filters are generally some sort of callback that are called at different points in the 'lifecycle' of our route (i.e pre/post-hooks). Here, we are going to use Spring Filters, but keep in mind that you could develop your own filters.
As usual, Spring being quite big, you'll have to setup XML configuration files to add filters to your application. For this step, __implement your filter chain__ to replace our login method. Follow the recommendations in the docs to use __UsernamePasswordAuthenticationFilter__.

#hint(You should be able to find everything you need in the official Spring documentation.)

#newpage
#Step 03 - Extending our brewery

For now, we can't do much with our website. This will be fixed soon:

- Add everything needed to handle the beer people brew on our website
    - A new model to represent a beer
    - A way to save a batch on the page allowing to mix ingredients
    - A way for a user to see its batches on his profile
- Add a page, accessible from the index, that lists all custom beers made by users, with their detailed composition, and basic info about their creator.
    - A __logged user__ visiting this page should be able to add a batch to his favorites.
    - A favored batch should be visible on the user's profile, but separated from his own batches.

#hint(As usual\, repeat the steps we took for everything newly created: Services\, Daos\, DB Tables\, Authorization/Filters\, Data Integrity\, JSP.)

#hint(You are free to implement this however you want. Just remember to keep your field/function names semantically correct\, and be sure to follow every 'convention' we explained.)

#warn(Here\, we are heavily modifying our classes. That wouldn't have to happen in 'real life': you should have designed most of your models and how they are linked together __before__ starting to implement ; this week being an exercise\, we chose to progress from concept to concept\, instead of preparing everything beforehand. Do not do this in a real project\, you'd go bold trying to solve every conflict that appears when making breaking changes to an API\, especially if you don't work alone.)

#newpage
#Step 04 - To administer or not to administer

To further illustrate the use of filters, security, and Authorization, we are going to add an Admin part to our webapp:

- Add a way, using Spring Filters, Roles, and JSP, to connect as an Admin.
- Add a way for the Admins to:
    - Review registered users
    - Review registred ingredients
    - Review registered beers
    - Modify instances of the previous resources

This is how most webapp handle Authorization: using roles to separate who can access what, and deliver data to whose it's.

#newpage
#Conclusion

The week is almost over, and you now know how to setup a complete web application! It was quite a dense week, so don't feel embarassed or anything about returning to previous days to clarify certain parts. The concepts we exposed, although widely spread, are not particularly easy to grasp, and only become crystal clear with practice. As such, do not hesitate to ask your assistants, find resources on the web ([StackOverflow](https://stackoverflow.com/) is your best friend), or even cry in despair ; but the most important is that you actually understand all the steps!#br
Tomorrow, we will finish our brewery, adding some 'quality of life' concepts for our users, like using cookies, sessions, HTTP headers, etc...
