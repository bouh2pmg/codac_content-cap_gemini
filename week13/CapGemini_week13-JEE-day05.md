---
module:			Cap Gemini
title:			week13-day05
subtitle:		JEE - En route for the Brewery!
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w13\_d05
repoRights:		gecko
language:		Java, XML, JSP
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

Welcome to the last day of this week! We are finally really close to a complete application. Today, we are going to consolidate our webapp, and make the life of our user easier.

#newpage
#Step 01 - OAuth2

Right now, the only way a user has to connect is by registering on our website first. But most of the time, we just need an ID and some basic info about our users. Hence, we can rely on another service to host those info and just ask the user the right to access this data and use it for our website: this is called an __Authorization Code Grant__.

Add a __Connect with facebook__ button on your login page using Spring Security; you should find everything you need on the docs, it is a relatively simple and widesreapd task.

#hint(Feel free to use any other OAuth2 server\, like Google.)

#hint(You might have to add some dependencies.)

#newpage
#Step 02 - Profile enhancements

The profile page of our users looks a bit dull right now. Let's add some features:

- Add a way for users to upload a profile image when registering.
- If not already done, allow a user to modify each field of his profile.

#hint(Once again\, keep following the concepts we described!)

#hint(Don't forget REST!)

#newpage
#Step 03 - WebSockets

HTTP is a nice protocol, but it is just a Request/Response protocol, which means that __we have to__ send a request if we want a response. Sometimes, you'll want live updates of your content, like a messaging system: WebSockets are designed just for that.#br

A WebSocket is just some sort of bidirectional pipe, with a server connected on one end, and a client on the other. It is exactly like using Windows or Linux Sockets, but for the web, and allows live exchange of data.#br

- If not already done, add a page to view a specific batch.
    - It should display at least the basic info of the brew, its author, and date of creation.
- On this same page, add a way for users to comment on the batch.
    - A comment MUST contain at least its author and a timestamp (and the message of course).

Be careful though, here, we want our users to see the message as soon as it is sent, not after refreshing the page! Make use of WebSockets to solve this. You will need the same things we did for the rest of the webapp: Models, Service etc... But this time, with Spring Websockets.

#hint(Once again\, some fairly good examples are available on Spring's website.)

#newpage
#Step 04 - Finishing touches

There isn't much more to add to our website in terms of new concepts; take the remaining time to look more closely to the JSTL and JSPs in general:

- Add a way to autologin with a Cookie when re visiting the webapp
- Look at how to handle the Http Session. There is no actual need for it in our webapp, but you could just use it as a POC.
- Why not add a way to send an email to a user ?
- Look closely at how to define your own custom tags for JSP. This is an excellent way to separate your views into 'modules' and improve code readability!
    - For instance, maybe you could define your own tag that displays an ingredients info in a nice little box or expandable panel, letting you just pass the Ingredient object to the tag, and letting the tag handle everything else.
    - This is actually how most front technologies work nowadays: modular web components put together in HTML with another, more powerful language on the server (Like Java for us). This allows great code reusability, and clarifies your html pages
- JSP has its own Domain Specific Language (DSL), EL (Expression Language), which is relatively flexible. Take a look at it, it allows you to execute simple code directly inside your html page.

#newpage
#Conclusion

This subject nears its end because all that is left to do is up to you. Be creative! But also be smart and rigorous! We want you to be able to redo the same thing without explanations for next week. Take time to understand everything we've done so far and try to extend on the current project so that you know how everything fits together.#br
Next week, we'll start a new project from scratch; but don't worry, we'll skip all the tedious setup tasks using Spring Roo. You can take a look at it now if you want, it'll help you start next week. but don't lose all your time with it, we'll still explain it nect week.
