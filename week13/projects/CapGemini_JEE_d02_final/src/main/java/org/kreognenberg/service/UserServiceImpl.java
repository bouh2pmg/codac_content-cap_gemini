package org.kreognenberg.service;

import org.kreognenberg.model.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    private Map<String, User> mockUsers = new HashMap<String, User>() {{
        put("nal", new User("nal", "password",
                "mail@mail.com", "Ale", "Male",
                new String[]{}));
    }};

    public User register(User newUser) {
        if (mockUsers.containsKey(newUser.getUsername()))
            return null;
        mockUsers.put(newUser.getUsername(), newUser);

        return mockUsers.get(newUser.getUsername());
    }

    public User logUser(String username, String password) {
        if (mockUsers.containsKey(username)
                && mockUsers.get(username).getPassword().equals(password))
            return mockUsers.get(username);

        return null;
    }
}
