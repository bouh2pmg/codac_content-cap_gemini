package org.kreognenberg.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {
    @NotNull
    @Size(min = 2, max = 30)
    private String username;

    @NotNull
    @Size(min = 8, max = 256)
    private String password;

    @Email
    private
    String email;

    @NotNull
    private
    String beerKind;

    @NotNull
    private
    String gender = "Male";

    @NotNull
    @NotEmpty
    private
    String[] interests;

    public User(String username, String password, String email,
                String beerKind, String gender, String[] interests) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.beerKind = beerKind;
        this.gender = gender;
        this.interests = interests;
    }

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBeerKind() {
        return beerKind;
    }

    public void setBeerKind(String beerKind) {
        this.beerKind = beerKind;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getInterests() {
        return interests;
    }

    public void setInterests(String[] interests) {
        this.interests = interests;
    }
}
