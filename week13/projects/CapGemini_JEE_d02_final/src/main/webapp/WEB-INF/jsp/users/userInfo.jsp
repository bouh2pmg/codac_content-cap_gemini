<%--
  Created by IntelliJ IDEA.
  User: naliwe
  Date: 8/4/17
  Time: 10:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<jsp:include page="../fragments/header.jsp"/>

<body>
<h2>User info</h2>
<c:if test="${not empty user}">
    <table>
        <div>
            <label class="col-sm-2 control-label">Name:</label>
            <p class="col-sm-10">${user.username}</p>
        </div>
    </table>
</c:if>

<jsp:include page="../fragments/footer.jsp"/>
</body>

</html>

