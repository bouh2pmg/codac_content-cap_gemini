<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url value="/resources/core/js/material.min.js" var="materialJs"/>
<spring:url value="/resources/core/css/material.min.css" var="materialCss"/>
<spring:url value="/resources/core/css/kreognenberg.css" var="myCss"/>

<head>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link rel="stylesheet" href="${materialCss}">
    <link rel="stylesheet" href="${myCss}">
    <script src="${materialJs}"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kreognenberg</title>
</head>

<spring:url value="/" var="urlHome"/>
<spring:url value="/user" var="urlAddUser"/>
