<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<html>
<body>
<jsp:include page="../fragments/header.jsp"/>
<spring:url value="/users" var="addUserUrl"/>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <a class="mdl-layout-title mdl-navigation__link" href="${urlHome}">Kreognenber</a>
            <div class="mdl-layout-spacer"></div>
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="${urlAddUser}">Register</a>
            </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Kreognenberg</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="${urlAddUser}">Register</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content vertical-layout center">
            <h2>User Form</h2>
            <form:form cssClass="vertical-layout" method="POST" action="${addUserUrl}" modelAttribute="user">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <form:input class="mdl-textfield__input" type="text" id="usernameField"
                                path="username" pattern=".{2,32}"/>
                    <label class="mdl-textfield__label" for="usernameField">Username</label>
                    <span class="mdl-textfield__error">Username must be between 2 and 32</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <form:input class="mdl-textfield__input" type="text" id="passwordField"
                                path="password" pattern=".{8,256}"/>
                    <label class="mdl-textfield__label" for="passwordField">Password</label>
                    <span class="mdl-textfield__error">Password must be between 8 and 256</span>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <form:input cssClass="mdl-textfield__input" id="emailField"
                                path="email" type="email"/>
                    <label class="mdl-textfield__label" for="emailField">Email</label>
                    <span class="mdl-textfield__error">Email is invalid</span>
                </div>

                <div class="horizontal-layout around-justified" id="genderSelect">
                    <label class="mdl-textfield--floating-label" for="genderSelect">Gender</label>
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                        <form:radiobutton id="option-1" cssClass="mdl-radio__button"
                                          name="options" value="Male" path="gender"/>
                        <span class="mdl-radio__label">Male</span>
                    </label>
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                        <form:radiobutton id="option-2" class="mdl-radio__button"
                                          name="options" value="Female" path="gender"/>
                        <span class="mdl-radio__label">Female</span>
                    </label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label class="mdl-textfield__label" for="beerKindSelect">Favorite beer kind</label>
                    <form:select cssClass="mdl-textfield__input" id="beerKindSelect"
                                 path="beerKind">
                        <form:option value="Ale"/>
                        <form:option value="Lager"/>
                    </form:select>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label class="mdl-textfield--floating-label" for="interestSelect">
                        What are you going to use the site for ?
                    </label>
                    <form:select cssClass="mdl-textfield__input" id="interestSelect"
                                 path="interests" multiple="true" size="3">
                        <form:option value="Brew some beer"/>
                        <form:option value="Buy some beer"/>
                        <form:option value="Look around"/>
                    </form:select>
                </div>

                <hr/>
                <div class="horizontal-layout end-justified">
                    <button class="mdl-button mdl-js-button mdl-button--accent mdl-button--raised">
                        Add
                    </button>
                </div>
            </form:form>
        </div>
    </main>
    <jsp:include page="../fragments/footer.jsp"/>
</div>
</body>

</html>

