package main.org.kreognenberg.model;

public class User {
    private String   name;
    private String   email;
    private String   password;
    private String   beerKind;
    private String   gender;
    private String[] interests;

    public User(String name, String email, String password, String beerKind,
                String gender, String[] interests) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.beerKind = beerKind;
        this.gender = gender;
        this.interests = interests;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getBeerKind() {
        return beerKind;
    }

    public String getGender() {
        return gender;
    }

    public String[] getInterests() {
        return interests;
    }
}