package main.org.kreognenberg.servlets;

import main.org.kreognenberg.cdi.UserSession;
import main.org.kreognenberg.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SignupServlet extends HttpServlet {
    @Inject
    private UserSession session;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String   name      = request.getParameter("name");
        String   email     = request.getParameter("email");
        String   password  = request.getParameter("password");
        String   beerKind  = request.getParameter("beerKind");
        String   gender    = request.getParameter("gender");
        String[] interests = request.getParameterValues("interests");
        User     user      = new User(name, email, password, beerKind, gender, interests);

        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Kreognenberg Brewery</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Kreognenberg Brewery<h1>");

            if (session == null)
                out.println("Uh ohhh...");
            else
                out.println("<p>" + session.welcomeUser(user) + "</p>");

            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }
}
