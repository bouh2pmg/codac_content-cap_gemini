package main.org.kreognenberg.cdi;

import main.org.kreognenberg.model.User;

public interface UserSession {
    String welcomeUser(User user);
}
