package main.org.kreognenberg.cdi;

import main.org.kreognenberg.model.User;

import javax.enterprise.inject.Default;

@Default
public class UserSessionImpl implements UserSession {
    public String welcomeUser(User user) {
        return "Welcome to the Kreognenberg Brewery " + user.getName() + "!";
    }
}
