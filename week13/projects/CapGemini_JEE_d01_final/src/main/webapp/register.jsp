<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<h1>Sign-up</h1>

<p>Please fill in the form to sign-up.</p>
<form action="welcome" method="POST">
    <label>Name: </label><input type="text" name="name"><br />
    <label>Email: </label><input type="email" name="email"><br />
    <label>Password: </label><input type="password" name="password"><br />
    <label>What kind of beer do you prefer ?</label>
    <select name="beerKind">
        <option value="Ale">Ale</option>
        <option value="Lager">Lager</option>
        <option value="CaskAle">Cask Ale</option>
        <option value="CornBeer">Corn Beer</option>
        <option value="Other">Other</option>
    </select><br>
    <label>Gender:</label><br>
    <input type="radio" name="gender" value="male">Male
    <input type="radio" name="gender" value="female">Female<br>
    <label>Select what you'll use this website for:</label><br>
    <input type="checkbox" name="interests" value="buy">Buy beer
    <input type="checkbox" name="interests" value="brew">Brew some beer
    <input type="checkbox" name="interests" value="look">Just look around
    <input type="checkbox" name="interests" value="other">Other<br>
    <input type="submit" value="Sign-up">
</form>

</body>
</html>