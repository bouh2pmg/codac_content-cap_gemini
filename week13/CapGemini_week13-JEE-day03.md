---
module:			Cap Gemini
title:			week13-day03
subtitle:		JEE - Validation and Database
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w13\_d03
repoRights:		gecko
language:		Java, XML, JSP, Sql
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

Yesterday, we saw how to use Spring to link every part of our application together, while still separating concerns. Today, we are going to add Validation to our data, so that we can be sure of our __data integrity__ before writing to the database, and of course, we'll create and manage the database itself. Once again, you'll see that Spring alleviates most of the hard work, like linking some Java Objects to some database driver.

#warn(Today steps will be much less descriptive of what you have to do. We assume that you are now familiar with how Spring usually works and expect that you browse the docs even more ; you'll find a lot of interesting resources on Java and Spring! Just remember to understand the pieces of code you read before using them.)

#newpage
#Step 00 - ORM

You might have heard / read this acronym pretty much everywhere. It stands for __Object Relational Mapping__, and means exactly that. It maps Objects (in the code, Java or anything else) to some relational way of storing them (like all sql dialects). It is not really hard to understand, but quite complicated to elaborate.
Up until now, you wrote your Sql queries almost by hand, sometimes with the help of a Database Driver:

    User u = driver.query("SELECT * FROM users WHERE id=?", user.id);

It is quite simple to understand, quite simple to use, and works great. But what happens if this user has a list of Adresses ? Or a birth date ? Your driver has no way to determine whether it should query all existing adresses, or if its a OneToMany relation, or ManyToMany, or ManyToOne. It can't even decide how to format the birth date, it depends on what you need. You'd have to write all this by hand, make joints between tables in you queries, declare how to serialize / deserialie your date... in short, you'd almost have to write pure Sql queries, and all the related transformers for each and every class you need to store, whereas you are in an Object language. You should clearly be able to define your objects so that your drivers knows what to do with them, how to transform a complicated relational object into different sql queries that insert every new child object with its parent id, or the opposite, how to fill an objet with the result of a query, even with nested objects.
This is what an ORM does. It exposes a set of functions / annotations to let you define relations between your objects, and then easily serialize (from Java to DB) and deserialize (from DB to Java).
But, this kind of magic only makes sense when we know exactly why we need it. So let's write some queries!

Keep yesterday's project, and copy it as today's repository. You should have an index that links to a registration form, and a view to display a user's info. Start by adding a view to allow log in and the corresponding Java code. Then:

- Create a __dao__ package in __org.kreognenberg__.
- Add a __UserDao interface__ and a __UserDaoImpl class__ inside.
- Add a __HSQLDB__ [Data Source](https://www.jetbrains.com/help/idea/managing-data-sources.html) in IntelliJ.
- Add a __db.sql__ folder in __/resources/__ (at the root, not the one inside /WEB-INF/).
- Add two scripts, __create_db.sql__ and __populate_db.sql__.
- Add the required configuration (XML or Java, you choose, but XML is designed for configuration) to tell Spring you are using:
    - An __EmbeddedDatabase using HSQLDB__.
    - Pointing to a __users__ table.
    - That uses __create_db.sql__ first, and then __populate_db.sql__.
- Fill up the __UserDao__ interface with:
    - `User findById(Integer id)`: returns users.where(user.id == id).
    - `List<User> findAll()`: returns a List<User> of all the users.
    - `void save(User user)`: inserts a __new__ user.
    - `void update(User user)`: updates an __existing__ user.
    - `void delete(Integer id)`: calls users.removeWhere(user.id == id).
- Implement those functions in __UserDaoImpl__.
- Use the __Repository__ annotation.

__Dao__ means __Data Access Object__. It's yet again another abstraction level: The same way our controller doesn't need to know how we insert or retreive data (hence the Services), the Service doesn't need to know how we actually access the data (what functions are called to serialize or deserialize our objects into and from the databse): here come the Daos.

#hint(Although this abstraction level may look very close to the Services\, they are really different. Their difference doesn't lie in the form of the object\, which is almost the same as the related Service\, but in the actual code that is called. The Service delegates the responsibility of querying the data to the Dao\, gets the results\, and then either just returns the result to the Controller\, or modifies the object\, combines it with other queries\, transforms it ... Once again, this allows us to avoid __tightly coupling__ how we retrieve data and how we serialize it.)

Each function in __UserDaoImpl__ should query the database. You have to use the __NamedParameterJdbcTemplate__ with the __Jdbc__ driver. You'll have to implement a __UserMapper__ one way or the other ; a class that maps a __database row__ to a __User__ instance, and fills the user instance fields with the row fields. This is quite a tedious task, but it shouldn't take too long.

#hint(You might have to write a function that transforms a delimited String into a List<?>!)

#newpage
#Step 01 - Validation

Now that we have a database, we need a way to enforce data integrity. Especially with Sql, we want a way to verify that an object is correctly formed __before__ trying to insert it into the databse. For instance, we might want to verify that a user that subsribes has entered a valid email, or that his username is between 3 and 64 chars, etc... We don't need to let our database driver fail, we can check everything before actually calling our DB (which probably has the highest performance cost of all the lines we wrote until now, unless you did something really wrong with some Java functions). Apart from performance cost, I/O errors are always kind of tedious to handle. The more mecahnisms we put in place to avoid even having to check I/O errors, the better we feel. So let's dive in.

There are two major ways to handle data validation:

- Directly on the server: we receive a request containing the results of a form, check the contents, and associate a rule and an error message with every field of the form. If any field is invalid, we return the same form, but with its view filled with the error messages we generated. We are going to use a mix of Java and Hibernate Validators to illustrate this scenario.
- From the user's browser: Some html elements have integrated validation. For instance, Material Design Lite implements every MaterialDesign guideline, including input validation following a pattern you pass as an html attribute.

As you might have guessed, why would we ask our server to do the work a simple html element can do ? Well, sometimes, you need some server side info to validate an input. Unless you are using some webcomponent technology like Dart, Angular, Polymer etc.. which heavily rely on your web browser to execute the webapp code, you'll want to split the charge between the client and the server.
Usually, simple validation rules like the length of an input, the fact that its a string or an int, and that kind of validation is done client side, directly in html, because it is simple and doesn't require anything else than the input itself to be validated.

We are going to do a little bit of both here:

- Add validation __client side__, in html, using the css / html lib of your choosing (MDL recommended) in the __User Form__ for the __username and password__ fields.
- Add some __server side__ validation too for the same form, for all the remaining fields. You are free to validate data how you want, but keep things logical ; you can use the javax.validation and hibernate annotations for the simplest validators, but you'll have to use Spring Validators for more complicated validation rules (like how the password and password confirmation match).
    - Nothing should be empty.
    - The password must match the password confirmation.
    - The gender must match the values you chose to display ("Male", "Female", "Meat Popcycle", or whatever) (i.e ensure a user isn't trying to send you data you did not expect).
- Add a __messages__ folder in the __/resources/__ (the root one again).
- Add two files, __validation.properties__ and __messages.properties__ inside it.

#hint(Spring offers an easy way to validate a form server-side\, and return errors linked to every field. You will need the `<spring:bind>` JSP tag to bind a `<spring:form>` element to its specific error.)

Fill the two __.properties__ file we created with your error types and corresponding messages.

#hint(Consider using __ValidationUtils__ from Spring to map a specific validation error to its message so that Spring returns messages you set in a __ResourceBundle__.)

#newpage
#Step 02 - ORM to the rescue

So now, we have some validation, and a database to store our data. But writing the code to serialize / deserialize our objects into our database is quite the opposite of fun. Thankfully, Spring has mechanics to automate all of this. Once again, there are a lot of __data persistence__ implementations. We are going to use Hibernate, as it is fully integrated with Spring.

- Setup a __Hibernate SessionFactory__ targetting a __BasicDataSource__ with the
  __org.hsqldb.jdbcDriver__ driver.
- Modify your __UserDaoImpl__ so that it uses the __Hibernate API and SessionFactory__
    - BONUS: Don't modify this file, keep it as is and add another implementation, __HibernateUserDaoImpl__, and find a way to differentiate it from the default one, and tell Spring to use this one instead. This illustrates why we added this abtration level: we didn't have to modify the Controller or the Service!

These steps require some configuration and documentation reading to properly work. Be mindful of everything you find online!

#newpage
#Step 03 - Transactions

When we manipulate databases, we generally do the same operations: Create Read Update and Delete (CRUD operations). But sometimes, we need to apply a set of transformations to a dataset. When this occurs, we have to be very careful at how we do things. Imagine we want to query all the ingredients we have in stock, filter them by category, apply a new tax modifier to their price, then check if the price of every modified product is higher than a threshold and cap it if necessary. This relatively simple scenario brings some questions:

- What happens if after modifying the price of a product, an error occurs ?
- Should I keep the modifications I made so far and just return the error ?
- Should I skip the item and just continue ?
- Should I revert everything and notify the developper something went wront ?
- Should I care at all ?

There is no right answer ; it is domain specific. But, in most cases, the safest choice is the best: leave the databse in the state it was __before you called the function__. Because some of the steps we want to apply depend on the previous ones (like the last step), we can't afford risking a modification to happen where it shouldn't.
Such a modification is called a __Transaction__, and is a very well know database pattern. As such, Hibernate offers __Transactional Operations__. To illustrate them, we are going to extend a little on our Brewery:

- Create a new model, __IngredientAttribute__:
    - `String name`: name of the attr.
    - `String taste`: Bitter, Sweet, Spicy ...
- Create a new model, __Ingredient__:
    - `String name`: the name of the ingredient.
    - `String category`: Spice, Mead, etc...
    - `int amount`: how much of it is used.
    - `List<IngredientAttribute> attributes`: the attrs of the ingredient.
    - `int pricePerKilo`: self-explanatory.

Create the appropriate Getters and Setters.

#hint(Whenever you have a class that is intended to have getters and setters\, remember you are using and IDE! Create your class\, then the attributes\, but just the attributes. Then\, in IntelliJ\, press Alt+Insert\, and chose Getter and Setter. Select every field you want to encapsulate\, then press Enter or click OK. Great\, uh ? You can do the same for the Constructor.)

#hint(In general\, remember those three shortcuts in IntelliJ:
    - Alt+Enter on a symbol to show contextual actions on the symbol (something underlined in red generally has an easy fix via this menu).
    - Alt+Insert to generate code
    - Ctrl+Shift+A opens the Execute Action menu ; from here\, just type whatever you want to do\, like "reformat "\, and IntelliJ will list the appropriate actions.
    Know your tools!)

- Add validation to the new models
- Add a __IngredientDao__ interface that exposes the same functions as the __UserDao__, but for Ingredients.
- Add a __IngredientService__ that exposes the same functions as the __UserService__, but for Ingredients.
    - Add a new function to the service, `void incPriceOfEveryIngredient(String category)`, which does what it says, no traps.
- Implement the service and the Dao
    - The function we added to the __IngredientService__ __must be transactional__!

Here we illustrate the fact that storing each object individually is __not related__ to how we manipulate those objects in our domain: the service knows that it has to increase the prices per category, but the Dao doesn't. The Dao just accesses the database and serializes our Java objects.
Basically, the service represents what __process you apply to the data__, like manipulating a set of related objects, modifying their actual values depending on our application needs, whereas the Dao __just transforms our objects into database related dialect (hsql, postgres, etc...) and back__.

#warn(You'll also have to repeat Step 02 for the Ingredient class. Be careful at how you handle the `attributes` list! An ingredient __has one or many attrs__\, and each attr __can have zero or more__ associated ingredient.)

#newpage
#Step 04 - Authentication and Authorization

We now have everything setup. From linking our views to our code to validating data and inserting / retreiving it from a database. But for now, anyone can access anything. This is bad. It means almost anyone, if he just opens up the Dev Tools of any browser, can watch what queries we make, to which ip, and how the request is formed. From this, that person could just send requests to our server and maybe steal or break things.
Even if we assume everyone is nice and no one will attack our server (which is a nice fantasy), some routes must not be accessible by anyone. For instance, what would be the use of being able to query `getAll()` from our User table ? Who, apart from an admin or the application itself (which generally is its own admin), would need such a list ?

This is where Authentication and Authorization come into play:

- __Authentication__ just means a way to identify a user. There are several ways to Authenticate, like __Basic Auth__, __Digest Auth__, __Cookies__, __OAuth 1 and 2__, __Tokens__, __Request Signature__ etc...
- __Authorization__ is the complement of __Authentication__: Once our user is identified, what can he do ? This is completely domain specific, but the most common pattern is to have 3 categories:
    - __NotLogged__: The user hasn't logged in yet, he's just a basic viewer and has the fewer rights on the website.
    - __User__: The User is logged and can access a first layer of secured data, like his profile, he can make requests to the server by using the webapp interface, etc...
    - __Admin__: The super-user or set of super-users: this group can usually do anything on the website, the server, and anything in between.

So, here, we are going to use __Basic Auth__ and __Role based Authorization__ ; basically everything you are used to: enter a username and a password to __Authenticate__, and then associate one or more __Roles__ to the user to determine which routes he can call. Once again, we shouldn't need to modify any existing class except one: here, it's the Service. We are going to follow the same roles we saw in the previous list, except that the __NotLogged__ role doesn't really exist, it's just 'nothing', since the user isn't logged yet.

- __Secure__ these __User__ routes (create them if necessary):
    - `findAll` should be called only by ADMINs.
    - `delete` too.
    - `getById` should only be queried by a logged user, and you should verify that the returned object is the one belonging to the user making the query.
    - `create` should be used only by ADMINs.
    - `update` should be called only by the user requesting to change his profile, or by the ADMINs (as you wish).
- Follow the same pattern for __Ingredient__, except for `findAll`: anyone can list the ingredients, it is not sensible data.
    - Only an ADMIN can `incPriceOfEveryIngredient` in a category.

Now, try to query your webapp with Postman (GUI) or Curl (CLI), and verify that you get 403 - Forbidden responses where you should. This is a good time to try to break everything by sending stupid requests: you shouldn't manage to break anything.

#warn(This is one of the most important parts of your webapp. You can have the most beautiful front\, with excellent features and an awesome implementation\, it will be for nothing if any malicious script can take your server down or steal your customers' data. Just to reiterate\, even if you think your website is not used enough to be attacked\, it __will be attacked anyway__. Automated scripts browse the whole internet in search of easily stolen data\, and try all the most basic attack strategies. Those attacks are well known by most of the companies developing Web Frameworks and are generally easily countered by just using your Framework correctly ; Spring isn't an exception. For the most complicated attacks\, which are often specifically directed at your website\, you'll probably need the help of an expert. But if you come to be the target of dedicated attacks\, you're either maintaining a huge application\, which would mean you can afford to have some experts working on security\, or you've done something terribly wrong to the wrong person. In either cases\, protecting against the most basic of attacks is __your job__\, even if you do not plan to specialize in web security.)

#newpage
#Step 05 - Views

We're finally close to having a complete, secure, maintainable and extensible webapp! That's a lot of adjectives, but they are the good kind of adjective, the ones any company would want to hear about their website. 'Complete' and 'Secure' might be obvious, but 'maintainable' and 'extensible' are too often forgotten or not done correctly. Here, using Spring and __separating every concern__ greatly contributes to it, but most of it comes from how you name your functions, your attributes, how you organize your packages, etc... This is the reason why we designed interfaces before concrete implementations, why we decided to follow WebMVC standards, why we separated everything into packages etc... In short, just general good practices. Remember to always follow them!#br

Now, we just need some views to reflect the modifications we've done in Java.

- Modify your 3 main pages (index, userForm and userInfo) to reflect the Auth settings we put into place if not already done.
- Add links on your main page to allow __a logged user__ to compose his own brew using the __Ingredient__ class and table. The actual composition process is up to you, but you must at least:
    - Enter a name for the new batch.
    - Be able to chose ingredients in each category (filter them).
    - Be able to mix different amounts of each ingredient.
    - Show a result of the batch in some sort of 'badge': its name, its composition, its creator, etc...
- Add links on your main page to allow __an ADMIN__ to:
    - Modify the info of each individual ingredient.
    - Create new ingredients
    - Call the very very useful `incPriceOfEveryIngredient` transaction we described earlier.

Once again, the design is up to you, and once again, I recommend MDL or Bootstrap (MDL feels a bit cleaner in my opinion).

#newpage
#Conclusion

Right, we finally have something that looks very much like a professionnal website! Tomorrow, we're going to dive a little deeper in what an API really is, and what good practices we should follow.
