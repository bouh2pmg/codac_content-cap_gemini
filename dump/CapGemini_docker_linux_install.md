---
module:			Cap Gemini
title:			Docker CE
subtitle:		Linux install
background:		'../background/EpitechCapBackground.png'

noFormalities:	true
author:			Pauline FREY & Teddy FONTAINE & Vincent TRUBESSET
version:		1.0
---

#space(1)
Docker is the worlds leading software container platform.

#space(1)
* Developers use Docker to eliminate works on my machine problems when collaborating on code with co-workers.
* Operators use Docker to run and manage apps side-by-side in isolated containers to get better compute density.
* Enterprises use Docker to build agile software delivery pipelines to ship new features faster, more securely and with confidence for both Linux and Windows Server apps.

#What is a Container?

Using containers, everything required to make a piece of software run is packaged into isolated containers.

#space(1)
Unlike VMs, containers do not bundle a full operating system only libraries and settings required to make the software work are needed.

#space(1)
This makes for efficient, lightweight, self-contained systems and guarantees that software will always run the same, regardless of where it's deployed.

#space(1)

#hint(If you want more informations about containers see "[What is a container?](https://www.docker.com/what-container)")

#newpage

#Ubuntu

#imageCenter(/res/ubuntu.png, 128px, 5)

#hint(Ubuntu package manger: __apt-get__)

##Prerequisites

To install Docker CE, you need the 64-bit version of one of these Ubuntu versions:

#space(1)

* Yakkety 16.10
* Xenial 16.04
* Trusty 14.04

#space(1)

#hint(See [the best way to run Docker on Ubuntu](https://store.docker.com/editions/community/docker-ce-server-ubuntu))

#newpage

#Debian

#imageCenter(/res/debian.png, 128px, 5)

#hint(Debian package manger: __apt-get__)

##Prerequisites

Docker CE is supported on the following 64-bit Debian versions:

#space(1)

* Debian stretch (testing)
* Jessie 8.0
* Wheezy 7.7

#space(1)

#hint(See [the best way to run Docker on Debian](https://store.docker.com/editions/community/docker-ce-server-debian))

#newpage

#Fedora

#imageCenter(/res/fedora.png, 128px, 5)

#hint(Fedora package manger: __dnf__)

##Prerequisites

Docker CE is supported on the following 64-bit Fedora versions:

#space(1)

* Fedora 25
* Fedora 24

#space(1)

#hint(See [the best way to run Docker on Fedora](https://store.docker.com/editions/community/docker-ce-server-fedora))

#newpage

#CentOS

#imageCenter(/res/centos.png, 128px, 5)

#hint(Fedora package manger: __yum__)

##Prerequisites

Docker CE is supported on CentOS 7.3 64-bit.

#space(1)

#hint(See [the best way to run Docker on CentOS](https://store.docker.com/editions/community/docker-ce-server-centos))

#newpage

#Other Linux ?

#imageCenter(/res/gecko.png, 500px, 1)

#space(1)

#warn(Call a gecko !!)

#newpage

#Next steps, play with Docker !

Download the Dockerfile **capgemini_debian** or **capgemini_alpine** for the adventurers.
Build an image from it. #br
Then, run a container from that image and install **emacs** inside. #br
Commit your changes to the image and launch a new container.
You should be able to use **emacs** without reinstalling it. #br

#hint(Refer to the Docker Cheat Sheet for additional information.)