---
module:			Cap Gemini
title:			Docker
subtitle:		Windows install < 10
background:		'../background/EpitechCapBackground.png'

noFormalities:	true
author:			Pauline FREY & Teddy FONTAINE & Vincent TRUBESSET
version:		1.0
---

#space(1)
Docker is the worlds leading software container platform.

#space(1)
* Developers use Docker to eliminate works on my machine problems when collaborating on code with co-workers.
* Operators use Docker to run and manage apps side-by-side in isolated containers to get better compute density.
* Enterprises use Docker to build agile software delivery pipelines to ship new features faster, more securely and with confidence for both Linux and Windows Server apps.

#What is a Container?

Using containers, everything required to make a piece of software run is packaged into isolated containers.

#space(1)
Unlike VMs, containers do not bundle a full operating system only libraries and settings required to make the software work are needed.

#space(1)
This makes for efficient, lightweight, self-contained systems and guarantees that software will always run the same, regardless of where it's deployed.

#space(1)

#hint(If you want more informations about containers see "[What is a container?](https://www.docker.com/what-container)")

#newpage

#Docker Toolbox on Windows

Legacy desktop solution. Docker Toolbox is for older Windows systems that do not meet the requirements of Docker for Windows.

##Get Docker Toolbox for Windows

#warn(Make sure your Windows system supports Hardware Virtualization Technology and that virtualization is enabled.)

#hint([Get Docker Toolbox for Windows](https://download.docker.com/win/stable/DockerToolbox.exe))

##Install Docker Toolbox

In this section, you install the Docker Toolbox software and several __helper__ applications.
The installation adds the following software to your machine:

#space(1)

* Docker Client for Windows
* Docker Toolbox management tool and ISO
* Oracle VM VirtualBox
* Git MSYS-git UNIX tools

#space(1)

Double-click __DockerToolbox.exe__ to run the installer and follow all steps.

##Run

Click on the __Docker Quickstart Terminal__ icon to launch a Docker Toolbox terminal, and try out some Docker commands!

#space(1)

#terminal(docker version
#prompt
docker run hello-world)

#newpage

#Next steps, play with Docker !

Download the Dockerfile **capgemini_debian** or **capgemini_alpine** for the adventurers.
Build an image from it. #br
Then, run a container from that image and install **emacs** inside. #br
Commit your changes to the image and launch a new container.
You should be able to use **emacs** without reinstalling it. #br

#hint(Refer to the Docker Cheat Sheet for additional information.)
