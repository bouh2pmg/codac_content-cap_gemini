---
module:			Cap Gemini
title:			Docker
subtitle:		Cheat Sheet
background:		'../background/EpitechCapBackground.png'

noFormalities:	true
author:			Vincent TRUBESSET
version:		1.O
---

# Introduction
Docker is a container platform. Unlike VMs, containers do not bundle a full operating system - only libraries and settings required to make the software work are needed. #br
This makes for efficient, lightweight, self-contained systems and guarantees that software will always run the same, regardless of where it’s deployed. #br
As you will use Docker from now on, we provide you this cheat sheet that will help you in your daily use, and will most certainly save you countless hours of research, struggle and frustration.

#newpage

# Build a Docker image from a Dockerfile
Docker can build images automatically by reading the instructions from a Dockerfile, a text document that contains all the commands to assemble an image. #br

#terminal(docker build ---file DOCKERFILE ---tag IMAGE_NAME .)

`--file`: option to specify a Dockerfile path.
`--tag`:  option to give a name to the resulting image, allowing you to easily retrieve, run and update it. #br

#warn(The `"."` at the end is extremely important: it will give a build context to Docker. Don't forget it!)

# Run your container from a previously created image

Once you created your base image, you can run a container from it. Here, we'll specify a shared directory between your machine and the container, and set the container's working directory to it. #br

#terminal(docker run ---interactive ---tty ---volume PATH:/shared ---workdir="/shared" IMAGE_NAME)

`--interactive`: option to keep the container in foreground with the command line available. #br
`--tty`: option to allocate a pseudo TTY - the console environment to be brief - so the container can run properly in the terminal. #br
`--volume`: option to mount a volume on the container, here used to setup a shared directory. Replace `PATH` by the folder you want to share. The `:/shared` suffix specify where the folder will be located in the container. #br
`--workdir`: option to set the working directory of the container at launch.

# Save your modifications and retrieve them

By default, a container is volatile: all changes you made are forgiven when you exit, except for the shared folder. #br
If you need to save your changes, you will first need to get the `ID` from the container. Then you will be able to commit your changes to the image. #br
That means the next time you will run a container from the image, it will have all the modifications you made before comitting. #br

#terminal(docker ps
CONTAINER ID ...
abcdef012342 ...
#prompt docker commit abcdef012342 IMAGE_NAME)

#hint(`docker ps` will list all the running containers.)

#warn(Don't omit the `IMAGE_NAME`\, otherwise your image won't be updated and your changes won't persist.)

#newpage

# I accidentally closed my container, how do I retrieve my changes?!

Don't panic: you can retrieve the latest container ID and commit its changes to the image. Then, you can run a new container from the freshly updated image and continue to work. #br

#terminal(docker ps ---latest
CONTAINER ID ...
abcdef012342 ...
#prompt docker commit abcdef012342 IMAGE_NAME)

`--latest`: option to show the latest created container, whether it is running, stopped or closed. #br

___