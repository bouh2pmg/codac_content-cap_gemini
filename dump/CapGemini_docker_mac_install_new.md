---
module:			Cap Gemini
title:			Docker
subtitle:		Mac install >= El Capitan 10.11
background:		'../background/EpitechCapBackground.png'

noFormalities:	true
author:			Pauline FREY & Teddy FONTAINE & Vincent TRUBESSET
version:		1.0
---

#space(1)
Docker is the worlds leading software container platform.

#space(1)
* Developers use Docker to eliminate works on my machine problems when collaborating on code with co-workers.
* Operators use Docker to run and manage apps side-by-side in isolated containers to get better compute density.
* Enterprises use Docker to build agile software delivery pipelines to ship new features faster, more securely and with confidence for both Linux and Windows Server apps.

#What is a Container?

Using containers, everything required to make a piece of software run is packaged into isolated containers.

#space(1)
Unlike VMs, containers do not bundle a full operating system only libraries and settings required to make the software work are needed.

#space(1)
This makes for efficient, lightweight, self-contained systems and guarantees that software will always run the same, regardless of where it's deployed.

#space(1)

#hint(If you want more informations about containers see "[What is a container?](https://www.docker.com/what-container)")

#newpage

#Docker CE for Mac

Docker CE for Mac is an easy-to-install desktop app for building, debugging, and testing Dockerized apps on a Mac. #br
Docker for Mac is a complete development environment deeply integrated with the Mac OS Hypervisor framework, networking, and filesystem. #br
Docker for Mac is the fastest and most reliable way to run Docker on a Mac.

#Get Docker CE for Mac

#hint([Get Docker CE for Mac (stable)](https://download.docker.com/mac/stable/Docker.dmg))

##Install

Double-click **Docker.dmg** to start the install process.
When the installation completes, Docker starts automatically. #br

The whale in the top status bar shows that Docker is running, and accessible from a terminal.

##Run

Open a command-line terminal, and try out some Docker commands. #br

#terminal(docker version
#prompt
docker run hello-world)

##Enjoy

Docker CE for Mac plays nicely on the desktop and command-line. #br
You get the full Docker toolset, with many options configurable through the UI.

#imageCenter(/res/d4mac-artboard.png, 500px, 5)

#Share a folder between your machine and a container

#space(1)

#imageCenter(/res/d4mac-shared.png, 400px, 5)

#newpage

#Next steps, play with Docker !

Download the Dockerfile **capgemini_debian** or **capgemini_alpine** for the adventurers.
Build an image from it. #br
Then, run a container from that image and install **emacs** inside. #br
Commit your changes to the image and launch a new container.
You should be able to use **emacs** without reinstalling it. #br

#hint(Refer to the Docker Cheat Sheet for additional information.)