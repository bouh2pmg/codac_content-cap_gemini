---
module:			Cap Gemini
title:			Docker
subtitle:		Mac install < El Capitan 10.11
background:		'../background/EpitechCapBackground.png'

noFormalities:	true
author:			Pauline FREY & Teddy FONTAINE & Vincent TRUBESSET
version:		1.0
---

#space(1)
Docker is the worlds leading software container platform.

#space(1)
* Developers use Docker to eliminate works on my machine problems when collaborating on code with co-workers.
* Operators use Docker to run and manage apps side-by-side in isolated containers to get better compute density.
* Enterprises use Docker to build agile software delivery pipelines to ship new features faster, more securely and with confidence for both Linux and Windows Server apps.

#What is a Container?

Using containers, everything required to make a piece of software run is packaged into isolated containers.

#space(1)
Unlike VMs, containers do not bundle a full operating system only libraries and settings required to make the software work are needed.

#space(1)
This makes for efficient, lightweight, self-contained systems and guarantees that software will always run the same, regardless of where it's deployed.

#space(1)

#hint(If you want more informations about containers see "[What is a container?](https://www.docker.com/what-container)")

#newpage

#Docker Toolbox on Mac

Legacy desktop solution. Docker Toolbox is for older Max systems that do not meet the requirements of Docker for Mac.

##Get Docker Toolbox for Mac

#hint([Get Docker Toolbox for Mac](https://download.docker.com/mac/stable/DockerToolbox.pkg))

##Install Docker Toolbox

Install Docker Toolbox by double-clicking the package or by right-clicking and choosing __Open__ from the pop-up menu and follow all steps.

##Run

Click on the __Docker Quickstart__ icon to launch a Docker Toolbox terminal, and try out some Docker commands!

#space(1)

#terminal(docker version
#prompt
docker run hello-world)

#newpage

#Next steps, play with Docker !

Download the Dockerfile **capgemini_debian** or **capgemini_alpine** for the adventurers.
Build an image from it. #br
Then, run a container from that image and install **emacs** inside. #br
Commit your changes to the image and launch a new container.
You should be able to use **emacs** without reinstalling it. #br

#hint(Refer to the Docker Cheat Sheet for additional information.)
