---
module:			Cap Gemini
title:			Docker CE
subtitle:		Windows install >= 10
background:		'../background/EpitechCapBackground.png'

noFormalities:	true
author:			Pauline FREY & Teddy FONTAINE & Vincent TRUBESSET
version:		1.0
---

Docker is the worlds leading software container platform.

#space(1)
* Developers use Docker to eliminate works on my machine problems when collaborating on code with co-workers.
* Operators use Docker to run and manage apps side-by-side in isolated containers to get better compute density.
* Enterprises use Docker to build agile software delivery pipelines to ship new features faster, more securely and with confidence for both Linux and Windows Server apps.

#What is a Container?

Using containers, everything required to make a piece of software run is packaged into isolated containers.

#space(1)
Unlike VMs, containers do not bundle a full operating system only libraries and settings required to make the software work are needed.

#space(1)
This makes for efficient, lightweight, self-contained systems and guarantees that software will always run the same, regardless of where it's deployed.

#space(1)

#hint(If you want more informations about containers see "[What is a container?](https://www.docker.com/what-container)")

#newpage

#Docker CE for Windows

Docker CE for Windows is Docker designed to run on Windows 10.
It is a native Windows application that provides an easy-to-use development environment for building, shipping and running dockerized apps.

#space(1)

Docker CE for Windows uses Windows-native Hyper-V virtualization and networking and is the fastest and most reliable way to develop Docker apps on Windows.

#space(1)

Docker CE for Windows supports running both Linux and Windows Docker containers.

#Get Docker CE for Windows 10

#hint([Get Docker CE for Windows (stable)](https://download.docker.com/win/stable/InstallDocker.msi))

##Install

Double-click __InstallDocker.msi__ to run the installer.

When the installation finishes, Docker starts automatically.

#space(1)

The whale in the notification area indicates that Docker is running, and accessible from a terminal.

##Run

Open a command-line terminal like PowerShell, and try out some Docker commands!

#space(1)

#terminal(docker version
#prompt
docker run hello-world)

##Enjoy

Docker is available in any terminal as long as the Docker CE for Windows app is running.

#space(1)

Settings are available on the UI, accessible from the Docker whale in the taskbar.

#space(1)

#imageCenter(/res/d4win-artboard.png, 400px, 5)

#newpage

#Share a folder between your machine and a container

#space(1)

#imageCenter(/res/d4w-shared-drives.png, 500px, 0)

#newpage

#Next steps, play with Docker !

Download the Dockerfile **capgemini_debian** or **capgemini_alpine** for the adventurers.
Build an image from it. #br
Then, run a container from that image and install **emacs** inside. #br
Commit your changes to the image and launch a new container.
You should be able to use **emacs** without reinstalling it. #br

#hint(Refer to the Docker Cheat Sheet for additional information.)
