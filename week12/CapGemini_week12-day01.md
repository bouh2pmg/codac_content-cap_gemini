---
module:			Cap Gemini
title:			Job Board
subtitle:		Web project
background:		'../background/EpitechCapBackground.png'

repository: 	job_board
repoRights:		gecko
language:		HTML, javascript, AJAX, jQuery, Java
groupSize:		1

noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---


This project aims at realising

- a database to store job advertisements,
- a webpage ($front end$) using Javascript technologies to display an online job advertisements board,
- an application ($back end$) in Java to feed the job advertisements database.

There are several steps, that must be completed one after the other.

#warn(Mind out! Each step depends on the previous one...)

For each step, create a directory in your repository named **Step XX** that contains all the necessary files.

#hint(When you complete a step\, duplicate the files in a new folder as a base for the next step.)


All your files must be tested on your local host.



#Step 01

Create an SQL database to store job advertisements.
It must contain at least:

- a table to stock advertisements,
- a table to stock companies,
- a table to stock people (whether in charge of an advertisement or applying to an advertisement),
- a table to keep information about a job application (referencing the mails sent, the people concerned, the ad concerned,...).

#hint(You can add as many relevant tables as you wish to\, and add the fields you want in these tables.)




#Step 02

Write a HTML/CSS page showing several job advertisements.
For each ad, there must be at least a place for a title, one for a short description, and a "learn more" button.
Clicking on this "learn more" button does not have any action for the moment.

#hint(The design of this page is up to you\, but do not spend too much time on it now.)




#Step 03

The "learn more" button must now display all the available information about the ad (full description, wages, place, working time,...), without reloading the page.
No popup.#br

Keep the database fields coherent with the information you display on the HTML page.

#hint(Once again\, the design is up to you: put this data wherever you like.)




#Step 04

Add SQL requests to fetch information directly from your database.

#hint(Fill in the database manually.)



#Step 05

Create a small Java application with a nice GUI to visualize the database content.
It must be able to modify job advertisements, delete or add some.

#hint(I suggest you inquire about java.sql package....)

#hint(The conception is up to you. Make it simple\, but respect OO principles!)




#Step 06

On your webpage, add an "Apply" button for each ad.
When this button is clicked, it opens a form to 

- enter information about you (name, email, phone,...)
- send a message to the owner of the ad (you).

This action must be saved in the database.



#Step 07

Add an authentification on the webpage.
When identified, you don't have to fill in your personal information to apply to a job.

#hint(You need a **connection** page and another one to create/modify an account.)



#Step 08

Now you can put a vernish layer by nicely designing your pages, tweaking and refining the style sheets.
