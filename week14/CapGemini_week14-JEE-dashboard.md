---
module:			Cap Gemini
title:			week14-dashboard
subtitle:		JEE - Complete project
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_dashboard
repoRights:		gecko
language:		Java, XML, Roo, JSP
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

Welcome back! This week, we are not going to ask you to follow steps with clear instructions, you'll have to develop a project from scratch, using everything you know. This obviously doesn't mean you can't ask for help; it just means you'll have to make choices.#br

We've seen how to setup a basic project and use the different technologies JEE puts at our disposal, but we just scratched the surface. Use this project to go deeper and produce production ready code, with good abtractions where they are needed. Extensibility is not an option, nor is clean, readable and maintainable code.#br

#newpage
#Introduction

The goal of this project is to create a kind of dashboard where a user can regroup whatever website he wants and have the latest news shown to  him. For instance, imagine an interface where you log in, then pick some services like Facebook, Google+, Reddit etc..., log in to those services, and then can, on the same page, have access to a set of functionalities for each of those services; this is what we expect you to realize during the week.

You'll have to use Spring and JSP, but you are free to use whatever front framework you like. Once again, the graphic design is up to you, but please do not lose too much time on it, it is not the most important part of the project!

#newpage
#The project

As mentionned before, you are to develop a Dashboard.#br
What we mean by Dashboard is a place where you regroup different services, parse their data, and show it depending on a user's preferences. Take a look at [this dashboard](https://app.klipfolio.com/published/27eec8a142cb92a1abb86562371dbbc0/social-media-command-centredf7f06fcfb19b29d6279133b22efc143) on Klipfolio: this is close to what we want to achieve.

The example shown here is oritented towards the number of 'likes' and its evolution: it gets a user info from the related service, manipulates the data to correspond to what they want, and then shows something completely different to the user (i.e it is not just the faebook page included inside the app, they took the data and displayed it differently). We want you to achieve the same kind of webapp, although this particular example is very static, we can't add a service or modify anything. You are obvisouly asked to avoid this kind of limitations.

Most of the work we ask for this project HAS TO come from you. We won't give you instructions about every step, nor will we tell you what pattern or concept you should use where. Nevertheless, there are some rules you HAVE TO follow. Apart from those rules, you are free to implement however you want, but keep in mind that we will review your work, and judge its extensibility, maintanability, and relevance. Do not implement useless features, do not forget about abstraction just because __you know you won't need it__ (it's a lie 99.99% of the time), or because __you're only two people working on the project__, you never know, and applying strict design rules from the start is __always__ (I insist, __always__) a good idea. Plus, this is an exercise, meaning it has strict validation rules and considerations other than 'is it necessarly the best way to proceed' (although, in this case, it might very well be).

#newpage
#Setup

As you might have guessed, all the setup we did for last week is to be done again this week. Thankfully, Spring offers a CLI (Command Line Interface) tool that helps generate configuration files and code: [Spring Roo](https://projects.spring.io/spring-roo/). Use it, and read the docs, it will most likely save you a lot of time.

#hint(If you are not sure what Roo does when issueing commands\, either go back to last week\, read the docs\, or ask an assistant\, but don't just generate code you won't understand.)

#newpage
#The Basics

The main part of this project is having a way to handle different services in the same webapp for a particular user. Thus, you'll __HAVE TO__ implement one way or the other:

- A register / login / logout system.
- An internal Authorization system, to filter which user can do what.
- A way to use external authorization systems, such as OAuth2, Basic Auth, etc... to be able to connect to other services and retrieve user related data.
- A 'main area', that will show info about the selected services, according to user preferences.

This is what we expect in order for your project to be considered valid. But this is just the most basic functionalities of the full project!

#newpage
#Advanced

Once you have all the basics setup and working, you can start implementing the interesting part:

- Find a way to represent an external service as a unique entity, using class inheritance and smart abstractions.
- Add a way for the user to add a service from a list of services. Following the previous point __will__ help you.
- Add a way to tag a service (Favorite, Social, Work, etc...)
- Add filters to the main area, to sort services:
    - Alphabetically
    - By number of updates
    - By tag
- Add a way to interact with the services you show (Like a post for Facebook or Google+, post a comment for Github, etc...) __directly inside your app__.
- Add a notification system to receive the external services notifications. You __HAVE TO__ use __WebSockets__, or something similar, but using HTTP Requests or equivalent is __FORBIDDEN__.
- Add a search bar, to search through your services:
    - By name
    - By tag

#newpage
#Very advanced

This part will be considered as bonus.

#warn(You are entitled to bonuses ONLY IF you have all the basic and advanced steps.)

- Add a way to theme you entire application, and allow the user to change theme.
- Add a way to extract certain info from a service.
    - This is quite complicated if you didn't split concerns correctly __from the beginning__. You have to abtract what is a 'relevant info', the methods to retrieve them, and the methods to print them to the user.
    - Although complicated, this point is extremely important if you want to learn how to correctly use inheritance, abtractions, and good Object Oriented design.
    - For example, I'd want to be able to see how many stars I've won since my last visit (for an hypothetical Github module).

Feel free to implement any other bonus you find relevant, but only once you have everything mandatory part!

#newpage
#General Considerations

- Unreadable code will result in failure. Indent your lines, do not stack everything together, make your code easy to read and understand. Use your IDE format tool! Most of the time, you can configure it in the settings to fit your needs
- Non relevant symbol names will result in failure. For instance, `int a = 15` __is not a good practice__. What does `a` mean ? `int age = 15` is infinitely better.
- Impossibility to compile and package your app through a build system (Maven, Gradle ...) will result in failure. We have to be able to type `mvn package` or equivalent from a shell to package the app.
- Bad to no design at all will result in failure. Take time to design your app (not graphically, code-wise) __BEFORE__ implementing everything. You will save hours of debugging in the end. Regroup concepts together, always ask yourself 'Am I re writing this piece of code ?'. If the answer is yes more than twice for a particular piece of code, you __SHOULD HAVE__ regrouped some code into functions, or functions into a class.
- Look at Java Generics. It will help you __a lot__ when designing sets of functionalities that only differ on which type they manipulate.
- Ask your Geckos, or [mail the designers](mailto:lab.koala@epitech.eu), but do not stay stuck on a step!
