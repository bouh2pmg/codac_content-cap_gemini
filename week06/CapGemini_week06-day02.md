---
module:			Cap Gemini
title:			day 04
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d04
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ
version:		1.1
---


Let's delve again deeper into OOP.
Today, you will keep using all of the previous' days concepts, and you will also discover a few more concepts:

- static keyword with methods (also called "static method")

- inheritance

- protected visibility

Here is a quick example of inheritance.#br

Let's imagine an "Apple" class inheriting from a "Fruit" class.
This "Apple" class will have its own method, like "Peel" or "Make an apple pie" but will also possess all of the methods and attributes that weren't private (protected or public) in its mother class (Fruit); for example, an "Eat" method.#br

We can then imagine another Class, "Banana", which also inherits from the "Fruit" class and has all of the attributes and methods that weren't private in the mother class.
It has its own methods and attributes.#br

Finally, inheritance is limitless. In this example we could have a "Golden Apple" class inheriting from the  "Apple" class.
It would inherit all of the properties that weren't private in the "Apple" class, which means that it would obviously also inherit the "Fruit" class characteristics).#br

And this could go on and on.

#warn(Inheritance is probably the most important notion in OOP.)

On a special note, sadly, in JAVA, it is not possible to inherit directly from 2 classes at the same time (you actually need to trick the language... because, JAVA) which means that it would not be possible to directly represent a Tangelo (a cross between Tangerines and Grapefruits. Google it).#br

We **strongly** advise you to read examples on the Internet attentively (even more than usual) before starting Exercise 03.

#warn(Unless specified otherwise\, all messages must be followed by a newline.)

#warn(Unless specified otherwise\, the getter and setter name will always follow this format: **getAttribute** or **setAttribute**.
If you attribute's name is **someWeirdNameAttribute**\, its getter will be **getSomeWeirdNameAttribute** (FYI\, this name convention is known as "CamelCase").)





#newpage

# Exercise 01 #hfill 1pt

**File to hand in**: pool_java_d04/ex_01/Animal.java#br

Create a new "Animal" class that has 3 const attributes, initialized as follows:
#space(1)__MAMMAL__ = 0, #space(1)__FISH__ = 1, #space(1)__BIRD__ = 2.#br

These variables will be used to pass our Animal's type to the constructors, which takes 3 mandatory parameters:

1. the name of our animal,

2. its number of legs,

3. its type, among the three previously-created attributes (MAMMAL, FISH, BIRD).


These 3 parameters must be stored inside new attributes, respectively named "name", "legs" and "type".
Create getters for each of these attributes.#br

Even though accidents can happen, we are generous Gods (well, at least, I am!) and will consider that our animals will never lose a leg after creation (and also, because I love cats).
That's why there is no setter for this attribute.
For the other attributes, you will need to think about it by yourself because there are some logical reasons for it.

#warn(Be careful\, the getter for "type" is a bit tricky.
You will need to make it work like in this example (and just guess for the other cases).)

Last, but not least, during its creation our Animal must say:

	My name is [name] and I am a [type]!

where [name] is the name of the Animal and [type], its type.

```java
	public class Example {
		public static void main(String[] args) {
		Animal isidore = new Animal("Isidore", 4, Animal.MAMMAL);

		System.out.println(isidore.getName() + " has " + isidore.getLegs()
				   + " legs and is a " + isidore.getType());
		}
	}
```
#terminal(java Example
My name is Isidore and I am a mammal!
Isidore has 4 legs and is a mammal.)









# Exercise 02 #hfill 1pt

**File to hand in**: pool_java_d04/ex_02/Animal.java#br

Copy your "Animal.java" file from the previous exercise.
Implement a static method in this class called **getNumberOfAnimals**, that  returns the number of "Animal" instances and displays it in a sentence like the following:

	There is(are) currently [x] animal(s) in our world.

where [x] is the number of currently alive animals.#br

The method to handle this is up to you.#br

You must also implement 3 variants of this function: **getNumberOfMammals**, **getNumberOfFish** and **getNumberOfBirds**.
They respectively return the number of mammals, fish and birds that are alive in the world and display the following message:

	There is(are) currently [x] [type](s) in our world.

where [x] is the number of animals of this type that are currently alive, and [type] is the type of animal of the function being called.
An example would be "There are currently 3 mammals in our world.".

#warn(In this exercise\, use plural when necessary.)

#hint(Animals never die.)








# Exercise 03 #hfill 1pt

**Files to hand in**: pool_java_d04/ex_03/Cat.java
#space(2.9)			 pool_java_d04/ex_03/Animal.java#br

Copy your "Animal.java" from the previous exercise.
Create a new "Cat" class that inherits from "Animal" and has a private "color" attribute, with a getter and a setter.#br

When "Cat" is created, display:

	[name]: MEEEOOWWWW

where [name] is...the name of the cat!#br

The name of the cat should always be specified as the first parameter of its constructor.
However, the color can be specified as its second parameter, but is not mandatory.
Its number of legs must be set to 4 and its type to MAMMAL by default.

#hint(If no color has been specified during creation, the default color will be grey.)

Add a public **meow** method to your "Cat" class.
This method does not take any parameters, and when calling it, it displays the following message:

	[name] the [color] kitty is meowing.

where [name] is the name of the cat and [color] its color.#br

```java
	public class Example {
		public static void main(String[] args) {
			Cat isidore = new Cat("Isidore", "orange");

			System.out.println(isidore.getName() + " has " + isidore.getLegs()
					   + " legs and is a " + isidore.getType());
			isidor.meow();
		}
	}
```

#terminal(java Example
My name is Isidore and I am a mammal!
Isidore: MEEEOOWWWW
Isidore has 4 legs and is a mammal.
Isidore the orange kitty is meowing.)











# Exercise 04 #hfill 1pt

**Files to hand in**: pool_java_d04/ex_04/Cat.java
#space(2.9)			 pool_java_d04/ex_04/Animal.java
#space(2.9)			 pool_java_d04/ex_04/Shark.java
#space(2.9)			 pool_java_d04/ex_04/Canary.java#br

Copy your classes from the previous exercise.
Create "Shark" and "Canary" class, which both inherit from "Animal".#br

When a "Shark" is created, display:

	A KILLER IS BORN!

The "Shark" class must also have a private attribute named "frenzy".#br

When the class is constructed, it receives its name as parameter.
Its number of legs should be set to 0 and its type to "FISH".
Its "frenzy" attribute should be on "false" by default.#br

Also add a **smellBlood** method to it.
It takes a Boolean as parameter and returns nothing.
This method changes the value of "frenzy" to the value passed as parameter.#br

Finally, you must add a **status** method that displays one of the two following messages, depending on frenzy:#br

	[name] is smelling blood and wants to kill.

if "frenzy" is true, and

	[name] is swimming peacefully.

if "frenzy" is false, where [name] is the shark's name.#br

Your "Canary" class must have a private "eggs" attribute, which indicates how many eggs it has laid in its life.
During initialization, "Canary" only takes one parameter: its name. 
"legs" is initialized to 2, "type" to BIRD, and "eggs" to 0.#br

During its creation, it must say:

	Yellow and smart? Here I am!

You must add a **getEggsCount** method that returns the number of eggs laid by the Canary.
And a **layEgg** method that increases the number of eggs laid by 1.

#newpage

```java
	public class Example {
		public static void main(String[] args) {
			Canary titi = new Canary("Titi");
			Shark willy = new Shark("Willy"); //Yes Willy is a shark here !

			willy.status();
			willy.smellBlood(true);
			willy.status();

			titi.layEgg();
			System.out.println(titi.getEggsCount());
     }
}
```

#terminal(java Example
My name is Titi and I am a bird!
Yellow and smart ? Here I am!
My name is Willy and I am a fish!
A KILLER IS BORN !
Willy is swimming peacefully.
Willy is smelling blood and wants to kill.
1)





#newpage

# Exercise 05 #hfill 1pt

**Files to hand in**: pool_java_d04/ex_05/Cat.java
#space(2.9)			pool_java_d04/ex_05/Animal.java
#space(2.9)			pool_java_d04/ex_05/Shark.java
#space(2.9)			pool_java_d04/ex_05/Canary.java#br

Copy your classes from the previous exercise.
Willy, our dear shark, needs to eat...
Add a public **eat** method to your "Shark" class.
It takes an "Animal" as parameter and returns nothing.
When the method is called, display:

	[Shark's name] ate a [Animal's type] named [Animal's name].

where [Shark's name] is the name of the shark,
#space(1.2)[Animal's type] is the type of the animal being eaten (mammal, fish, bird) 
#space(1.2)[Animal's name] is the name of the animal being eaten.#br

If the parameter can't be eaten, your shark must say:

	[name]: It's not worth my time.

where [name] is the name of the shark.#br

If the "Shark"'s "frenzy" attribute is set to true and our shark has successfully eaten, it must be set to false after calling this method.

#warn(Your shark will never be eaten itself.)







#newpage

# Exercise 06 #hfill 1pt

**Files to hand in**: pool_java_d04/ex_06/Cat.java
#space(2.9)			pool_java_d04/ex_06/Animal.java
#space(2.9)			pool_java_d04/ex_06/Shark.java
#space(2.9)			pool_java_d04/ex_06/Canary.java
#space(2.9)			pool_java_d04/ex_06/BlueShark.java
#space(2.9)			pool_java_d04/ex_06/GreatWhite.java#br

For this exercise, you need to create two new classes: "BlueShark" and "GreatWhite", which both inherit from the "Shark" class.
They both take their name during construction as a mandatory argument.#br

They are almost identical to the original "Shark' class, except that they are picky eaters. #br

"BlueShark" refuses to eat anything but "Fish".
"GreatWhite" refuses to eat "Canary". Even worse, if you try to feed a "Canary" to a "GreatWhite", it will say:

	[name]: Next time you try to give me that to eat, I'll eat you instead.

where [name] is the name of the GreatWhite.#br

Except for those minor differences, their "eat" function must do the exact same thing as for the "Shark" Class.#br

... Oh, I almost forgot!
If a GreatWhite eats another Shark, it must display:

	[name]: The best meal one could wish for.

where [name] is the name of the GreatWhite.#br

#hint(__Override__ keyword.)