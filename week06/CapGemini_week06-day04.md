---
module:			Cap Gemini
title:			day 05
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d05
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ
version:		1.1
---

Today you will delve even deeper into OOP.
You will keep using all the previous days' concepts, and also discover a few new things:

- Abstract class

- Abstract methods

- Interfaces

These are real cornerstones for Java language.#br

An abstract class is a class that can not be instantiated (which means that no object of this class can be created directly). 
To do so, one need to create a child class that inherits from this abstract class.
Abstract java classes require the "abstract" keyword.

#hint(As you will see\, it is extremely useful! It allows polymorphism (it is not a swear word\, you can [google it](https://www.google.fr/search?q=polymorphism+java)).)

An abstract method is a method without implementation.
It is not mandatory for an abstract class to contain abstract methods, but if a class contains abstract methods, it MUST be defines as anstract too.#br

An interface is a similar to a class with only abstract methods.
But it is not technically a class.
A class can implement one *or several* interface(s), thanks to the **implements** keyword.

#hint(By convention\, interfaces usually start with an uppercase 'i'\, and abstract classes usually start with an uppe-case 'a'. It is not mandatory and it could start with the letter in lower case\, or simply not start with those letters at all.)

#warn(Unless specified otherwise\, all messages must be followed by a newline.)

#warn(Unless specified otherwise\, the getter and setter name will always adding "get" or "set" in front of the name of the attribute\, in CamalCase.)




# Exercise 01 #hfill 1pt

**File to hand in**: pool_java_d05/ex_01/AWeapon.java#br

Create an "AWeapon" abstract class with the following attributes:

- **name** (a string): the name of the weapon,

- **apcost** (an integer): the action point cost to use the weapon,

- **damage** (an integer): the amount of damage dealt by the weapon,

- **melee** (a Boolean): true if the weapon is used for close combat false otherwise (false by default).

These attributes will have getters (**getName**, **getApcost**, **getDamage**, **isMelee**) but no setter.
This class' constructor will take the name, the apcost and the damage in this very order.#br

Also add a public method abstract named **attack**.

#warn(Be careful! You cannot instantiate an abstract class.)






# Exercise 02 #hfill 1pt

**Files to hand in**: pool_java_d05/ex_02/AWeapon.java
#space(2.9)	pool_java_d05/ex_02/PlasmaRifle.java
#space(2.9)	pool_java_d05/ex_02/PowerFist.java#br

Create these two classes, inheriting from "AWeapon", with the given features:

#twoColumns(.45, 

	PlasmaRifle

	Name: "Plasma Rifle"
	Damage: 21
	AP cost: 5
	Output of attack method: "PIOU"
	Melee: false
,

	PowerFist

	Name: "Power Fist"
	Damage: 50
	AP cost: 8
	Output of attack method: "SBAM"
	Melee: true
)

A call to attack() must display the specific output of the weapon followed by a newline.





# Exercise 03 #hfill 1pt

**File to hand in**: pool_java_d05/ex_03/IUnit.java#br

Let's create our first interface. It will be called IUnit.
Its goal is to determine the base methods that we want our units to implement when they are created.#br

Add a few methods to this interface in order to do that:

- A public method, **equip**, which takes an "AWeapon" as parameter.

- A public method, **attack**, which takes an "IUnit" as parameter.

- A public method, **receiveDamage**, which takes a int as parameter.

- A public method, **moveCloseTo**, which takes an "IUnit" as parameter.

- A public method, **recoverAP**, which takes no parameter.





# Exercise 04 #hfill 1pt

**Files to hand in**: pool_java_d05/ex_04/IUnit.java
#space(2.9) pool_java_d05/ex_04/AMonster.java
#space(2.9) pool_java_d05/ex_04/ASpaceMarine.java#br

Copy your interface from the previous exercise.
Now we will actually implement the base for our Monsters and our SpaceMarines, both abstract classes.#br

For both classes you must create three attributes: "name", "hp", and "ap".
The first one id the name of the unit, given during the construction of the unit.
The second is its health point, that depends on the type of unit (but initiated to 0 for the moment)
The third one is "action points", the resource that our unit uses to make an action. It also depends on the type of unit, but is initiated to 0 for now.
These attributes must have getters without setter (**getName**, **getHp** and **getAp**).#br

Let's now concentrate on the AMonster class.
Add a "damage" attribute and its getter, **getDamage**.
It must be set to 0 for the time being (and later will be set differently for each monster).
Also add an "apcost" attribute, also set to 0 for now.

## equip

Implement the **equip** method.
It must display:

	Monsters are proud and fight with their own bodies.

All monsters have a "melee" type, which means that they first need to get within melee range (see **moveCloseTo** method) before being able to attack their target.
So if the monster is not in the melee range of the Unit passed as parameter, display:

	[name]: I'm too far away from [target's name].

where [name] is the name of the monster and [target's name] its target's name.#br

If our monster is in melee range, it must check if it has enough AP to attack; that's when its "ap" and "apcost" attributes matter.
In order to attack, it should have more "ap" available than an attack's "apcost".#br

If the attack is successful you must deduct "apcost" from "ap" and call the target's **receiveDamage** method while passing the monster's "damage" as parameter.
You should display the following before calling **receiveDamage**:

	[name] attacks [target's name].


## receiveDamage

This **receiveDamage** function must always receive an integer representing the damage suffered by the monster.
The monster's HP must be reduced by this amount.

#warn(If the HP gets to 0 or below\, the monster must be considered dead and each of its methods (except the getter) should return false from that point on.)

This function will make the monster move closer to its target, which means that a later call to "attack" will be successful.
#hint(We will consider that the monster can only be close to one target at a time.)

If the monster is not already close to its target, a call to this function will display:

	[name] is moving closer to [target's name].


## recoverAP

A call to the **recoverAP** method increases the monster's AP by 7 at most. 
It should never go over 50.#br

#hint(The ASpaceMarine will be fully created in the following exercise. As of now\, it should just have what has been specified at the beginning of this exercise\, and all of the interface's methods should be empty (it should be possible to instantiate an ASpaceMarine as of now\, it just won't have all its functionalities yet).)






# Exercise 05 #hfill 1pt

**Files to hand in**: pool_java_d05/ex_05/IUnit.java
#space(2.9) pool_java_d05/ex_05/AMonster.java
#space(2.9) pool_java_d05/ex_05/ASpaceMarine.java
#space(2.9) pool_java_d05/ex_05/AWeapon.java
#space(2.9) pool_java_d05/ex_05/PlasmaRifle.java
#space(2.9) pool_java_d05/ex_05/PowerFist.java

Copy your classes from the previous exercises.#br

Let's continue creating our ASpaceMarine class. 
It should already have a few attributes.
Let's add a "weapon" attribute and its getter, **getWeapon**.

## equip

Our spaceMarine needs to take this new weapon and equip it.
If it's done successfully, display:

	[name] has been equipped with a [weapon's name].

where [name] is our SpaceMarine's name and [weapon's name] his weapon's name.#br

If the weapon has already been taken by another spaceMarine, the function will do nothing.
It's up to you to decide how to handle this.


## attack

If our SpaceMarine doesn't have any equipped weapons, the function do not do nothing but output:

	[name]: Hey, this is crazy. I'm not going to fight this empty-handed.

If the equipped weapon is a melee one and our SpaceMarine is not in range, he must say:

	[name]: I'm too far away from [target's name].

Like in AMonster, our SpaceMarine needs enough AP to attack.
If its available AP are at least equal to his weapon's cost, and if he is in range (or is using an in-range weapon), call the equipped weapon's **attack** method in addition to the target's **receiveDamage** method while passing the weapon's damage as parameter.#br

Also, if the attack has been successful, you should deduct the weapon's cost from the SpaceMarine's AP and display the following before calling **receiveDamage**

	[name] attacks [target\'s name] with a [weapon\'s name].


## receiveDamage
This function works exactly like AMonster.

## moveCloseTo
This function works exactly like AMonster.

## recoverAP
This function works exactly like AMonster, except that it will recover 9 AP instead of 7.





#newpage

# Exercise 06 #hfill 1pt

**Files to hand in**: pool_java_d05/ex_06/IUnit.java
#space(2.9) pool_java_d05/ex_06/AMonster.java
#space(2.9) pool_java_d05/ex_06/ASpaceMarine.java
#space(2.9) pool_java_d05/ex_06/AWeapon.java
#space(2.9) pool_java_d05/ex_06/PlasmaRifle.java
#space(2.9) pool_java_d05/ex_06/PowerFist.java
#space(2.9) pool_java_d05/ex_06/TacticalMarine.java
#space(2.9) pool_java_d05/ex_06/AssaultTerminator.java

Copy your classes from the previous exercises.#br

It is finally time to create our actual Space Marines.
As you should have guessed, all SpaceMarines need to inherit from the ASpaceMarine class.#br

Let's begin with TacticalMarine.
During its creation, he should say

	[name] on duty.

where [name] is his name, necessarly given during his creation.#br

During his creation, the TacticalMarine is equiped with a PlasmaRifle.
By default, a TacticalMarine has 100HP and 20AP.
Since a Tactical Marine is...tactical, it regenerates his AP faster than other Marines: he will actually take back 12 AP instead of 9, when **recoverAP** is called.#br

Let's talk about AssaultTerminor now.
When being created, he also receives his name and displays:

	[name] has teleported from space.

By default, he possess 150HP and 30AP.
During his creation, he is equiped with a PowerFist.
Also, since AssaultTerminator is a bit more resistant than other Marines, when his **receiveDamage** method is called, he reduces the damage by 3.

#warn(However\, the received damage can't be reduced under 1.)





# Exercise 07 #hfill 1pt

**Files to hand in**: pool_java_d05/ex_07/IUnit.java
#space(2.9) pool_java_d05/ex_07/AMonster.java
#space(2.9) pool_java_d05/ex_07/ASpaceMarine.java
#space(2.9) pool_java_d05/ex_07/AWeapon.java
#space(2.9) pool_java_d05/ex_07/PlasmaRifle.java
#space(2.9) pool_java_d05/ex_07/PowerFist.java
#space(2.9) pool_java_d05/ex_07/TacticalMarine.java
#space(2.9) pool_java_d05/ex_07/AssaultTerminator.java
#space(2.9) pool_java_d05/ex_07/RadScorpion.java
#space(2.9) pool_java_d05/ex_07/SuperMutant.java

Copy your classes from the previous exercises.#br

We will now finally create our Monsters!
As you should have guessed, all of the monsters classes must inherit from the "AMonster" class.#br

Our monsters will have generic names: they will all be called "RadScorpion" or "SuperMutant", followed by an id.
For example, the first RadScorpion will be called "RadScorpion #1", the second one will be "RadScorpion #2",...

#hint(Thus\, no need to give monsters any parameters when they are created.)

Let's create the RadScorpion first. When created, it displays:

	[name]: Crrr!

where [name] is its name.#br

RadScorpion is a fairly common monster, it only has 80HP.
It starts with the maximum AP, 50, and each one of its attacks deals out 25 damages, and costs 8 AP.
However, they can be pretty scary. That's why they will deal out double damage if they are attacking a marine, who is not an "AssaultTerminator" (because the other marines are too scared to run away).#br

Now let's create our SuperMutant. When created, it displays:

	[name]: Roaarrr!

It starts with 170HP and 20AP.
Each one of its attacks deals out 60 damages, but costs 20Ap.
When SuperMutant recover AP, they also recover HP, with a maximum of 10HP recovered by call (170HP being their full health).








# Exercise 08 #hfill 1pt

**Files to hand in**: pool_java_d05/ex_08/IUnit.java
#space(2.9) pool_java_d05/ex_08/AMonster.java
#space(2.9) pool_java_d05/ex_08/ASpaceMarine.java
#space(2.9) pool_java_d05/ex_08/AWeapon.java
#space(2.9) pool_java_d05/ex_08/PlasmaRifle.java
#space(2.9) pool_java_d05/ex_08/PowerFist.java
#space(2.9) pool_java_d05/ex_08/TacticalMarine.java
#space(2.9) pool_java_d05/ex_08/AssaultTerminator.java
#space(2.9) pool_java_d05/ex_08/RadScorpion.java
#space(2.9) pool_java_d05/ex_08/SuperMutant.java
#space(2.9) pool_java_d05/ex_08/SpaceArena.java

Create a SpaceArena class, to simulate fights between teams of Monsters and teams of SpaceMarines.#br

This class must have 3 methods:

- **enlistMonsters**
It takes an array of Monsters as parameter and add theses monsters to the one that is already registered to fight.

- **enlistSpaceMarines**
It takes an array of SpaceMarines as parameter and add them to the one that is already registered to fight.

#warn(It should not be possible to add a warrior that is already enlisted for a fight.
In case of doubloon\, the warriors placed before the cheater in the array should still be enlisted.
For example\, if enlistMonsters receives [RadScorpion\, RadScorpion\, somethingElse\, SuperMutant]\, both RadScorpions should be enlisted\, but the SuperMutant should not be.)

- **fight**.
It takes no parameters.
It makes the enlisted teams of monsters and spaceMarines fight amongst themselves.
If no monsters are registered, it outuputs:
```
	No monsters available to fight.
```
If no spaceMarines are registered, it outuputs:
```
Those cowards ran away.
```
If at least one of the two teams is missing, the function stops there by returning false.


#newpage
Here is how a fight should proceed:

* When a new round between a monster and spaceMarine begins, the spaceMarine always goes first.

* The one playing will try to attack, if it's successful, its turn is over.
If it failed because he wasn't in range, he will go closer.
If it failed because he didn't have enough AP, he will call his **recoverAP** method once.

* It is then the opponent's turn.
This process repeats until one of the opponents has fallen.

* The winner calls his **recoverAP** function once before starting the next fight.

* If the spaceMarine wins, the second monster comes in the arena and the whole process starts again until one of the two teams has been defeated (if the monster has won, then the second spaceMarine enters the fray).

Every time a monster or a spaceMarine enters the fight, display:

	[name] has entered the arena.

#hint(If ever both fighters enter at the same time\, the SpaceMarine is always introduced first.)

At the end of the fight, display:

	The [team\'s name] are victorious.

where [team's name] is whether "monsters" or "spaceMarines".

#hint(Remmber\, each winner stays in the arena waiting for the next fight.)

Here is a little example...

```java
	public class Example {
		public static void main(String[] args) {
			SpaceArena arena = new SpaceArena();

			arena.enlistMonsters([new RadScorpion(), new SuperMutant(), new RadScorpion()]);
			arena.enlistSpaceMarines([new TacticalMarine("Joe"), 
						new AssaultTerminator("Abaddon"), new TacticalMarine("Rose")]);
			arena.fight();

			arena.enlistMonsters([new SuperMutant(), new SuperMutant()]);
			arena.fight();
		}
	}
```

#terminal(java Example

	RadScorpion \#1: Crrr!
	SuperMutant \#1: Roaarrr!
	RadScorpion \#2: Crrr!
	Joe on duty.
	Joe has been equipped with a Plasma Rifle.
	Abaddon has teleported from space.
	Abaddon has been equipped with a Power Fist.
	Rose on duty.
	Rose has been equipped with a Plasma Rifle.
	Joe has entered the arena.
	RadScorpion \#1 has entered the arena.
	Joe attacks RadScorpion \#1 with a Plasma Rifle.
	PIOU
	RadScorpion \#1: I\'m too far away from Joe.
	RadScorpion \#1 is moving closer to Joe.
	Joe attacks RadScorpion \#1 with a Plasma Rifle.
	PIOU
	RadScorpion \#1 attacks Joe.
	Joe attacks RadScorpion \#1 with a Plasma Rifle.
	PIOU
	RadScorpion \#1 attacks Joe.
	Abaddon has entered the arena.
	Abaddon: I\'m too far away from RadScorpion \#1.
	Abaddon is moving closer to RadScorpion \#1.
	RadScorpion \#1: I\'m too far away from Abaddon.
	RadScorpion \#1 is moving closer to Abaddon.
	Abaddon attacks RadScorpion \#1 with a Power Fist.
	SBAM
	SuperMutant \#1 has entered the arena.
	Abaddon: I\'m too far away from SuperMutant \#1.
	RadScorpion \#1: SPROTCH !
	Abaddon is moving closer to SuperMutant \#1.
	SuperMutant \#1: I\'m too far away from Abaddon.
	SuperMutant \#1 is moving closer to Abaddon.
	Abaddon attacks SuperMutant \#1 with a Power Fist.
	SBAM
	SuperMutant \#1 attacks Abaddon.
	Abaddon attacks SuperMutant \#1 with a Power Fist.
	SBAM
	Abaddon attacks SuperMutant \#1 with a Power Fist.
	SBAM
	Abaddon attacks SuperMutant \#1 with a Power Fist.
	SBAM
	RadScorpion \#2 has entered the arena.
	Abaddon: I\'m too far away from RadScorpion \#2.
	SuperMutant \#1: Urgh !
)

#terminalNoPrompt(Abaddon is moving closer to RadScorpion \#2.
	RadScorpion \#2: I\'m too far away from Abaddon.
	RadScorpion \#2 is moving closer to Abaddon.
	Abaddon attacks RadScorpion \#2 with a Power Fist.
	SBAM
	RadScorpion \#2 attacks Abaddon.
	Abaddon attacks RadScorpion \#2 with a Power Fist.
	SBAM
	The spaceMarines are victorious.
	SuperMutant \#2: Roaarrr!
	SuperMutant \#3: Roaarrr!
	Abaddon has entered the arena.
	SuperMutant \#2 has entered the arena.
	Abaddon: I\'m too far away from SuperMutant \#2.


	RadScorpion \#2: SPROTCH!
	Abaddon is moving closer to SuperMutant \#2.
	SuperMutant \#2: I\'m too far away from Abaddon.
	SuperMutant \#2 is moving closer to Abaddon.
	Abaddon attacks SuperMutant \#2 with a Power Fist.
	SBAM
	SuperMutant \#2 attacks Abaddon.
	Abaddon attacks SuperMutant \#2 with a Power Fist.
	SBAM
	Abaddon attacks SuperMutant \#2 with a Power Fist.
	SBAM
	SuperMutant \#2 attacks Abaddon.
	Rose has entered the arena.
	Rose attacks SuperMutant \#2 with a Plasma Rifle.
	PIOU
	SuperMutant \#2: I\'m too far away from Rose.
	Rose attacks SuperMutant \#2 with a Plasma Rifle.
	PIOU
	SuperMutant \#2: I\'m too far away from Rose.
	Rose attacks SuperMutant \#2 with a Plasma Rifle.
	PIOU
	SuperMutant \#2: I\'m too far away from Rose.
	SuperMutant \#2 is moving closer to Rose.
	Rose attacks SuperMutant \#2 with a Plasma Rifle.
	PIOU
	SuperMutant \#3 has entered the arena.
	Rose attacks SuperMutant \#3 with a Plasma Rifle.
	PIOU
	SuperMutant \#3: I\'m too far away from Rose.
	SuperMutant \#3 is moving closer to Rose.
	Rose attacks SuperMutant \#3 with a Plasma Rifle.
	PIOU	
)

#terminalNoPrompt(SuperMutant \#3 attacks Rose.

	Rose attacks SuperMutant \#3 with a Plasma Rifle.
	PIOU
	Rose attacks SuperMutant \#3 with a Plasma Rifle.
	PIOU
	SuperMutant \#3 attacks Rose.
	The monsters are victorious.
	SuperMutant \#3: Urgh!
	SuperMutant \#2: Urgh!
	Joe the Tactical Marine has fallen!
	BOUUUMMMM ! Abaddon has exploded.
	Rose the Tactical Marine has fallen !
)