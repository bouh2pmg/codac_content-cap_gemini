---
module:			Cap Gemini
title:			day 06
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d06
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ & Pierre ROBERT
version:		1.4
---


#warn(Unless specified otherwise\, all messages must be followed by a newline.)

#warn(Unless specified otherwise\, the getter and setter name will always add "get" or "set" in front of the name of the attribute\, in CamalCase.)




#newpage

# Exercise 01 #hfill 1pt

**Files to hand in**: pool_java_d07/ex_01/Character.java#br

Create a "Character" class that is composed of the following protected attributes: "name", "life", "agility", "strength", "wit", and a constant "Class" string attribute, with the corresponding getters.

These attributes must have the following values:

- name: argument passed to constructor
- life: 50
- agility: 2
- strength: 2
- wit: 2

Add an **attack** method that takes a string as argument, and prints the following (whatever the argument):

    [name]: Rrrrrrrrr....

Of course, [name] must be replaced by your character's name.#br

Here is an example:
```java
	public class Example {
		public static void main(String[] args) {
			Character perso = new Character("Jean-Luc");

		    System.out.println(perso.getName());
		    System.out.println(perso.getLife());
		    System.out.println(perso.getAgility());
		    System.out.println(perso.getStrength());
		    System.out.println(perso.getwit());
		    System.out.println(perso.getClass());
		    perso.attack("who cares");
		}
	}
```
#terminal(java Example
Jean-Luc
50
2
2
2
Character
Jean-Luc: Rrrrrrrrr....)




#newpage

# Exercise 02 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_02/Character.java
#space(2.9) pool_java_d06/ex_02/Warrior.java
#space(2.9) pool_java_d06/ex_02/Mage.java#br

Copy the previous exercise's code and create the "Warrior" class as well as a "Mage" class, which inherits from "Character".
Modify each class's attributes as follows:#br

#twoColumns(.45, **Warrior**
- CLASS: "Warrior"
- life: 100
- strength: 10
- agility: 8
- wit: 3, **Mage**
- CLASS: "Mage"
- life: 70
- strength: 3
- agility: 10
- wit: 10
)#br

These two classes must each implement the **attack** method.
Its parameter defines the weapon used to attack.#br

The Warrior can attack with a "hammer" or a "sword".
If anything else is passed as parameter, he doesn't attack.
The "Warrior" class's **attack** method must display:

    [name]: Rrrrrrrrr....
    [name]: I'll crush you with my [weapon]!

The Mage can attack with "magic" or with a "wand".
If anything else is passed as parameter, he doesn't attack.
The "Mage" class's **attack** method must display:

    [name]: Rrrrrrrrr....
    [name]: Feel the power of my [weapon]!

Of course, [name] must be replaced by your character's name, and [weapon] by your character's weapon.#br

Our characters are proud and they like to announce themselves on the battlefield.
You will make sure that when creating a "Warrior" or "Mage" object, a message is written in the following format:

    [name]: My name will go down in history!

for a Warrior, and

    [name]: May the gods be with me.

for a Mage.

#hint(This hint is super...)

Here is an example:

```java
	public class Example {
		public static void main(String[] args) {
			Character warrior = new Warrior("Jean-Luc");
			Character mage    = new Mage("Robert");

		    warrior.attack("hammer");
		    mage.attack("magic");
		}
	}
```

#terminal(java Example
Jean-Luc: My name will go down in history!
Robert: May the gods be with me.
Jean-Luc: Rrrrrrrrr....
Jean-Luc: I'll crush you with my hammer!
Robert: Rrrrrrrrr....
Robert: Feel the power of my magic!)






#newpage

# Exercise 03 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_03/Character.java
#space(2.9) pool_java_d06/ex_03/Warrior.java
#space(2.9) pool_java_d06/ex_03/Mage.java
#space(2.9) pool_java_d06/ex_03/IMovable.java#br

As usual, you will reuse here the classes created in the previous exercises.
Please copy them in your folder.#br

We now have characters who can be Mages or Warriors.
They can attack, fair enough, but they still cannot move! This is bothersome...
In order to add this behavior to our classes, we are going to create an "IMovable" interface that contains the following methods: **moveRight**, **moveLeft**, **moveForward**, and **moveBack**.

#hint(This interface will obviously be implemented in the "Character" classes.)

These methods must display the following messages, respectively:

	 [name]: moves right
	 [name]: moves left
	 [name]: moves back
	 [name]: moves forward






# Exercise 04 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_04/Character.java
#space(2.9) pool_java_d06/ex_04/Warrior.java
#space(2.9) pool_java_d06/ex_04/Mage.java
#space(2.9) pool_java_d06/ex_04/IMovable.java#br

Paralysis is over!
Our characters can now move, but, being so proud, they want more!
Our friend Warrior refuses to be compared to a small and skinny Mage.
While the Warrior moves in a bold and virile manner, the Mage moves delicately!#br

To satisfy our boorish Warrior, implement overloads for the **IMovable** methods inherited by "Character".
Your methods must display the following messages that correspond to the class that overloads them:

for a warrior:

	 [name]: moves right like a bad boy.
	 [name]: moves left like a bad boy.
	 [name]: moves back like a bad boy.
	 [name]: moves forward like a bad boy.

for a mage:

	 [name]: moves right furtively.
	 [name]: moves left furtively.
	 [name]: moves back furtively.
	 [name]: moves forward furtively.

Here is an example:
```java
	public class Example {
		public static void main(String[] args) {
			Warrior warrior = new Warrior("Jean-Luc");
			Mage mage       = new Mage("Robert");

		    warrior.moveRight();
		    warrior.moveLeft();
		    warrior.moveBack();
		    warrior.moveForward();
		    mage.moveRight();
		    mage.moveLeft();
		    mage.moveBack();
		    mage.moveForward();
		}
	}
```

#terminal(java Example
Jean-Luc: My name will go down in history!
Jean-Luc: moves right like a bad boy.
Jean-Luc: moves left like a bad boy.
Jean-Luc: moves back like a bad boy.
Jean-Luc: moves forward like a bad boy.
Robert: May the gods be with me.
Robert: moves right furtively.
Robert: moves left furtively.
Robert: moves back furtively.
Robert: moves forward furtively.)





# Exercise 05 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_05/Character.java
#space(2.9) pool_java_d06/ex_05/Warrior.java
#space(2.9) pool_java_d06/ex_05/Mage.java
#space(2.9) pool_java_d06/ex_05/IMovable.java#br

Our characters are now customized to talk, walk and attack.
Yet, they still can't unsheathe their weapons! 
Being able to attack is nice, but attacking while the weapon is still in its sheath is going to be difficult...#br

You will agree that, whether Warrior or Mage, the character will draw his weapon the same way.
This is why, you will make sure that the "Character" class implements the **unsheathe** method so that both "Warrior" and "Mage" inherit from it.
However, you will also make sure that the **unsheathe** method cannot be overloaded by "Warrior"and "Mage".#br

This method must display the following text when called:

     [name]: unsheathes his weapon.





#newpage

# Exercise 06 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_06/Character.java
#space(2.9) pool_java_d06/ex_06/Warrior.java
#space(2.9) pool_java_d06/ex_06/Mage.java
#space(2.9) pool_java_d06/ex_06/IMovable.java#br

In the following exercises, we will focus on exceptions.#br

Go back to your "attack" method. 
If the Mage or Warrior doesn't know the weapon passed as parameter, you have to print a message.

#hint(Use the **try**\, **catch**\, **finally** keywords for this.)

If a Warrior is requested to use a weapon he doesn't know, he must ask:

     [name]: A [weapon]?? What should I do with this?!

If a Mage is requested to use a weapon he doesn't know, he must say:

     [name]: I don't need this stupid [weapon]! Don't misjudge my powers!

[name] is obviously the name of the Warrior or the Mage and [weapon] is the weapon he has to use.

Here is an example:

```java
	public class Example {
		public static void main(String[] args) {
			Character warrior = new Warrior("Jean-Luc");
			Character mage    = new Mage("Robert");

		    warrior.attack("screwdriver");
		    mage.attack("hammer");
		    warrior.attack("hammer");
		}
	}
```

#terminal(java Example
Jean-Luc: My name will go down in history!
Robert: May the gods be with me.
Jean-Luc: A screwdriver?? What should I do with this?!
Robert: I don't need this stupid hammer! Don't misjudge my powers!
Jean-Luc: Rrrrrrrrr....
Jean-Luc: I'll crush you with my hammer!)








# Exercise 07 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_07/Character.java
#space(2.9) pool_java_d06/ex_07/Warrior.java
#space(2.9) pool_java_d06/ex_07/Mage.java
#space(2.9) pool_java_d06/ex_07/IMovable.java
#space(2.9) pool_java_d06/ex_07/WeaponException.java#br

Let's create a **WeaponException** class dedicated to weapons error management.#br

This class must inherit from the *Exception* class, in which at least two different exceptions must be declared: when the weapon is not defined, and when it does not fit the character.#br

Use this new class, to print the following message:

	[name]: I refuse to fight with my bare hands.

when the *attack* method is called with an empty string, and

	[name]: A [weapon]?? What should I do with this?!
or

     [name]: I don't need this stupid [weapon]! Don't misjudge my powers!

if the weapon does not fit the character.

Here is an example:

```java
	public class Example {
		public static void main(String[] args) {
			Character warrior = new Warrior("Jean-Luc");
			Character mage    = new Mage("Robert");

		    warrior.attack("screwdriver");
		    mage.attack("hammer");
		    warrior.attack("hammer");
		    mage.attack("");
		}
	}
```

#terminal(java Example
Jean-Luc: My name will go down in history!
Robert: May the gods be with me.
Jean-Luc: A screwdriver?? What should I do with this?!
Robert: I don't need this stupid hammer! Don't misjudge my powers!
Jean-Luc: Rrrrrrrrr....
Jean-Luc: I'll crush you with my hammer!
Robert: I refuse to fight with my bare hands.)





# Exercise 08 #hfill 1pt

**Files to hand in**: pool_java_d06/ex_07/Character.java
#space(2.9) pool_java_d06/ex_07/Warrior.java
#space(2.9) pool_java_d06/ex_07/Mage.java
#space(2.9) pool_java_d06/ex_07/IMovable.java
#space(2.9) pool_java_d06/ex_07/WeaponException.java#br

Using the most adapted mechanism, handle exceptions concerning the way characters move, knowing that neither can moveDown, but Mages can moveUp (levitating, obviously).

Here is an example:
```java
	public class Example {
		public static void main(String[] args) {
			Warrior warrior = new Warrior("Jean-Luc");
			Mage mage       = new Mage("Robert");

		    warrior.moveRight();
		    warrior.moveDown();
		    warrior.moveUp();
		    mage.moveRight();
		    mage.moveDown();
		    mage.moveUp();
		}
	}
```

#terminal(java Example
Jean-Luc: My name will go down in history!
Jean-Luc: moves right like a bad boy.
Jean-Luc: can not move down the ground....
Jean-Luc: can not fly.
Robert: May the gods be with me.
Robert: moves right furtively.
Robert: can not move down the ground....
Robert: is floating in the air.)