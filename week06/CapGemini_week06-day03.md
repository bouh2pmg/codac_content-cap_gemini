---
module:			Cap Gemini
title:			day 01
subtitle:		SQL
background:		'../background/EpitechCapBackground.png'

repository: 	SQL_Day_01
repoRights:		gecko
language:		SQL
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.3
---

Thanks to the coming SQL days, you must become familiar with databases and the universal language to communicate with them: SQL.#br

There are many DataBase Management Systems (DBMS): 

* Microsoft SQL Server and Oracle for commercial DBMS,
* SQLite, PostgreSQL, MariaDB or even MySQL for free and open DBMS.

For practical reasons, we must use __MySQL__ which is free and one the most widely used DBMS.

#warn(You should take note that we must not be testing with the same DB than you.
That means that you shall only use general request (and no request like "id = 42" for example).)




#newpage

#Exercise 0 - set up

To install MariaDB, open a terminal and execute the following command:
#terminal(docker pull mariadb)

Create a "datadir" directory containing the "coding.sql" file available in the intranet.#br

Then execute
#terminal(docker run --name some-mariadb -v /path/to/datadir/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=gecko -d mariadb:latest
docker exec -it some-mariadb bash)

You can now execute MariaDB:
#terminal(docker start some-mariadb)

You are now in the MySQL console.
Create a database with the following command:	

	CREATE DATABASE coding;

To "use" this database, type: 
	
	USE coding;

Finally, to populate the database, enter:

	SOURCE /var/lib/mysql/coding.sql;

You now have everything you need to write your SQL requests in the console in front of you. 





#Exercise 1 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_01/ex_01.sql#br
Write a query that displays the __list of all the tables__ in the database.





#Exercise 2 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_02/ex_02.sql#br
Write a query that displays the __description__ of the movies table.





#Exercise 3 #hfill 1pt	 

**Turn in**:  SQL_Day_01/ex_03/ex_03.sql#br
Write a query that displays the __current date__ in a column "Date" at the "YYYY-MM-DD" format.





#Exercise 4 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_04/ex_04.sql#br
Write a query that displays the __title and summary__ of all the movies sorted in alphabetical order.





#Exercise 5 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_05/ex_05.sql#br
Write a query that displays the __name of all the genres__ in the table genres in uppercase. 
The column must be named "NAME OF ALL THE GENRES".





#Exercise 6 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_06/ex_06.sql#br
Write a query that displays the __title of the last 42 movies__ in the table movies. 
The column must be named "Title of the last 42 movies".
The results must be ordered by id descendant.





#Exercise 7 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_07/ex_07.sql#br
Write a query that displays the __name of the most expensive subscription__ in the subscriptions table, as well as its price.
The columns must be respectively named: "Name of the most expensive subscription" and "Price".





#Exercise 8 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_08/ex_08.sql#br
Write a query that displays the movies' __title__ whose genre is "action" or "romance".





#Exercise 9 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_09/ex_09.sql#br
Write a query that displays __how long the shortest movie__ is in minutes.
The movies with NULL or 0 duration should not be taken into account.
The column must be named "Duration of the shortest movie".





#Exercise 10 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_10/ex_10.sql#br
Write a query that displays the __id__ of the movies whose title contains the chain of characters "tard" (case-insensitive).
The column must be named "Identifier".





#Exercise 11 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_11/ex_11.sql#br
Write a query that displays the __number__ of movies whose title ends up with the string "tion" (case-insensitive).
The column must be named "Number of movies ending with 'tion'".






#Exercise 12 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_12/ex_12.sql#br
Write a query that displays the __total number__ of movies whose genre is "western" and whose producers is "tartan movies" or "lionsgate uk".
The column must be named "Number of 'western' movies".





#Exercise 13 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_13/ex_13.sql#br
Write a query that displays the __number and name__ of the rooms that have more than 0 seats and that are not on the first floor (first floor, not ground floor).
The columns must be named "Room numbers" and "Room names".





#Exercise 14 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_14/ex_14.sql#br
Write a query that displays the __number__ of movies whose title starts with "eX" (case-sensitive).
The column must be named "Number of movies that starts with 'eX'".





#Exercise 15 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_15/ex_15.sql#br
Write a query that displays the __average duration__ of the movies rounded to 2 decimals.
The column must be named "Average duration".





#Exercise 16 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_16/ex_16.sql#br
Write a query that displays the __month of birth__ in English from the 42#high(nd) to the 84#high(th) member (the 42#high(nd) and the 84#high(th) must be included).
The column must be named "month of birth".






#Exercise 17 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_17/ex_17.sql#br
Write a query that displays the __title of the longest movie__.
The column must be named "Title of the longest movie".





#Exercise 18 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_18/ex_18.sql#br
Write a query that displays the __last  name followed by a dash, followed by the firstname__ of each member from the table profiles.
The first letter of the last name and the first name’s first letter must be in upper case.
The members should be displayed from the youngest to the oldest.
The column must be named "Full name".





#Exercise 19 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_19/ex_19.sql#br
Write a query that displays the __title__ of the movies whose id is 21, 87, 263, 413 or 633.
The column must be named "Movie title".





#Exercise 20 #hfill 1pt

**Turn in**:  SQL_Day_01/ex_20/ex_20.sql#br
Write a query that displays the __number__ of produced movies per year.
The year must not be 0
The result has to be ordered by descending year of production.
The columns must be named "Number of movies" and "Year of production".