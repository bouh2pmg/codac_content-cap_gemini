---
module:			Cap Gemini
title:			day 13
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d13
repoRights:		gecko
language:		C

noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

#warn(Sources must be turned in with BLIH and GIT.)

#warn(You are only allowed to use the __write__\, __sleep__ and __usleep__ functions.)






#Exercise 1 #hfill 2pts

**Turn in**: pool_c_d13/ex_01#br

Write a program that takes a string as parameter and displays each character one by one, as though it were slowly written by a human.

#hint(man usleep; man ascii)





#Exercise 2 #hfill 2pts

**Turn-in**: pool_c_d13/ex_02#br

Write a program that takes a string as parameter and displays it slowly, in reverse order.




#Exercise 3 #hfill 2pts

**Turn-in**: pool_c_d13/ex_03#br

Write a program that takes a string as parameter, displays it, and then erases each character one by one from the end.




#Exercise 4 #hfill 3pts

**Turn-in**: pool_c_d13/ex_04#br

Write a program that takes a string as parameter, displays it, and then erases each character one by one from the beginning, replacing the deleted characters with spaces.




#Exercise 5 #hfill 3pts

**Turn-in**: pool_c_d13/ex_05#br 

Write a program that takes a string as parameter and displays it, then erases each character, two by two, simultaneously from the end and the beginning.
You must still replace the erased characters with spaces.





#Exercise 6 #hfill 4pts

**Turn-in**: pool_c_d13/ex_06#br

Write a program that takes a string as parameter and displays it slowly from the string's middle character, two by two.
In the event of an odd string, start by first displaying the middle one, then two characters by two characters.




#Exercise 7 #hfill 4pts

**Turn-in**: pool_c_d13/ex_07#br 

Write a program that takes a string as parameter and displays the characters from the middle, like in exercise 6, but showing each character in the ASCII order until it reaches the correct one.
We only want to see alphabetical characters, so in the event of a lowercase character, display each lowercase character in alphabetical order starting from "a", until it reaches the correct one. The same goes for an uppercase character (starting from "A").