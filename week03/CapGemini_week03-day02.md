---
module:			Cap Gemini
title:			day 12
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d12
repoRights:		gecko
language:		C

noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#warn(Do not turn in your main function unless it has been explicitly asked for.)

As of today, you do not need to comply with the norm anymore. 
This doesn't mean that you are free to make the ugliest code ever. We reserve the right to refuse to answer a question if your code is too ugly. 
 You could also lose points if we feel that the code is too "dirty".#br

However, from now on, this means that you do not have any forbidden keywords or functions.

#hint(You should take a look at the following keywords: __for__\, __break__\, __switch__ and __continue__.)





#newpage 

#Exercise 1 - print unique #hfill 2pts

**Turn in**: pool_c_d12/ex_01/print_unique.c#br

Write a __print_unique__ function that takes an int array as its first parameter, the array's size as a second parameter, and prints every unique number in the array (followed by a comma, except for the last one, which must be followed by a new line).

#hint(The use of __break__ or __continue__ may be useful here, but it's not required.)

It must be prototyped as follows:

```c
	void 	print_unique(int *array, int size);
```

Here is an example:
#terminal(cat main.c

	void 	print_unique(int *, int);

	int 	main()
	{
	   int 	tab[] = {0\, 0\, 2\, 3\, 4\, 3\, 6\, 1};

	   print_unique(tab, 8);
	   return (0);
	}

#prompt cc *.c -o ex_01
#prompt ./ex_01
2\,4\,6\,1)






#newpage

#Exercise 2 - merge array #hfill 3pts

**Turn in**: pool_c_d12/ex_02/merge_array.c#br

Write a __merge_array__ function that takes 4 parameters: two int arrays and their respective sizes.
The function returns a newly-allocated array that contains the combination of both arrays' passed as parameter content.#br

It must be prototyped as follows:

```c
	int 	*merge_array(int *arr1, int *arr2, int size1, int size2);
```




#Exercise 3 -  magic square #hfill 3pts

**Turn in**: pool_c_d12/ex_03/magic_square.c#br

Write a __magic_square__ function that takes an 9-int array as parameter and returns 0 if this array is a valid magic square, and 1 otherwise.
A magic square is a square in which the sum of the numbers on each line is equal to the sum of numbers in each column and each diagonal.
For instance,

	8   1   6
	3   5   7
	4   9   2

is a magic square, whose sums are always equal to 15.#br

Your function will receive these numbers that will already be in an array like this:

	n0 n1 n2 n3 n4 n5 n6 n7 n8 n9

The corresponding square is then:

	n0   n1   n2
	n3   n4   n5
	n6   n7   n8

Your function must be prototyped as follows 
```c
	int 	magic_square(int *sqr);
```






#Exercise 4 -  split array #hfill 3pts

**Turn in**: pool_c_d12/ex_04/split_array.c#br

Write a __split_array__ function that takes an int array, its size, and two int pointers as parameters.
This function must return a pointer on two int arrays. 
The first of these must contain all of the odd numbers contained in the array passed as parameter, and the second one all of the even numbers.#br

Your function must store the size of the new array in the int pointers given as parameters. 
The size of the odd array must be stored in the first int pointer, and the size of the even ones in the second int pointer.#br

Your function must be prototyped as follows: 
```c
	int		**split_array(int *arr, int size, int *new_size1, int *new_size2);
```







#Exercise 5 -  Pascal triangle #hfill 4pts

**Turn in**: pool_c_d12/ex_05/pascalTr.c#br

Write a __pascalTr__ function that takes a size as parameter and returns a two-dimensional, dynamically-allocated array that represents a Pascal triangle.
In a Pascal triangle, the first two rows and the first column are set to 1; all of the other elements are the sum of the element directly above them, and the one to the left of the element directly above them.#br

Here is the size 6 Pascal triangle:

	1
	1 	1
	1	2	1
	1	3	3	1
	1	4	6	4	1
	1	5	10	10	5	1


Your function must be prototyped tas follows: 
```c
	int 	**pascalTr(int);
```




#Exercise 6 - matrices addition #hfill 5pts

**Turn in**: pool_c_d12/ex_06/matrices.c

Write a function named __matrices_addition__ that computes the addition inside a matrice.
It takes 5 parameters: a double int array, the column offset (the starting column for the addition), the row offest (the starting rows for the addition), the matrices size (supposed to be square) and the "direction".
The direction is represented by an int, and takes values corresponding to a clock. There are 8 possible directions (front, back, up, down and diagonals). For the diagonals, several values are possible :

	10 or 11	0	1 or 2
	9			-	3
	7 or 8		6	4 or 5

The function returns the sum of the elements chosen.

#hint(If any error occurs, the function will simply return 0.)

It must be prototyped as follows: 
```c
	int  matrices_addition(int **, int column_offset, int row_offset, int size, int dir);
```

#newpage
For example:
#terminal(cat main.c

	\#include <stdio.h>

	int  	matrices_addition(int **, int column_offset, int row_offset, int size, int dir);

	int		main()
	{
	  int	mat[5][5] =
		{
			{0\, 1\, 2\, 3\, 4}\,
			{5\, 6\, 7\, 8\, 9}\,
			{10\, 11\, 12\, 13\, 14}\,
			{15\, 16\, 17\, 18\, 19}\,
			{20\, 21\, 22\, 23\, 24}
		};
	  int	*tab[5];

	  tab[0] = mat[0];
	  tab[1] = mat[1];
	  tab[2] = mat[2];
	  tab[3] = mat[3];
	  tab[4] = mat[4];
	  printf("%d\n", matrices_addition(tab, 0, 0, 5, 3));
	  printf("%d\n", matrices_addition(tab, 0, 0, 5, 6));
	  printf("%d\n", matrices_addition(tab, 0, 0, 5, 0));
	  printf("%d\n", matrices_addition(tab, 3, 3, 5, 11));
	  printf("%d\n", matrices_addition(tab, 3, 3, 5, -2));
	  printf("%d\n", matrices_addition(tab, 3, 22, 5, 2));
	  return (0);
	}
#prompt cc *.c -o ex05
#prompt ./ex05
10
50
0
36
0
0)






#Bonus -  white rabbit  #hfill 2pts

**Turn in**: pool_c_d12/ex_07/white_rabbit.c#br

#quote(~, Sitting on top of the hill\, Alice was bored to death\, wondering if making a chaplet of flowers was worth getting up and actually gathering said flowers. 
And then\, suddenly\, a White Rabbit with pink eyes passed by\, running like a madman. 
This was actually not really worth mentioning\, and Alice wasn't much more puzzled to hear the Rabbit mumbling: "Oh my god\, Oh my god ! I'm going to be late!". 
However\, from the moment the Rabbit pulled out a watch from his vest pocket\, looked at the time\, and ran even faster\, Alice was on her feet in a heartbeat. 
She suddenly realized that she had never seen either a Rabbit with a vest pocket\, nor a watch to pull out of one. 
Dying to know more\, she ran through the fields in the rabbit's wake\, and was lucky enough to be right on time to see him rushing into a huge burrow\, under the bushes. Not a moment later and was she already inside\, never even stopping to wonder how the hell she would get out of there.#br

After drifting around for a long\, long time\, in the maze of the burrow's walls\, Alice met a Gecko\,  whose words\, more or less\, were these: "Hey there ye ! Wot's you doin' here ? Lookin' fer the pink pony as well ?". 
Not a pink pony\, but a white rabbit\, Alice answered. 
"Aaah\, but I know him well\, th'old rabbit friend"\, the Gecko retorted. "I saw him not even five minutes ago! Looked like he was in a helluva hurry\, th'old rabbit friend !". 
Alice asked the Gecko to show her the direction the Rabbit was heading. Without hestitating\, the Gecko pointed to his left and blurted out : "Thatta way!!"\, before suddenly pausing and pointing to the opposite direction. "Err\, nay... I think it wos rather thatta way...". After having pointed to a dozen of different directions\, the Gecko finally admitted "Hmmm... Actually\, I think I may well be lost."#br

Alice was in despair. 
She was lost in a huge burrow\, and was off the trail of the white rabbit. 
When he saw her in this state\, the Gecko took pity on her\, and told her: "Dun' worry there gal\, we'll find your friend th'White Rabbit. Look what I got there." He immediately took a 37-faced die out of his vest pocket (yes\, he also had a vest pocket) and handed it to Alice. 
He showed each of the faces to Alice. "This is the first face\, yeh can tell cause o' the number 1 written on it. And this is face 2"\, and so on until reaching the 37th one.#br

The Gecko then told Alice: 
"what yeh got in yer hand\, it's a magic die! Yeh must take real good care of it! But it'll help yeh find the White Rabbit! 
Now\, listen well\, open yer ears\, I'll explain to yeh how yeh must use it. Each time yeh don't know where t'go anymore\, roll the die.)




#quote(Alice in Wonderland (kind of) - Lewis Carroll, The result'll tell yeh which direction the White Rabbit took.
Although\, if the die gives yeh a multiple of 11 and the weather is nice\, y'should always take a nap...might as well enjoy the sun. 
'Cause yeh can be sure the White Rabbit'll do the same. But if the weather is crap\, better roll the die again. Same thing when you wake up after the nap\, of course. 
If the weather is still nice it tells ye to nap again\, you woke up waaay too early ! 
If you ever get a 37\, then you've found the White Rabbit. 
Never forget that after a cup of tea\, you should always go straight ahead. 
When you get a face that's higher than 24, and that three times this face gives you seven-times-ten-eight or 146\, it means the dice was wrong\, y'should turn and head back. If the result is four or 5\, go left\, there's a caterpillar here that smokes his pipe all day long and blows smoke rings. 
Bit crazy he is\, but quite funneh! With a 15\, straight ahead with you. When the die says 12\, ye're out of luck\, that was for naught and yeh have to throw again. When th'result is 13\, head to yer right. Also works with 34 or more. Left it is\, if the die says six\, 17 or 28. 
A 23 means it's 10pm! Time for a cup of tea. Find a table\, and order a lemon tea. If you don't like lemon\, green tea will do. 
Oh right\, never forget to count all of your results ! When you add'em and yeh find 421\, yeh found the White Rabbit. 
Say hi for me\, while you're at it. Oh\, I need to tell you: that count the result thing\, that also works with three-times-hundred-and-ninety-seven, at least. 
Whenever you get a result that you can divide by 8 and get a round result with nothing left\, just head back where you came from. That number 8 is crappy\, I don't like it. When the result is twice or three times 5\, keep going ahead\, yer on the right path! The sum though is quite a bugger\, if the die tells you that you found the White Rabbit\, y'still need to do what the die told you to do! 
If it tells yeh t'go left\, well y'go left and th'White Rabbit'll be there. If it tells right\, then you don't go left\, you go right! Well\, you got it anyway. Dun worry\, everything'll be fine. 
Ah\, still\, be careful if the die tells you something between eighteen and 21\, go left right away! Otherwise you'll end up meeting the Queen of Hearts. She's completely nuts\, she'�ll never let you leave. 
Really\, between 18 and 21 included\, left as fast as yeh can! Hey\, know what? If you ever get a 1\, look on top of yer head\, means the Rabbit is here! Got it? Got all that? Y'see\, th'nots so complicated." 
Alice's head was swimming with all of those numbers\, but she rolled the die and went after the White Rabbit. While she was fading away\, the Gecko yelled"Ah\, I forgot! Whenever y'don't know what teh do\, just roll the die again!")

Write a function named __follow_the_white_rabbit__ with the following prototype: 
```c
	int 	follow_the_white_rabbit();
```

This function must follow Alice's journey. 

#hint(Use random(3) to simulate the rolling dice.)

When Alice must go left, print "left" on the standard output, followed by a newline. 
If she must go right, display "right", followed by a newline.
If she must keep going straight ahead, display "ahead", followed by a newline.
And if she must head back, display "back", followed by a newline !#br
Finally, when she finds the White Rabbit, display "RABBIT!!!", followed by a newline.#br

The function must return the sum of all the results the die has given so far.

#warn(You must provide only one function\, and no main function.
Don't call srandom(3) by yourself either\, we will deal with it in our main.)
