---
module:			Cap Gemini
title:			day 11
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d11
repoRights:		gecko
language:		C

noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process, and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn in your main function unless it has been explicitly asked for.)

#warn(You are only allowed to use the __write__ function to do the following exercises.)






#Exercise 1 - typedef #hfill 3pts 

**Turn in**: pool_c_d11/ex_01/pool_c_d11.h
**Allowed function**: -#br

Turn in a __pool_c_d11.h__ file that is properly protected against multiple inclusions. 
It must contain 3 things:

* a typedef of a __t_ptr_func__ function pointer that complies with the prototype shown in the example,
* an __s_struct_operations__ structure that contains two fields: a function pointer that we've just defined as __ptr_func__, and a char called __operator__,
* a typedef called __t_operations__

Here is an example:

#terminal(cat main.c

	\#include <unistd.h>
	\#include "pool_c_d11.h"

	int				addition(int a, int b)
	{
	   return (a + b);
	}

	int				main()
	{
	  t_operations	op;
	  int			res;

	  op.operator = '+';
	  op.ptr_func = &addition;
	  res = op.ptr_func(3 + 5, 1) + '0';
	  write(1, &res, 1);
	  write(1, "\\\n", 1);
	  return (0);
	}
#prompt cc *.c -o ex_01
#prompt ./ex_01
9)





#Exercise 2  pointer functions #hfill 4pts 

**Turn in**: pool_c_d11/ex_02/operations.c, pool_c_d11/ex_02/get_actions.c
**Allowed function**: malloc#br

Copy your .h file from the previous exercise. 

#hint(You do not need to turn it in\, we will add our own .h file. 
However, you need to include it with your .c files to complete this exercise.)

In an __operations.c__ file, write the 5 following functions that compute the corresponding mathematical operations between their parameters and return the result:

```c
	int addition		(int, int);
	int subtraction		(int, int);
	int multiplication	(int, int);
	int division		(int, int);
	int modulo			(int, int);
```

#hint(If an error occurs\, the functions simply return 0.)

In a __get_actions.c__ file, write a function that returns an allocated array of __s_operations__ structs. 
You can sort the operations however you want in this array. However, you must add a NULL pointer at the end of your array in the __ptr_func__ field.#br

You will also need to initialize all of the structures; each one of them will need to contain the information of one operation that we created in __operations.c__. 

#hint(If an error occurs\, your function must simply return NULL.)

Here is its prototype:
```c
	t_operations	*get_actions();
```


#newpage

Here is an example:
#terminal(cat main.c

	\#include <unistd.h>
	\#include <stdlib.h>
	\#include "pool_c_d11.h"

	t_operations	*get_actions();

	int				main()
	{
	  t_operations	*ops;
	  int			i;

	  ops = get_actions();
	  i = 0;
	  while (ops != NULL && ops[i].ptr_func != NULL && ops[i].operator != '+')
	      ++i;
	  if (ops && ops[i].ptr_func)
	   {
	      i = ops[i].ptr_func(2 + 4, 3) + '0';
	      write(1, &i, 1);
	      write(1, "\\\n", 1);
	   }
	  return (0);
	}
#prompt cc *.c -o ex_02
#prompt ./ex_02
9)





#newpage

#Exercise 3 - do_op #hfill 6pts 

**Turn in**: pool_c_d11/ex_03/
**Allowed function**: atoi, printf, malloc, free
**Makefile**: yes#br


Write a program called __do-op__ that computes a mathematical operation.
It must be executed with three arguments: a first number, an operator, and a second number.#br

For example : 
#terminal(make
[ ... ]
#prompt ./do-op 42 '+' 21
63)

The operator parameter will correspond to the appropriate function in an array of function pointers.
This means that you must use function pointers...

#hint(If any errors occur\, your program must display "Error." on the error output, followed by a newline.)





#Exercise 4 - my_sort_wordtab #hfill 3pts 

**Turn in**: pool_c_d11/ex_04/my_sort_wordtab.c
**Allowed function**: -#br

Write a __my_sort_wordtab__ function that sorts the words given as parameter in ascii order.
The prototype is as follows: 

```c
	int 	my_sort_wordtab(char **tab);
```

#warn(The use of __strcmp__ is prohibited, but you can use the one you wrote in Day 05. If you did not write one\, take some time to write it now...)






#Exercise 5 - my_advanced_sort_wordtab #hfill 4pts 

**Turn in**: pool_c_d11/ex_05/my_advanced_sort_wordtab.c
**Allowed function***: -#br

Write a __my_advanced_sort_wordtab__ function that sorts the words given as parameter by using the comparison function that is also given as parameter.
The prototype is as follows: 

```c
	void 	my_advanced_sort_wordtab(char **tab, int(*cmp)(char *, char *));
```

A call to __my_advanced_sort_wordtab()__ with __my_strcmp__ as a second parameter must give the same result as a call to __my_sort_wordtab()__.
