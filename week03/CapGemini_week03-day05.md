---
module:			Cap Gemini
title:			day 03
subtitle:		System Administration
background:		'../background/EpitechCapBackground.png'

repository: 	none
repoRights:		none
language:		none

noFormalities:	true
author:			Pierre ROBERT, Francois ROSAIN
version:		1.0
---

#warn(You must do this project by yourself but of course feel free to discuss between peers from time to time.)

There is nothing to submit in your repository for today.

#hint(Since there is nothing to submit\, you are strongly advised to spend this day with your assistants so that you can ask questions at the right time.)

With these exercises, you will learn how to secure a server and install additional components.
At the end of the day, you will be capable to install a lot of different things like packages, servers, programming languages, databases but you will also be familiar with security protocols, file transfer and more. 

#hint(To start these exercices\, you should first start an Apache web server on a Docker container. All the configuration files that should be updated will be the files inside this container.). #br

#hint(Don't forget to often save your container. We never know what could happen !)

#imageCenter(docker-dead.png, 400px, 224px)



#newpage

#00- Back to the Apache

For this exercice, you should just run the container of yesterday, with Apache web server, and all the configurations of yesterday.#br

#hint(You saved the state of your Apache container yesterday \!)

#imageCenter(docker-crab.jpg, 400px, 400px)




#newpage

#01- Apache security

#imageRight(apache.jpg, 120px, 5)

Let's see how to set minimum security to your previously installed web server.
An Apache server with basic configuration lets some information available for ill-intentioned people.
This is a problem we are now going to deal with.

##Server information 

When your server encounters an error, and cannot process the request, Apache will by default give information about the server’s type and version.
In your browser, try to access a page that does not exist on your server:

	http://localhost/doesnt_exists

See? No? 
Try to locate on this page the line which contains the version of Apache. 
To make this a public information can be a security problem: ill-intentioned people could try to exploit vulnerabilities and exposures linked to it. 
Let’s hide this line by editing __apache2.conf__:

1. Go to folder: etc/apache2
2. Edit the file:  conf-available/security.conf
3. Locate the line: ServerTokens and change its value to: Prod
4. Locate the line: ServerSignature and change its value to: Off
5. Restart Apache
#terminal(service apache2 restart)

Refresh the page that does not exist, the line disappeared ;)

##Disable folder’s content listing 

By default, when you access a folder without any _index.html_ or _index.php_ file, Apache lists the content of the folder and show its file tree.#br

To disable this behavior, open the file __apache2.conf__ in /etc/apache2 and modify the following section:

	<Directory /var/www/>
	        Options FollowSymLinks MultiViews
	        AllowOverride All
	        Require all granted
	</Directory>

Finally, restart Apache to apply changes.

##IP Filtering 

If you want a site that can only be accessed from a given network or IP address, you can filter the authorized IPs by editing the __apache2.conf__.
Here are examples (the three altogether do not really make sense):

	#The client must correspond to the ip
	    Require ip 176.16.1.1
	#Accept all clients
	    Require all granted
	#Refuse all requests
	    Require all denied

Or by using some "Require" scopes:

	<RequireAny>
	    #require one of the next rule to be true
	    <RequireAll>
	        #require all the next rules to be true
	        <RequireNone>
	            #require all the next rules to be false
	            Require host bad.host.com
	        </RequireNone>
	        <RequireAll>
	            Require user www-data
	            Require ip 127.0.0.1
	        </RequireAll>
	    </RequireAll> 
	    
	    <RequireAll>
	        #require all the next rules to be true
	        Require user root
	        Require ip 123.123.123.123
	    </RequireAll> 
	</RequireAny>

Play a little bit with those rules. 

#hint(To understand how to use it\, take a quick (or long\, it depends on how brave you are) look [here](http://httpd.apache.org/docs/2.4/en/howto/access.html) to get detailed information.)






#02- SSH : Secure SHell

#imageRight(ssh.jpg, 115px, 2)

#hint(If you don’t remember what is SSH\, go back to day one of System Administration and read again the recap.)

To grant SSH a minimum security, we can make some basic adjustments.

#warn(Everytime you have to edit a configuration file\, create a backup before.
Copy the «sshd_config» file to «sshd_config_backup» in the same repository. It will be easier to restore to default configuration.)

1. Open the file __/etc/ssh/sshd_config__
2. Change the listening port on port 42.
3. Authorize only the user jerry to connect to SSH.
4. Block a list of users.
5. Refuse the login root in SSH.
6. Add a SSH connection banner.

```
Port 42
AllowUsers jerry
DenyUsers foo
PermitRootLogin no
Banner /etc/issue
```

Finally, restart SSH to apply changes:
#terminal(service ssh restart)

Now, restore the default configuration.




#03 - htaccess

__.htaccess__ is an HTTP Apache server configuration file. 
They are used to configure access rights, url redirections, customized error messages and much more.

#warn(Be careful\, the command 'ls -l' does not print the .htacess file.
Think about the meaning of ‘.’)

##Password protection

To protect a folder with a password, you need to create two files in this very folder: __.htaccess__ and __.htpasswd__.
The directive AllowOverride must be set to All for this directory. 

#hint(You should now know which file you must modify. Don't forget to restart Apache after modification.)

In your folder, create a __.htaccess__ file with the following content:

	AuthName "Secure Zone"
	AuthType Basic
	AuthUserFile "/var/www/html/prod/.htpasswd"
	Require valid-user

Go to [this page](http://www.htaccesstools.com/htpasswd-generator/) for instance and generate the directive for the __.htpasswd__ by entering the user login and password you want.#br

Copy the result in the __.htpasswd__ file.

For instance, the user login "teacher" with the password "teacher" gives:

	teacher:$apr1$uihClQqC$cyZSwYrtWVYAynRzrqvpi/


##Preventing directory content listing

To prevent users from listing all the files contained in a directory when there is no index file (.html, .php,...), create a __.htacess__ file containing the line below:

	Options -Indexes

## Redirecting error messages

If you want to use customized error messages or redirect the errors of a web page, create or edit a __.htacess__ file containing lines of this type:

	ErrorDocument error_number message_or_destination

The 3 most common errors are:

1. 404: not found,
2. 403: forbidden,
3. 500: internal server error.

Replace " error_number"  by the corresponding number and replace "message_or_destination" by the action to perform. 
To display a simple message, type in the corresponding message between quotes. 
To redirect to a page, write the path to the page. #br

Here are 2 examples that can help you:

1. You wish to display "Sorry, you cannot access this file" in case of a 403 error. Add the following line to your .htaccess:
```
ErrorDocument 403 "Sorry, you cannot access this file"
```

2. You wish to send 404 errors on your customized page 404.html:
```
ErrorDocument 404 /404.html
```

## Specify a different index file

By default, the index file of a directory is __index.html__, __index.htm__ or __index.php__. 
If you want another file, you can add a line of this type to your .htaccess:

	DirectoryIndex name_of_file

For example, if you want to use the page home.html as index page, use the following line:

	DirectoryIndex home.html

#newpage

## Permanent redirect

This send a permanent redirect HTTP 301 status code which informs users, and more importantly search engines, that they must update their links with the new address.

#hint(If you don’t know what 301 or HTTP code means\, it's time to Google it..)

To redirect the whole site to a new address:

	Redirect 301 / http://new-site.tld/

To redirect a directory or a file to a new one:

	Redirect 301 /old_directory http://new-site.tld/new_directory
	Redirect 301 /old_file.php http://new-site.tld/new_file.php

##Rewriting url

__.htacess__ and the mod_rewrite module of Apache allow URL rewriting.
These lines redirect each request on the script testing.php :

	RewriteEngine On
	RewriteRule .* testing.php

Try the folowing one. Can you understand what it does?

	RewriteEngine On
	RewriteRule letstest /test_wslash/testing.php

#hint(This chapter is important and deep\, be curious and check the [online documentation of Apache](http://httpd.apache.org/docs/current/mod/mod_rewrite.html#rewriterule) about mod_rewrite and rewrite rules) 







#04- SSL : Secure Sockets Layer

#imageLeft(ssl.png, 180px, 7)

SSL/TLS is the standard security technology for establishing an encrypted link between a web server and a browser.
This link ensures that all data passed between the web server and browsers remain private and integral.#br

Basically, when you see "https" at the beginning of an url, it’s a secure website using SSL.#br

#imageCenter(SSL-clientserver.jpg, 500px)




#05- NGINX

#imageLeft(nginx.png, 180px, 6)

NGINX is a free, open-source, high-performance HTTP server and reverse proxy.
NGINX is known for its high performance, stability, rich feature set, simple configuration, and low resource consumption. 
It can be used as a reverse proxy with Apache to handle cache memory, load balancing, SSL certificates or compression.

To install it, type
#terminal(sudo apt-get install nginx)

If you try launching NGINX, you will realize that it's not possible.
#terminal(sudo service nginx start)

This is normal, because it tries to start on port 80 which is also used by Apache. Let's edit the configuration file of NGINX (__/etc/nginx/sites-enabled/default__) to change the listen default port to 88:

	Listen 88;

Then restart NGINX to apply your changes :
#terminal(sudo service nginx restart)

If everything is okay, you should see the NGINX welcome page on http://localhost:88/.




#newpage

#06- Load Balancing

Load balancing is a set of techniques which can distribute a workload among various computers of a group.
These techniques can both manage service overloads by distributing them on several servers, and reduce eventual unavailability of the service in case of failure of a software or device of a single server 1,2.#br

These techniques are commonly used, for example, in HTTP services where a high-traffic website must manage hundreds of thousands of requests every second.#br

The most common architecture is composed of several load balancers (type of routers dedicated to this task), one being the main, and one or several others as backup which can take over, and a collection of similar computers doing the calculations. We can call this set of servers a server farm or, more generally, a server cluster.
The expression server pool is also used.

#imageCenter(loadBalancing.png, 400px)

#hint(Learn more about load balancing [here](https://www.nginx.com/resources/glossary/load-balancing/).)


#hint(Don't forget to often save your container !)
