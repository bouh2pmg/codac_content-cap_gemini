---
module:			Cap Gemini
title:			day 01
subtitle:		System Administration
background:		'../background/EpitechCapBackground.png'

repository: 	none
repoRights:		none
language:		none

noFormalities:	true
author:			Pauline FREY, François ROSAIN & Teddy FONTAINE
version:		1.1
---

This first day marks your first step as a System Administrator.

#space(1)

You will discover how to manage containers with Docker.

#warn(You must do this project by yourself but of course feel free to discuss between peers from time to time.)

There is nothing to submit in your repository for today.

#hint(Since there is nothing to submit\, you are strongly advised to spend this day with your assistants so that you can ask questions at the right time.)

#warn(For all exercices make a script!)

#space(1)

#imageCenter(docker_doge.jpg, 400px)

#newpage

#01- Can you PULL the force ?

#imageLeft(docker_pull_the_force.png, 200px, 6)

#space(1)
* Get the __hello-world__ container from the Docker Hub, available on the Docker Hub.
* Start the __hello-world__ container\, manage to display the container welcome message\, then quit.
#br #br #br #br #br #br

#hint(For all steps make a script!)

#space(2)

#terminal(docker ............... hello-world

#space(1)
Hello from Docker!
This message shows that your installation appears to be working correctly.

#space(1)
To generate this message\, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client\, which sent it
    to your terminal.

#space(1)
To try something more ambitious\, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

#space(1)
Share images\, automate workflows\, and more with a free Docker ID:
 https://cloud.docker.com/

#space(1)
For more examples and ideas\, visit:
 https://docs.docker.com/engine/userguide/)

#newpage

#02- Sleep in a car

#space(2)

#imageCenter(alpine_linux.png, 300px, 3)

#space(2)

Start a shell from an __alpine__ container, manage to directly interact with the container from the terminal and delete the container at the end of the shell execution.

#space(1)

#hint(docker run --help)

#03- All in one

#imageRight(all_in_one.jpg, 250px, 3)

#space(2)

Create only one script for:

#space(1)
* List all active containers.
* List all active and inactive containers.
* List all images.
#newpage

#04- Buy a container, save a panda

From the shell of a __debian__ container, manage to install with the Package Manager of the container what is necessary to compile C source code, and what is necessary to push it on a git repository (ensure to update the Package Manager and the existing packages of the container before). Only commands used in the container are asked in this exercice.

#space(1)

#warn(Save your container state)

#05- Volumes

* Create a volume named __coding__.
* List all Docker VOLUMES created on the machine.

#space(3)

#imageCenter(mac-volume.jpg, 150px)

#newpage

#06- MySQL

Create a script to start a mysql container as a background process.
It should be able to restart by itself in case of error, and manage to set the database password for root to __gecko__. You should also manage to stock the database into the __coding__ volume, and that the created container directly creates a database named __zerglings__, and the container shoud be named __spawning-pool__.

#space(2)

#imageCenter(mysql_is_a_novel.jpg, 300px, 4)

#space(2)

Create a script to delete the container named __zerglings__.

#newpage

#07- My own renault alpine

From an __alpine__ image, add a text editor to your container (emacs or vim, as you desire), which should start when the container start.

#space(1)

#warn(Save your container state)

#space(2)

#imageCenter(docker_containers_everywhere.jpg, 300px)

#space(2)

#hint(alpine's package manager is __apk__.)

#newpage

#08- May the force be with you (bonus)

Create a Dockerfile with a tomcat server fully functional.

#space(2)

#imageCenter(may_the_force_be_with_you.jpg, 300px, 3)