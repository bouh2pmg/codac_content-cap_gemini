---
module:			Cap Gemini
title:			day 02
subtitle:		System Administration
background:		'../background/EpitechCapBackground.png'

repository: 	none
repoRights:		none
language:		none

noFormalities:	true
author:			Pierre ROBERT, Francois ROSAIN
version:		1.0
---

#00- Preparation

Start a __debian__ or __alpine__ container (use your favorite). Use this container for all the next exercices.#br

#hint(Docker Hub.)

#imageCenter(docker-ascii, 400px, 284px)



#01- apt-get : Advanced Packaging Tool 

__APT__ is a package manager used by Debian GNU/Linux and its derivatives.
It’s a command line tool helpful to easily install a package by name.#br

#imageRight(apt-get.jpg, 240px, 5)

Here are some useful commands:
#terminal(apt-get install <package>
#prompt apt-get search <words>
#prompt apt-get remove <package>
#prompt apt-get update
#prompt apt-get upgrade)



#02- SSH : Secure SHell

#imageLeft(ssh.jpg, 115px, 5)

$~$
__SSH__ is a program and a secured communication protocol. 
You need to use apt-get to install it on your Linux machine, using the command:
#terminal(sudo apt-get install ssh openssh-server)

Here is a connection test:
#terminal(ssh your_login@localhost
The authenticity of host 'localhost (::1)' can't be established.
ECDSA key fingerprint is 11:7f:e6:31:96:46:1f:26:a2:14:bf:00:db:d4:20:71.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'localhost' (ECDSA) to the list of known hosts.
your_login@localhost's password: 

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
You have new mail.
Last login: Thu Oct  8 13:56:54 2015 from localhost)

This is how to generate SSH keys (RSA 4096):
#terminal(ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/home/your_login/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/your_login/.ssh/id_rsa.
Your public key has been saved in /home/your_login/.ssh/id_rsa.pub.
The key fingerprint is:
98:5c:bb:c8:ca:da:9c:c2:ab:20:57:78:6d:ca:2c:65 root@debian8-vm
The key's randomart image is:

	+---[RSA 4096]----+
	|                 |
	|                 |
	|        .        |
	|   . o + .       |
	|  . E * S        |
	|   B + . .       |
	|o.o + o .        |
	|o.o= o           |
	|..o+*            |
	+-----------------+
)

#newpage

As a test, create a user named jerry:
#terminal(adduser jerry
Adding user 'jerry' ...
Adding new group 'jerry' (1001) ...
Adding new user 'jerry' (1001) with group 'jerry' ...
Creating home directory '/home/jerry' ...
Copying files from '/etc/skel' ...
Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
Changing the user information for jerry
Enter the new value\, or press ENTER for the default

		Full Name []: Jerry Doe
		Room Number []: 
		Work Phone []: 
		Home Phone []: 
		Other []: 
Is the information correct? [Y/n] Y)

Then add your neighbor’s public key to the authorized keys of user jerry.
Finally, use SSH to connect with jerry’s account on your neighbor’s the computer by using your private key.






#03- SCP : Secure, Contain, Protect

#imageRight(scp.jpg, 240px, 9)

__SCP__ designates a secured file transfer between two computers using the SSH communication protocol. 
The term SCP designates both the program and the protocol.#br

As a test, create a file named testSCP that contains “Hello World!” and send it in the /tmp folder by using jerry’s account :

#terminal(echo 'Hello World !' > testSCP
#prompt scp testSCP jerry@localhost:/tmp)







#04- CURL : Client URL request library

#imageLeft(curl.jpg, 120px, 5)

CURL is a command line tool for transferring data through different protocol.
To install it, type this command:
#terminal(sudo apt-get install curl)

Then test your installation with google.com:
#terminal(curl -v google.com

	* Rebuilt URL to: google.com/
	* Hostname was NOT found in DNS cache
	*   Trying 173.194.45.41...
	* Connected to google.com (216.58.204.110) port 80 (#0)
	> GET / HTTP/1.1
	> User-Agent: curl/7.38.0
	> Host: google.com
	> Accept: */*
	> 
	< HTTP/1.1 302 Found
	< Cache-Control: private
	< Content-Type: text/html; charset=UTF-8
	< Location: http://www.google.fr/?gfe_rd=cr&ei=22gWVuyzBsHI8gf5x6igCA
	< Content-Length: 258
	< Date: Thu\, 21 Apr 2017 13:00:11 GMT
	* Server GFE/2.0 is not blacklisted
	< Server: GFE/2.0
	< 
	<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
	<TITLE>302 Moved</TITLE></HEAD><BODY>
	<H1>302 Moved</H1>
	The document has moved
	<A HREF="http://www.google.fr/?gfe_rd=cr&amp;ei=22gWVuyzBsHI8gf5x6igCA">here</A>
	.
	</BODY></HTML>
	* Connection #0 to host google.com left intact
)

#hint(Try yourself with a cURL request on epitech.eu !)






#05- Apache

#imageRight(apache.jpg, 120px, 5)

Apache is the most common HTTP server. 
It is designed to accept many modules which give it additional functionalities (Perl, PHP, Python, Ruby, CGI, SSI, URL rewriting, content negotiation, etc.).
To install it, type the following:
#terminal(sudo apt-get install apache2 apache2-doc)

All Apache configuration files are store in Apache /etc/apache2/ :

1. apache2.conf : general configuration (formerly httpd.conf)
2. mods-available/ : available modules 
3. mods-enabled/ : enabled modules
4. sites-available/ : available sites (contains default for the site hosted by default)
5. sites-enabled/ : enabled sites

In order to enable the modules, use __a2enmod__ (Apache 2 Enable Module) followed by the name of the module. For instance, we enable here mod_rewrite for URL rewriting.
#terminal(a2enmod rewrite)

In order to activate a website with its configuration stored in sites-available, use __a2ensite__ (Apache 2 Enable Site):
#terminal(a2ensite <siteName>)

To disable a site, use __a2dissite__.
To disable a module, use __a2dismod__.

#warn(Do not modify the content of mods-enabled and sites-enabled!)

#newpage
Open your browser, and enter localhost as an URL.
You should see the Apache default page:
#imageCenter(apache-default.png, 400px)

Here is how it works:
#imageCenter(apache-http.png, 460px)






#06- Virtual Host (for Apache)

With the common configuration, your website folder is located in the __/var/www/html/__ folder.#br

When you want to access your website on your browser, type __localhost[:port]__.
This is the common local server name. The initial one, pointing the previous directory. 
Unfortunatly, this directory should be own by the super user, so you can't modify the files with your common user.#br

It's not very friendly or secure to invoke the super user to develop your website, so we'll create a virtual host.
By doing this, we are telling Apache there is another directory acting as a web server. 
We can name it and give it some particular configuration.#br

For exemple, we want to use our entire «Rendu» folder working as a web server. 
This would be very handy: each of your projects would be accessed the same way, with no files to copy and no super user to invoke.
After doing that, you'll access your «Rendu» folder by typing __cap-gemini[:port]__ on your browser.#br

Enter your apache configuration folder:
#terminal(cd /etc/apache2)

Copy the default Apache configuration file (usually named _000-default.conf_).
#terminal(sudo cp sites-available/000-default.conf \\textbackslash

sites-available/cap-gemini.conf)

#warn(Do not move it or change it\, this file is very useful to write virtualhost configuration!)

Now, let's modify our new cap-gemini configuration :
#terminal(sudo emacs -nw sites-available/cap-gemini.conf)

Make sure it looks like the following:

	ServerAdmin you@cap-gemini.com
	ServerName cap-gemini.com
	ServerAlias www.cap-gemini.com
	DocumentRoot /path/to/your/folder

	ErrorLog ${APACHE_LOG_DIR}/cap-gemini.error.log
	CustomLog ${APACHE_LOG_DIR}/cap-gemini.access.log combined

Last configuration file: the main apache configuration file __/etc/apache2/apache2.conf__.
In this file, you should find some configuration items like these:

	<Directory /var/www/>
	        Options FollowSymLinks
	        AllowOverride None
	        Require all granted
	</Directory>

You have to copy the file and replace the « Directory » rule (ie : /var/www) with your folder (for example : « /home/johndoe/Rendu »).#br

You can add one keyword on the « Options » line : __Indexes__.
It is very useful: it allows to list the files on the current directory if no index files were found.#br

You have know to warn your OS that « cap-gemini.com » is now a valid url and we own it.
Edit your hosts configuration in /etc/hosts and add the following line :

	127.0.0.2	cap-gemini.com
	127.0.0.2	www.cap-gemini.com

To enable your new configuration, run :
#terminal(sudo service apache2 reload
#prompt sudo service apache2 restart
#prompt  sudo a2ensite cap-gemini)

You can now access your directory by hitting _cap-gemini.com_ in your browser. 

#hint(If you can't access your webpage\, verify that the permissions of the whole path to your file is correct.)






#newpage

#07- PHP

PHP is a server-side programming language. 
To install it, run:
#terminal(sudo apt-get install php5-common libapache2-mod-php5 php5-cli php5-curl)

The PHP configuration used in command line is stored in _/etc/php5/cli/php.ini_.
This is not to be confused with the Apache configuration file, stored in _/etc/php5/apache2/php.ini_.
The following is not mandatory, but here are some suggestions for modifications:

	max_execution_time = 30
	max_input_time = 60
	memory_limit = 64M
	upload_max_filesize = 10M
	register_globals = Off
	expose_php = Off

#hint(Refer to the documentation for a complete description.)

#warn(Don't forget to restart your Apache server after any modification of the PHP configuration:
#terminal(sudo service apache2 restart))

As a matter of fact, this configuration is loaded on startup.#br

#hint(A default site is created when installing Apache, it is located in _/var/www/html_.)

In order to test your installation of PHP, you need to create your first PHP file.
Create a file named __index.php__ with the following content:

	<?php
	     echo phpinfo();
	?>

#newpage
If you now go on the address _localhost/index.php_, you should see the following page, which is the result of the __phpinfo()__ function execution:

#imageCenter(phpinfo.png, 400px)

Here is the way it works:

#imageCenter(php-server.jpg, 350px)





#08- MySQL

MySQL is a database management system.
Use the following command line to install it on your linux:
#terminal(sudo apt-get install mysql-server php5-mysql)

During the installation process, you should see the following screen asking for a new root password:

#imageCenter(mysql-pwd.png, 450px)

Set the password as you want. But remember it.

#hint(Really\, remember it...)

#newpage
Your MySQL installation is now finished.
To test it, run the following command:
#terminal(mysql -u root -p)

Then type the password you entered before.
You should get a new prompt:
#terminal(mysql -u root -p

	Enter password: 
	Welcome to the MySQL monitor.  Commands end with ; or \\g.
	Your MySQL connection id is 44
	Server version: 5.5.44-0+deb8u1 (Debian)

	Copyright (c) 2000\, 2015\, Oracle and/or its affiliates. All rights reserved.

	Oracle is a registered trademark of Oracle Corporation and/or its
	affiliates. Other names may be trademarks of their respective
	owners.

	Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

	mysql> exit
	Bye
)



#newpage

#09- node.js

#imageRight(node.jpg, 100px, 5)

__Node.js__ is an open source, event-driven software platform in JavaScript designed for scalable network applications. #br

A difference with Apache is that Node is asynchronous and is on one thread only. But it does the work very quickly!#br#br

Let's install node.js:
#terminal( curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
#prompt sudo apt-get install nodejs
#prompt node -v
v7.5.0)

To try your new node.js installation out, create a small web server.
Create a folder named _nodejs_ in your home:
#terminal(mkdir /home/your_login/nodejs)

Create a _server.js_ file in this folder, containing:

	var http = require('http');

	var server = http.createServer(function(request, response) {
	    console.log('New request!!');
	    response.writeHead(200, {'Content-Type' : 'text/plain'});
	    response.end('Hello World !');
	});

	server.listen(3000);
	console.log('Server listening on port 3000');

Finally, let’s start the server:
#terminal(node server.js)

Go to _localhost:3000_ and see what happened.

#newpage
Here is how it works:

#imageCenter(node-server.png, 500px)



#newpage

#BONUS - Dockerfile master

Redo all the previous exercices in a dockerfile.#br#br#br

#imageCenter(docker-serveur.png, 400px, 284px)