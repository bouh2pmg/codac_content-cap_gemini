---
module:			Cap Gemini
title:			day 11
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d11
repoRights:		gecko
language:		C

noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process\, and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn in your main function unless it has been explicitly asked for.)


Today, you will have to play with a square on the same principle as the rubik’s cube but on a two dimensional square.
We want you to understand what you are doing, today’s exercises are only about algorithms. It’s not a rude word, don’t worry. #br

The puzzle is split step by step and there are many ways to solve it, this solution is certainly not the best but it might be the best in a pedagogical way. #br

After each exercise is done, turn it in and then copy the whole folder for the next exercise. Make sure you keep your header file (rubiks.h) up to date. #br

This is not an easy exercise, but if you follow the guidelines and ask the geckos for help, you might succeed.

#newpage



#Exercise 01 - print and check #hfill 1pt 

**Turn in**: pool_c_d11/ex_01/ruler.c, pool_c_d11/ex_01/rubiks.h#br

Write a "print_tab" function that takes a pointer on an integer pointer as parameter. This parameter is a table.
This function must display the table, producing this output for a proper configuration:
#terminalNoPrompt(-----------------

	| 0 | 0 | 1 | 1 |
	-----------------
	| 0 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
)
Write a "check_square" function that takes the table as parameter and returns 0 if the table is in a proper configuration, 1 otherwise.

#hint(A table is said in a 'proper' configuration if the four 2x2 corner subsquares it contains are filled with the same number.
The previous output represents a proper table.)

The functions must be prototyped as follow:

	void 	print_tab 	(int **table);
	int 	check_square(int **table);




#newpage

#Exercise 02 - direct algorithms #hfill 3pts 

**Turn in**: pool_c_d11/ex_02/algo.c#br

Write three algorithms to manipulate your square:

* "algo_line" that takes the table and a line number as parameters.
This function rotates this single line to the left.
* "algo_column" that takes the table and a column number as parameters.
This function rotates this single column to the top.
* "algo_square" that takes the table and a subsquare number as parameters.
This function rotates this single subsquare clockwise.

The functions must be prototyped as follow:

	void 	algo_line  (int **table, int line);
	void 	algo_column(int **table, int column);
	void 	algo_square(int **table, int square);

You must also create a __MAGIC\_SQR\_DBG\_\___ macro. If it is set to 0xADe4dB33, display the rotation way and the concerned subsquare. Otherwise, don't display anything.

#warn(Do not let it in your code\, we will add out during correction.
If MAGIC\_SQR\_DBG\_\_ is not defined, your code should compile! Think about preprocessor define.)

Here is an example:
```c
\#define MAGIC_SQR_DBG__ 0xADe4Db33

//...
int 		main()
{
	int 	**table;
	//...
	algo_line(table, 0);
	algo_column(table, 0);
	algo_square(table, 0);
	//...
}
```

#terminal(cc *.c -o rotation
#prompt ./rotation

	Rotate Left line 0
	-----------------
	| 0 | 1 | 1 | 0 |
	-----------------
	| 0 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------

	Rotate Top column 0
	-----------------
	| 0 | 1 | 1 | 0 |
	-----------------
	| 2 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 0 | 2 | 3 | 3 |
	-----------------

	Rotate Clockwise square 0
	-----------------
	| 2 | 0 | 1 | 0 |
	-----------------
	| 0 | 1 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 0 | 2 | 3 | 3 |
	-----------------
)



#newpage

#Exercise 03 - reverse algorithms #hfill 3pts 

**Turn in**: pool_c_d11/ex_03/algo_rev.c#br

Reverse the algorithms from the previous exercise.#br

The functions must be prototyped as follow:

	void 	algo_line_reverse  (int **table, int line);
	void 	algo_column_reverse(int **table, int column);
	void 	algo_square_reverse(int **table, int square);

Adding the following piece of code to the previous one:
```c
	//...
	algo_square_reverse(table, 0);
	algo_column_reverse(table, 0);
	algo_line_reverse(table, 0);
	//...
}
```
would give the following result:

#terminal(cc *.c -o rotation_reverse
#prompt ./rotation_reverse

	Rotate Counter Clockwise square 0
	-----------------
	| 0 | 1 | 1 | 0 |
	-----------------
	| 2 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 0 | 2 | 3 | 3 |
	-----------------

	Rotate Bottom column 0
	-----------------
	| 0 | 1 | 1 | 0 |
	-----------------
	| 0 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------

	Rotate Right line 0
	-----------------
	| 0 | 0 | 1 | 1 |
	-----------------
	| 0 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
	| 2 | 2 | 3 | 3 |
	-----------------
)




#newpage

#Exercise 04 - is in #hfill 1pt

**Turn in**: pool_c_d11/ex_04/is_in.c#br

Write two functions: "is\_in\_col" and "is\_in\_line".
They take the table as first argument, a column/line number as second argument and a value as the last argument.
If the given value is in the column/line, the function returns 0. It returns 1 otherwise.#br

The functions must be prototyped as follow:

	int is_in_line(int** table, int line,   int value);
	int is_in_col (int** table, int column, int value);

From the initial square, the following piece of code:
```c
	printf("%d\\n", is_in_line(table, 0, 1));
	printf("%d\\n", is_in_col (table, 2, 3));
	printf("%d\\n", is_in_line(table, 3, 1));
	printf("%d\\n", is_in_col (table, 2, 0));
```
produces the following output:
#terminalNoPrompt(0
0
1
1)





#newpage

#Exercise 05 - looking for space #hfill 4pts

**Turn in**: pool_c_d11/ex_05/looking_space.c#br

We now want look for an available spot in the square. Actually, we do not want to find an available spot in the whole square, but just in some cells; some lines and some columns will be excluded.
An available spot is a spot with a different value from a given one.#br

1. The first parameter is the table.
2. The second parameter is an 4-cell array.
Each index represents a line. 
If the value of the index is 0, the refered line is included in the search.
If the row is filled with another value, the refered line must be excluded.
3. The third parameter acts the same way than the second one but for the columns.
4. The last parameter is a value. Every cell filled with this value is to be considered not empty. All the otheres are empty spots.

This function returns a 2-cell array containing, respectivly, the line and the column where the space is found. A NULL value means that there is no space in the given range.#br

The function must be prototyped as follow:

	int *look_for_space(int **table, int *lines, int *columns, int value);

#hint(Only return the first available spot\, starting from left to right and from top to bottom.)

Here is an example:
```c

	//...
	\#include <stdlib.h>
	\#define EMPTY 0
	\#define BLOCKED 1
	//...
	void 		check_return(int *ret)
	{
		if (ret != NULL)
			printf("line:\t%d\ncolumn:\t%d\n", ret[0], ret[1]);
		else
			printf("Nothing found in the given range.\n");
	}

	int 		main()
	{
		//...
		int 	lines[4];
		int 	columns[4];
		//...
		lines[0]   = BLOCKED;
		lines[1]   = BLOCKED;
		lines[2]   = EMPTY;
		lines[3]   = BLOCKED;
		columns[0] = EMPTY;
		columns[1] = EMPTY;
		columns[2] = BLOCKED;
		columns[3] = BLOCKED;
		check_return(look_for_space(table, lines, columns , 1));
		check_return(look_for_space(table, lines, columns , 2));		
		//...
	}
```
The following input square:

	-----------------
	| 2 | 1 | 3 | 0 |
	-----------------
	| 0 | 0 | 1 | 1 |
	-----------------
	| 2 | 2 | 3 | 0 |
	-----------------
	| 2 | 3 | 1 | 3 |
	-----------------

produces the following output:
#terminalNoPrompt($~$

	line:   	2
	column:		0

Nothing found in the given range.)








#newpage

#Exercise 06 - looking for value #hfill 2pts

**Turn in**: pool_c_d11/ex_06/looking_value.c#br

You just wrote a function to find empty spots.
In the same manner, write a function that looks for a given value.#br

The function must be prototyped as follow:

	int *look_for_value(int **table, int *lines, int *columns, int value);

The previous piece of code (given in exercise 05) with calls to this function instead of _look_for_space_, applied to the same input square would produce the following output:
#terminalNoPrompt(Nothing found in the given range.

	line:		2
	column:		0
)




#newpage

#Exercise 07 - rotate #hfill 1pt

**Turn in**: pool_c_d11/ex_07/rotate.c#br

These functions must rotate the given column/line (given as second argument) depending of the offset given as third argument.

#hint(Think about the offset and which algorithm (direct or reverse) you should use.)

The functions must be prototyped as follow:

	void rotate_lines  (int **table, int line, 	 int offset);
	void rotate_columns(int **table, int column, int offset);

Given the following input square:

	-----------------
	| 0 | 2 | 0 | 0 |
	-----------------
	| 0 | 3 | 1 | 3 |
	-----------------
	| 2 | 2 | 1 | 3 |
	-----------------
	| 1 | 1 | 3 | 2 |
	-----------------

Using this piece of code:
```c
	//...
	\#include <stdlib.h>
	\#define EMPTY 0
	\#define BLOCKED 1
	//...
	int 		main()
	{
		int 	**table;
		//...
		int 	*ret_value;
		int 	*ret_space;
		int 	lines[4];
		int 	columns[4];
		//...
		lines[0]   = BLOCKED;
		lines[1]   = EMPTY;
		lines[2]   = BLOCKED;
		lines[3]   = BLOCKED;
		columns[0] = EMPTY;
		columns[1] = EMPTY;
		columns[2] = EMPTY;
		columns[3] = EMPTY;

		ret_value = look_for_value(table, lines, columns, 0);

		lines[0] = EMPTY;
		lines[1] = BLOCKED;

		ret_space = look_for_space(table, lines, columns, 0);
		rotate_lines(tables, ret_space[0], ret_space[1]-ret_value[1]);
		print_tab(table);
		printf("\n");
		rotate_lines(tables, ret_value[1], ret_space[0]-ret_value[0]);
		print_tab(table);
		//...
	}
```

one gets as ouptut:
#terminalNoPrompt(-----------------

	| 0 | 2 | 0 | 0 |
	-----------------
	| 3 | 0 | 3 | 1 |
	-----------------
	| 2 | 2 | 1 | 3 |
	-----------------
	| 1 | 1 | 3 | 2 |
	-----------------

	-----------------
	| 0 | 0 | 0 | 0 |
	-----------------
	| 3 | 2 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------
)




#newpage

#Exercise 08 - build your first subsquare #hfill 2pts

**Turn in**: pool_c_d11/ex_08/build_first.c#br

With all the functions you now have, you can now build the top left subsquare (subsquare 0).
First, combine the previous functions into a "build_first_line" function that puts the same value on all cells of the first line.
Then turn this line into a subsquare thanks to the "line_to_square" function.#br

The functions must be prototyped as follow:
 
	void build_first_line(int **table);
	void line_to_square  (int **table, int line);

```c
	\#define MAGIC\_SQR\_DBG\_\_ 0xADe4Db33
	//...
	void build_first_square (int **table)
	{
		print_tab(table);
		build_first_line(table);
		line_to_square(table, 0);
	}
```

#warn(Do not turn in the above function "build_first_square".)

Given the following input square:

	-----------------
	| 0 | 0 | 0 | 0 |
	-----------------
	| 3 | 2 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------

we would get the following output:
#terminalNoPrompt(-----------------

	| 0 | 0 | 0 | 0 |
	-----------------
	| 3 | 2 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------

	Rotate Left square 0
	-----------------
	| 0 | 2 | 0 | 0 |
	-----------------
	| 0 | 3 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------


	Rotate Left square 0
	-----------------
	| 2 | 3 | 0 | 0 |
	-----------------
	| 0 | 0 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------

	Rotate Left line 0
	-----------------
	| 3 | 0 | 0 | 2 |
	-----------------
	| 0 | 0 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------
)

#terminalNoPrompt(Rotate Left line 0

	-----------------
	| 0 | 0 | 2 | 3 |
	-----------------
	| 0 | 0 | 3 | 1 |
	-----------------
	| 2 | 1 | 1 | 3 |
	-----------------
	| 1 | 2 | 3 | 2 |
	-----------------
)



#newpage

#Exercise 09 - build your second square #hfill 1pt

**Turn in**: pool_c_d11/ex_09/build_second.c#br

You should be able to build the last line of the square. And consequently the last subsquare (subsquare 3).

#hint(You should place this subsquare next to the first one. This will help you build the last two subsquares.)

The function must be prototyped as follow:
 
	void build_last_line(int **table);

```c
	\#define MAGIC\_SQR\_DBG\_\_ 0xADe4Db33
	//...
	void build_second_square (int **table)
	{
		build_last_line(table);
		line_to_square(table, 3);
		push_it_up(table);
		print_tab(table);
	}
```

You should get something like:

	-----------------
	| 0 | 0 | 3 | 3 |
	-----------------
	| 0 | 0 | 3 | 3 |
	-----------------
	| 2 | 1 | 1 | 2 |
	-----------------
	| 1 | 2 | 2 | 1 |
	-----------------




#Exercise 10 - finish it #hfill 2pts

**Turn in**: pool_c_d11/ex_10/finish.c#br

Last step: rearrange the last two lines.
Do it as you want, you can build a line and transmute it or directly build the square.#br

The function must be prototyped as follow:
 
	void build_final_line(int **table);
