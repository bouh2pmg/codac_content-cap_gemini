---
module:			Cap Gemini
title:			week17-day02
subtitle:		JEE - JAHIA - MiniIntra
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w17\_MiniIntra
repoRights:		gecko
language:		Java, XML, JAHIA
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

This project will be the guide to learn JAHIA. The process is quite simple: we provide you with a mock-up, and you have to implement it using JAHIA.

#newpage
#Mandatory part

You have to implement a simple pseudo Intranet, quite like the one you are using here. It should allow logged users to:

- See their available projects
- See their grades
- Access an E-Learning platform

We won't go too deep in each functionality, but the basics have to be functional.

##Models

###Project

A Project will hold:

- A name
- A category (C, C++, Functionnal Programming etc...)
- A number of points given if validated
- Start and end dates


###User

A user will hold:

- A name
- A password
- A list of projects to which he is registered
- A grade associated to each project

###Document (E-Learning part)

A document will hold:

- A name
- Some binary content (Images, PDFs, slides, videos ...)
- Some descriptive text

###E-Leaning 'blob'

This is what will hold several documents on the same subject. It will contain:

- A name
- A category (C, C++, Functionnal Programming etc...)
- A list of Documents

##Views

Following the mock-up, you should create a view for each type of content.

###Login

This view MUST be the first one a user sees, whatever page he requested. No content is available to 'visitors', except the login page. Upon loggin in, the user accesses the Projects view.

###Projects

This view will list all the available projects, sorted by category. A logged user MUST be able to register to a project, adding it to its list of registered projects. All the project information MUST be visible for each project.

###Users

On this view, a logged user can see his information, and a list of projects to which he is registered, sorted by start date. If the project is finished, the user MUST see his grade for the project.

###E-Learning

This view will list all available 'help units', sorted by category. Every blob MUST display its name and its category. Upon clicking on one of the blobs, the user is redirected to another view, listing the content of the blob with all available information.

###Navigation menu

On the left of every view, a navigation menu MUST be present. This menu will display a tree containing references to all the aforementioned views:

```
Root
  |
  |--Projects
      |
      |--C
      |--C++
      |--Etc...
  |--E-Learning
      |
      |--C
      |--C++
      |--Etc...
  |--Profile
```

###Conclusion

This subject is not really detailed, but it is close to what you could get as a specification document: just a list of required points, with a mock-up.
The mock-up is here so that you can have a global idea of what your client expects. Stay as close to it as possible!
This is generally the prefered way of working on a web project: prepare a mock-up for your client, validate it with him, implement.
