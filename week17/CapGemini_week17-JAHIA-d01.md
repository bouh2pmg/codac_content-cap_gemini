---
module:			Cap Gemini
title:			week17-day01
subtitle:		JEE - JAHIA
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w17\_d01
repoRights:		gecko
language:		Java, XML, JAHIA
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

Welcome back! The next two weeks are going to be really different. Instead of actually writing code and handling content by hand, we are going to use a CMS (Content Management System): [JAHIA](https://academy.jahia.com/home.html). Long story short, it is a software designed especially to handle JEE websites, from designing the views to handling user rights, going through database and content management. It also provides lots of ways to collaborate with other developpers without the need to synchronize everything.#br
The first two subjects will be quite descriptive of what part of the docs you should read, but after that, you're almost alone. JAHIA is a real world, hugely maintained CMS. Litterally tons of people use it; Thus, the documentation is extremely well made, containing everything you should know about the software. Make use of those resources to complete the weeks.#br

As an example, we are going to implement an E-Learning platform: a place where you regroup documents under specific topics, exercises, correction modules, and grades. As you might have understood, the app will simulate a content-heavy website to showcase JAHIA's features.

#newpage
#Step 01 - Setup

Start by reading [those slides](https://academy.jahia.com/documentation/digital-experience-manager/7.2/functional/digital-experience-manager-in-15-slides), install JAHIA, and follow the steps. You should have a page with some default content by the end.

#newpage
#Step 02 - Edit mode

#hint(Read the [Techwiki](https://academy.jahia.com/documentation/techwiki)!)

#hint(By the way\, look at the different URLs you visit: this is a good REST API)

Continue reading the [docs](https://academy.jahia.com/documentation/digital-experience-manager/7.2/functional/quick-start); they'll help you understand the basics of how to navigate in JAHIA. Read at least every topic starting with "Edit Mode:".

#warn(Be careful\, at the time of writing this subject\, the docs for Edit Mode: Content Editing operations of version 7.2 point to the wrong page (it points to the next one\, which is duplicated). Click on the little dropdown just under the title of the page\, and select version 7.1; this version holds the right page.)

This step is going to be the last for today, as it requires you to read a lot of documentation. Just play around with what is exposed to you, fiddle with the settings, try to implement simple and canonical units. Just keep our final objective in mind: we need a website similar to the Intranet you're using right now:

- Login / Logout / Authentication and Authorization mechanisms
- Document abstractions able to hold different types of data
- Project abstractions
- Grades abstraction
- Correction grades abstractions
- Navigation menus

Tomorrow, you'll have the full subject, with a simple mock-up as example.
