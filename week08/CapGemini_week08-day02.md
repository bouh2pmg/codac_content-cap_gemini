---
module:			Cap Gemini
title:			Currency Converter
subtitle:		your very first Java project! 
background:		'../background/EpitechCapBackground.png'

repository: 	currency-converter
repoRights:		gecko
language:		Java
groupSize:		2
binary:			curconv

noErrorMess:	true
noBonusDir:		true
author:			Pierre ROBERT
version:		1.2
---

In this very first project, you are asked to realize a currency converter.
You are free to manage the currencies you fancy, but 2 points are mandatory:

1. you must have a GUI (Graphical User Interface),
2. the conversion rates must be stocked in a dedicated file (parsed by your program) so that they can be modified.

#imageCenter(convertisseur-devise.png, 280px)

#imageRight(conv.jpg, 120px, 25)

#hint(Review the pool days concerning Swing and file reading!)

You may (should?) add some bonus points if you wish.
It up to you, but here are some interesting features:

* live update of the currency rate,
* management of badly formatted rate files,
* a GUI to modify the rate file,
* sound.

#newpage 
 
You are also urged to use Design Patterns.

#hint(Draw from the images in this subject!)

#warn(You MUST have a nice and clever oriented object conception.)

#imageCenter(currency.png, 150px)