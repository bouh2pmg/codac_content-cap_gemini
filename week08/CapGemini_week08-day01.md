---
module:			Cap Gemini
title:			day 02
subtitle:		SQL
background:		'../background/EpitechCapBackground.png'

repository: 	SQL_Day_02
repoRights:		gecko
language:		SQL
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#warn(You should take note that we will not be testing with the same DB than you.
That means that you shall only use general request (and no request like "id = 42" for example).)







#Exercise 1 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_01/ex_01.sql#br
Write a query that displays the __number__ of members and their __average age__, rounded to the nearest integer.
The columns must be named "Number of members" and "Average age".

#hint(A man of 55 years and 11 months is considered to be only 55 years old.)






#Exercise 2 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_02/ex_02.sql#br
Write a query that displays only the __zip code__ where there is more than one individual; display them in ascending order.
The column must be named "Zip codes".





#Exercise 3 #hfill 1pt	 

**Turn in**:  SQL_Day_02/ex_03/ex_03.sql#br
Write a query that displays for each floor its __number__, the __total number of seats__ and the __total number of rooms__, sorted by ascending number of seats.
The columns must be named "Floor number", "Total number of seats" and "Total number of rooms".





#Exercise 4 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_04/ex_04.sql#br
Write a query that displays the __first 92 characters of the summary__ of movies whose id is odd and between 42 and 84.
The column must be named "Summaries".






#Exercise 5 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_05/ex_05.sql#br
Write a query that displays the __email addresses__ of the members in the table profiles, replacing the string "machin.com" by "cap-gemini.fr".
The whole list must be sorted by reverse alphabetical order.
The column must be named "New email addresses".





#Exercise 6 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_06/ex_06.sql#br
Write a query that displays for each movie their __title__ and the __number of days__ since they were released. The release date must be defined.
The columns must be named: "Movie title" and "Number of days passed".





#Exercise 7 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_07/ex_07.sql#br
Write a query that displays the __title__ of movies whose first letter of their title is between 'O' and 'T' included.
The whole list has to be sorted in alphabetical order.
The column must be named "Movie title".





#Exercise 8 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_08/ex_08.sql#br
Write a query that displays the __name__ of the table genres whose id is not between 6 and 12.

#hint(Those numbers should not be included in the final result.)





#Exercise 9 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_09/ex_09.sql#br
Write a query that displays the __title__ and __min_duration__ of all the movies.
The result has to be sorted by descending length of title and then sorted by ascending movie duration.





#Exercise 10 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_10/ex_10.sql#br
Write a query that must sum the __prod_year__ of all the movies in a column "Sum prod_year".
Each year must be summed only once.





#Exercise 11 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_11/ex_11.sql#br
Write a query that __adds a new entry__ in the subscription table.
Its name must be "Premium", its summary "For the privileged", its price 80 and the duration of subscription 126.






#Exercise 12 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_12/ex_12.sql#br
Go back to the query of the previous exercise and add the __premium subscription__ 5 times.

#warn(You must only use one INSERT INTO statement.)





#Exercise 13 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_13/ex_13.sql#br
Delete the __last 4 subscriptions__ of the subscription table. 





#Exercise 14 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_14/ex_14.sql#br
__Update__ the name of the last subscription to "Premium++".





#Exercise 15 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_15/ex_15.sql#br
Update the __email addresses__ of members by replacing the string "machin.com" by "cap-gemini.fr".





#Exercise 16 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_16/ex_16.sql#br
Delete the movies whose __prod_year__ is equal to 0.






#Exercise 17 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_17/ex_17.sql#br
Update the field __producer_id__ in the movies table.
When the field is not defined, set it to the id of the producer who has the less movies. Moreover the producer name must finish by "film".





#Exercise 18 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_18/ex_18.sql#br
__Delete__ all movies in the movie table that were released in 1990.





#Exercise 19 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_19/ex_19.sql#br
Add in the job table the __list of jobs__ given to you in the .csv (the file must be in the same format and must be in _/tmp/jobs.csv_).





#Exercise 20 #hfill 1pt

**Turn in**:  SQL_Day_02/ex_20/ex_20.sql#br
Export the __content__ of the movie table with format CSV in a file in _/tmp/movies.csv_.