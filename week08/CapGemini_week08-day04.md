---
module:			Cap Gemini
title:			Report Generator
subtitle:		automatic meeting reporting 
background:		'../background/EpitechCapBackground.png'

repository: 	report-generator
repoRights:		gecko
language:		Java
groupSize:		2
binary:			repgen

noErrorMess:	true
noBonusDir:		true
author:			Pierre ROBERT
version:		1.1
---

Design and create a software that generates meeting reports as PDF files.#br

All the features are up to you, as far as there is a GUI.
Here are some possible features:

* attendance facts,
* automatic date management,
* meeting agenda,
* roundtable discussions,
* TODO tasks,...

#warn(You MUST have a clean and clever oriented object conception.)

As far as coding is concerned, nothing is mandatory, but you are urged to use:

* proper libraries,
* inheritance features,
* design patterns,
* clean code.

#hint(Organize cleverly your files and directories!)