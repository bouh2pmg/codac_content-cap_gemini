---
module:		Cap Gemini
title:		day 09
subtitle:	Java Pool

repository: 	pool_java_d09
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ
version:		1.3
---


Today, you will finally be introduced to the notion of graphic interfaces or GUI (Graphic User Interface).
Your programs will react, not to keyboard entries, but to graphic components (such as buttons, lists, menus,...).#br

To do so, you will work with the **Swing** package which is automatically included in java.#br

A package is a set of classes.
To use one of these classes, you need to import it, with a line similar to one of the following:
```java
	import java.[package].[class]
	import javax.[package].[class]
```

It is possible to create packages, but it is not the point here.#br

**Swing** is highly connected to another graphical package: **AWT**.



#newpage

# Exercise 01 #hfill 1pt

**File to hand in**: pool_java_d09/ex_01/Calculator.java#br

First, create a **Calculator** class.
This one must only contain the following functions :

- add
- subtract
- multiply
- divide

Each function must take two doubles as parameters and return the operation executed on these two numbers.




# Exercise 02 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_02/Calculator.java
#space(2.9) pool_java_d09/ex_02/Window.java#br

Copy the previous exercise's code and create a Window class that inherits from the JFrame class (**import javax.swing.JFrame**).#br

#hint([Google JFrame](https://www.google.fr/search?q=jframe&oq=jframe)...)

This class must not contain a main.
Its constructor must take 3 parameters: a string (*title*) and two integers (*height* and *width*).

#hint(Don't forget to use the **setVisible()** function.)






# Exercise 03 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_03/Calculator.java
#space(2.9) pool_java_d09/ex_03/Window.java
#space(2.9) pool_java_d09/ex_03/Button.java#br

#warn(You have to use a GridLayout (**import java.awt.GridLayout**) here.)

Let's add some content to our window.
For the calculator, you need some buttons :

- 10 buttons for the numbers (from 0 to 9),
- 4 buttons for the operations ('+', '-', '*', '/'),
- a button to reset the operation in progress,
- a button to compute the result.

Create a **Button** class inherited from JButton (**import javax.swing.JButton**). Its constructor takes 3 arguments: the text on the button and the x-position and the y-position of the button.#br

Add an array attribute, named *buttons*, to your Window class without setter or getter.
Feed this array with all the needed buttons.

#hint(Your very first GUI should now be almost completed\, nice and clean.)




#newpage

# Exercise 04 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_04/Calculator.java
#space(2.9) pool_java_d09/ex_04/Window.java
#space(2.9) pool_java_d09/ex_04/Button.java#br

The buttons are now placed, but nothing happens when they are clicked.
To handle this, you need listeners.
A listener is a mechanism that links an event (such as a click), and the appropriate function to perform in case of this event.#br

In Java, listeners are classes that implement specific interfaces, and define the **actionPerformed(ActionEvent event)** method.
Here, the *ActionListener* interface.
To do so, our **Button** class must implement this *ActionListener* interface.

```java
	public class Button (...) implements ActionListener {

		(...)

		public void actionPerformed(ActionEvent e) {    
    		// things to do...
    	}
	};
```

Then, to use this listener on a specific button:

```java
	Button b = new Button(...);
	b.addActionListener(this);
```

#hint(*this* refers to the calling class.
It is the argument of *addActionListener* since this is the class that implements *ActionListener*.)

Another way to do so, shorter and more Object Oriented friendly, consists in defining an anonymous class as parameter of *ActionListener*.

```java
	Button b = new Button(...);
    button.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent event){
			// things to do...
		}
    });
```

#hint(An anonymous class is a class without name. Thus\, it can only be called when it is defined and can not be reused elsewhere.)

Using whichever method, create listeners for all buttons.
When a button is clicked, it must print on the standard output:

	[text] clicked

[text] being the text on the button.




# Exercise 05 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_05/Calculator.java
#space(2.9) pool_java_d09/ex_05/Window.java
#space(2.9) pool_java_d09/ex_05/Button.java#br

You now need to add a new component to the GUI: the screen to output the result..#br

Add a *JLabel* attribute to your **Window** class.#br
Modify your GridLayout to include this JLabel in your window.

#hint(By default\, the screen displays "0".)

When a button is clicked, display the operation in progress on the screen.
If this button is the one to reset the operation in progress or the one to compute the result, the screen is cleared, and and shows "0".



#newpage

# Exercise 06 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_06/Calculator.java
#space(2.9) pool_java_d09/ex_06/Window.java
#space(2.9) pool_java_d09/ex_06/Button.java#br

Complete your calculator so that pressing the button to compute the result indeed computes the result and outputs it on the screen.

#hint(Congrats! Your very first Java application is now over!)




# Exercise 07 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_07/Calculator.java
#space(2.9) pool_java_d09/ex_07/Window.java
#space(2.9) pool_java_d09/ex_07/Button.java#br

Add a button to clear the last character.

#hint(If no character is displayed on the screen\, nothing happens...)




#newpage


# Exercise 08 #hfill 1pt

**Files to hand in**:  pool_java_d09/ex_08/Calculator.java
#space(2.9) pool_java_d09/ex_08/Window.java
#space(2.9) pool_java_d09/ex_08/Button.java#br

Customize your buttons, so that you can modifiy their size and make them rounded and opaque.



# Exercise 09 #hfill 2pts

**Files to hand in**:  pool_java_d09/ex_09/Calculator.java
#space(2.9) pool_java_d09/ex_09/Window.java
#space(2.9) pool_java_d09/ex_09/Button.java#br

Add the following features :

- if the user clicks on an operator button while a precedent result is displayed, this result becomes the starting point of the next operation.

- if the user tries to clear the last character while the screen is empty, the previous operation is displayed on the screen.

- if a minus is put in front of a number, it becomes a negative number.

- if a plus is put in front of a number, it becomes a positive number.






#newpage

# Exercise 10 #hfill 2pts

**Files to hand in**:  pool_java_d09/ex_10/Calculator.java
#space(2.9) pool_java_d09/ex_10/Window.java
#space(2.9) pool_java_d09/ex_10/Button.java
#space(2.9) pool_java_d09/ex_10/CalcException.java#br

Make an interface to manage cleanly the operators.
Add new operations, such as modulo and factorial.


Add an **CalcException** class that handles the aberrant operations.

#hint(Did you think of the following ones?
```
	3/5*			0+-				-+-4				4/0
``` 
)

#warn(\
```
	-3*5			0+-4			+4			
```
are allowed!)

#warn(You have to make it nice to output appropriate error messages depending on the case.)




# Exercise 11 #hfill 2pts

**Files to hand in**:  pool_java_d09/ex_11/Calculator.java
#space(2.9) pool_java_d09/ex_11/Window.java
#space(2.9) pool_java_d09/ex_11/Button.java
#space(2.9) pool_java_d09/ex_11/CalcException.java
#space(2.9) pool_java_d09/ex_11/IOperator.java#br

Add an interface to manage cleanly the operators.
Implement new operations, such as modulo and factorial.