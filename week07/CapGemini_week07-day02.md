---
module:			Cap Gemini
title:			day 08
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d08
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ & Pierre ROBERT
version:		1.3
---

In 1995, the *Gang of Four* (Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides) wrote a now very famous book called *’Design Patterns : Elements of Reusable Object-Oriented Software’*. 
It introduced 23 **design patterns**, that are 23 ways to conveive and organize classes. Each design pattern is a reliable solution to a very common problem, a kind of good practise.
They are language-independent.#br

Nowadays, those design patterns are still used, and some more were added to them, and some variants appeared.
You are about to study a few of them in today's exercises in order to extract some common principles, helpful to design applications.

#hint(To represent classes' organization and content\, UML diagrams will be used. You'll get a very brief introduction to these diagrams.)

The use of design patterns shows many advantages:

- quality
they are proven answers validated by experts,

- speed
they are fast to implement and save time on conception brainstorming,

- reusability
they can be reused in different applications without further development,

- ease
they are fully documented and well-known

- readability
they are a common vocabulary for many people who use and master them.


## Subdivision

Design patterns are subdivided into 3 categories:

- **Creational**
To instanciate, initialize and configure classes and objects.
*Factory*, *AbstractFactory*, *Builder* and *Prototype*.

- **Structural**
To organize and connect classes.
*Adapter*, *Bridge*, *Composite*, *Decorator*, *Facade*, *Flyweight* and *Proxy*.

- **Behavioral**
To manage objects so that they can collaborate and interact.
*Interpreter*, *Template Method*, *Chain of Responsibility*, *Command*, *Iterator*, *Mediator*, *Memento*, *Observer*, *State*, *Strategy* and *Visitor*.

#warn(Design patterns are not always relevant. Some are lambasted by developers\, probably rightly.)


## UML

UML means **Unified Modeling Language**.
It is a uniformed and practical way to represent classes and their interactions.#br

A class is represented by a box like the one below:

#imageCenterCorners(ClassLegend.png, 500px, 0)

#newpage
Here are the different relations among them:

#imageCenterCorners(ClassRelationshipLegend.png, 500px, 0)

#imageCenterCorners(ClassRelationshipCardinality.png, 500px, 0)

#hint(Ouch... It looks a bit repellent at first glance\, but it is not as tough as it seems.)







# Exercise 01 #hfill 1pt

**Files to hand in**: pool_java_d11/Factory/Toy.java
#space(2.9) pool_java_d11/Factory/TeddyBear.java
#space(2.9) pool_java_d11/Factory/Gameboy.java
#space(2.9) pool_java_d11/Factory/Factory.java
#space(2.9) pool_java_d11/Factory/GiftPaper.java#br

The factory methods allow to encapsulate objects creations.
This is useful when the creation process is complex, when it depends on configuration files or user entries for example.#br

Today Santa is asking you to manage his toy factory.
Create a **Toy** class that contains a String *title* as attribute, with a getter but no setter.#br

Also create two classes which inherit from the **Toy** class: a **TeddyBear** class and a **Gameboy** class. Their *title* attributes are respectively "teddy" and "gameboy".#br

Create now a **GiftPaper** class.
This class has only a **Toy** attribute and a *wrap* method which is its attribute's setter.#br

Create a **Factory** class which contains a *create* method.
This method takes a String as parameter and returns a **Toy**.
If the parameter is "teddy", it returns a **TeddyBear**; if it is "gameboy", it returns a **GameBoy**.#br

Finally, add a *getPaper* method to your **Factory** class which returns a **GiftPaper**.

#imageCenterCorners(Factory_UML, 380px, 0)




# Exercise 02 #hfill 2pts

**Files to hand in**: pool_java_d11/Factory/Toy.java
#space(2.9) pool_java_d11/Factory/TeddyBear.java
#space(2.9) pool_java_d11/Factory/Gameboy.java
#space(2.9) pool_java_d11/Factory/Factory.java
#space(2.9) pool_java_d11/Factory/GiftPaper.java
#space(2.9) pool_java_d11/Factory/Elf.java#br

Add to your classes an **Elf** class which contains a **Toy**, a list of **GiftPaper** and a **Factory** as attributes and a *listening* method.
This method must wait for user entries to do things.
Here is the commands list:

- **get**
creates a **TeddyBear** or a **Gameboy** depending on the parameter(s) (for example: "get teddy" or "get 10 giftpapers").
The **Elf** cannot get something if he already has something in his hands.
If the **Elf** can get a toy, the method must display:
```
	What a nice one! I would have liked to keep it...
```
If he cannot display it:
```
	Minute please ?! I'm not that fast.
```

- **pack**
makes the **Elf** get a new **GiftPaper**, wrap the **Toy** in it and display:
```
	And an other kid will be happy!
```
If the **Elf** doesn't have **GiftPaper** anymore, it must display:
```
	Wait... I can't pack it with my shirt.
```




# Exercise 03 #hfill 3pts

**Files to hand in**: pool_java_d11/Composite/ISentence.java
#space(2.9) pool_java_d11/Composite/Word.java
#space(2.9) pool_java_d11/Composite/SentenceComposite.java#br

Create a **ISentence** Interface that only contains a *print* method. This method takes no parameter and returns nothing.
Create a **Word** class that inherits from **ISentence** and override the *print* function to display "word".#br

You now have to create a Composite class named **SentenceComposite**.
It must contain an **ArrayList<Sentence>** named *childSentence* as attribute.
Override its *print* function to make it iterate on its children to call their own *print* functions.#br

Finally, add two functions: *add* and *remove* which both take an **ISentence** as parameter to add or remove a child.#br

Here is an example:

```java
	public static void main(String[] args) {
		Word w1 = new Word();
		Word w2 = new Word();
		Word w3 = new Word();
		Word w4 = new Word();

		SentenceComposite sc1 = new SentenceComposite();
		SentenceComposite sc2 = new SentenceComposite();
		SentenceComposite sc3 = new SentenceComposite();

		sc1.add(w1);
		sc1.add(w2);
		sc1.add(w3);

		sc2.add(w4);

		sc3.add(sc1);
		sc3.add(sc2);
		sc3.print();
	}
```

#terminal(java Example
word
word
word
word)






# Exercise 04 #hfill 5pts

**Files to hand in**: pool_java_d11/Observer/IObservable.java
#space(2.9) pool_java_d11/Observer/Order.java
#space(2.9) pool_java_d11/Observer/IObserver.java
#space(2.9) pool_java_d11/Observer/Customer.java#br

Let's start an application for customers to see in real time the state of the orders they placed.
We'll use the Observer design pattern.

#hint(This design pattern can be used when one object has several observers\, when one observer has several objects to observe or when several objects are followed by several observers.)

Create a **IObservable** interface with three methods:

- *setObserver* : takes an IObserver as parameter
- *deleteObserver* : takes no parameter and returns nothing
- *notifyObserver* : returns a boolean, false if the observer is null

Create a **Order** class which inherits from the **IObservable** interface.
This class has four attributes: *position* (string), *destination* (string), *timeBeforeArrival* (int) and *observer* (**IObserver**) you will create later.
Add a getter for the first three ones.#br

It also has a *setData* method which takes two strings and one int as parameters to set respectively the position, the destination and the time.#br

Then, Create an **IObserver** interface which only has one *update* method that takes an *Observable* as parameter.#br

Create now a **Customer** class which inherits from the **IObserver** interface.
Its *update* method must display the delivery information the following way:

	Position ([position]), [time] minutes before arrival at [destination].

#hint(Use the *instanceof* keyword.)

Add a *notify* method to your **IObservable** class which calls the *update* method of its **IObserver**.
This method must be called after updating the delivery's data#br


#imageCenterCorners(Observer_UML, 400px, 0)

Let's see an example of how it should work:

```java
	public static void main(String[] args) {
		order order = new Order();
		Customer customer = new Customer();

		order.setObserver(customer);
		order.setData("123.5326, 237.9277", 10, "6W 40th Street, New York");
	}
```




#terminal(java Example
Position (123.5326, 237.9277)\, 10 minutes before arrival at 6W 40th Street\, New York.)







# Exercise 05 #hfill 5pts

**Files to hand in**: pool_java_d11/Decorator/Warrior.java
#space(2.9) pool_java_d11/Decorator/BasicWarrior.java
#space(2.9) pool_java_d11/Decorator/KingWarrior.java
#space(2.9) pool_java_d11/Decorator/StuffDecorator.java
#space(2.9) pool_java_d11/Decorator/Shield.java
#space(2.9) pool_java_d11/Decorator/FireSword.java#br

The Decorator design pattern is useful to enrich dynamically a basic class.
Let's see how to use it.#br

Create a **Warrior** abstract class with two attributes:

- an int, _hp_, for the health points,
- an int, _dmg_, for the damage points it causes.

Add a getter and a setter for each attribute: *getHp*, *setHp*, *getDmg* and *setDamages*.#br

Now create two classes **BasicWarrior** and **KingWarrior** which inherit from **Warrior**, and set the following attributes in their constructors:

#twoColumns(.45,

	**BasicWarrior**

	hp: 	40
	dmg: 	7
,

	**KingWarrior**

	hp: 	60
	dmg: 	10
)

You now have to implement the decorator class, whose goal is to add skills to our warriors. 
Create the class **StuffDecorator** that inherits from **Warrior** and has a **Warrior** named *holder* as protected attribute.#br

Make sure your **StuffDecorator** class implements the first two attributes' getters in order to force your decorators to use them.#br

Create a **Shield** class and a **FireSword** class which both inherit from the **StuffDecorator** class. Their constructor takes a **Warrior** as parameter to initialize their attribute *holder*.#br

The **Shield** constructor must display:

	May this shield protect me against every enemy.

And the **FireSword** displays:

	I can slice and burn like the wind and the flames.

Now override the getters to make the **Shield** add 10 health points and the **FireSword** 3 damage points.#br


#imageCenterCorners(Decorator_UML, 450px, 0)

Here is an example:

```java
	public static void main(String[] args) {
		Warrior albert = new BasicWarrior();
		System.out.println("Albert has " + albert.getHp() + " health points.");
		albert = new Shield(albert);
		System.out.println("Albert has " + albert.getHp() + " health points.");

		Warrior georges = new KingWarrior();
		System.out.println("Georges has " + georges.getHp() + " health points and can hit " 
										  + georges.getDmg() + " damages.");
		georges = new FireSword(georges);
		georges = new Shield(georges);
		System.out.println("Georges has " + georges.getHp() + " health points.");
		System.out.println("Georges can hit " + georges.getDmg() + " damages.");
	}
```


#terminal(java Example
Albert has 40 health points.
May this shield protect me against every enemy.
Albert has 50 health points.
Georges has 60 health points and can hit 10 damages.
I can slice and burn like the wind and the flames.
May this shield protect me against every enemy.
Georges has 70 health points.
Georges can hit 13 damages.)








# Exercise 06 #hfill 182pts

If design patterns are often handy, they are not always appropriate and are not the Alpha and Omega of OO conception.
Rather than trying to apply (even cleverly) these patterns, understanding the object model in depth is way more relevant.#br

To do so, here is a list of concepts and Wikipedia references you should read:

- [SOLID](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design))
**Single responsibility**
a class should have only a single responsibility (i.e. only one potential change in the software's specification should be able to affect the specification of the class)#br
**Open/closed**
software entities... should be open for extension, but closed for modification#br
**Liskov substitution**
objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program#br
**Interface segregation**
many client-specific interfaces are better than one general-purpose interface#br
**Dependency inversion**
one should depend upon abstractions, not concretions.\

#hint(Read some more about SOLID\, this is the keystone of conception!)

- [Design by contract](https://en.wikipedia.org/wiki/Design_by_contract)

- [Composition over inheritance](https://en.wikipedia.org/wiki/Composition_over_inheritance)

#hint(These are advanced principles\, but at some point in your Java understanding\, you need to be familiar with them...)