---
module:			Cap Gemini
title:			day 10
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d10
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Jonathan NAU
version:		1.2
---

Today, we will focus on two other Java's specificities: **annotations** and **reflection**.#br

Annotations allow the developers to attach information or even action to a number of things (classes, methods, variables, ...) in an elegant way.

#hint(You already used **@Override** annotation in the 6#high(th) exercise of the 4#high(th) day.)

Reflection is a mechanism that allows you to inspect the content of a class or object at runtime.


#newpage

# Exercise 00 

If not done yet, do the 6#high(th) exercise of the 4#high(th) day.




# Exercise 01 #hfill 3pts

**File to hand in**: pool_java_d10/ex_01/Character.java#br

**Javadoc** is a tool used to generate a documentation of a Java code.
It heavily depends on annotations to get information about classes, methods and variables.#br

For this exercise, copy the code of the Character class from day 07 and document it as best you can using at least 4 different annotations from the javadoc.

#hint(To see the result\, you can use the command *javadoc ex_01/Character.java* from the root of your repository)





# Exercise 02 #hfill 4pts

**File to hand in**: pool_java_d10/ex_02/Inspector.java#br

Create a class named **Inspector** with:

  - an attribute **inspectedClass** (Class)
  - a constructor taking the inspected class in parameter
  - a public method **displayInformations** that will display all the information of the class to inspect.

For example:
```java	
	Inspector inspector = new Inspector(Inspector.class);
	inspector.displayInformations();
```

#terminal(java Example
Information of the "com.company.Inspector" class:
Superclass: java.lang.Object
10 methods:
- displayInformations
- wait
- wait
- wait
- equals
- toString
- hashCode
- getClass
- notify
- notifyAll
1 fields:
- inspectedClass)

#hint(Use reflection...)





# Exercise 03 #hfill 1pt

**File to hand in**: pool_java_d10/ex_03/Inspector.java#br

Copy the **Inspector** class from the previous exercise and add a **createInstance** method to this class.
It must invoke a new instance of the **inspectedClass** and return it.

#hint(You may have to combine reflection and generics to do it.)





# Exercise 04 #hfill 2pts

**File to hand in**: pool_java_d10/ex_04/Test.java#br

We will create a *very* small test framework using annotations.
The first step is to create an annotation that will be used to mark the methods to call for the testing.#br

Create an annotation called **Test** with the following properties:

  - it must be available at runtime,
  - we should only be able to use it on methods,
  - it should have two fields:
    * **name**: a string containing the name of the test
    * **enabled**: a boolean indicating if the test is enabled or not (true by default).






# Exercise 05 #hfill 4pts

**Files to hand in**: pool_java_d10/ex_05/Test.java
#space(2.9) pool_java_d10/ex_05/TestRunner.java#br

Create a class named **TestRunner** with a **runTests** method.
This method takes a class in parameter and executes every method of this class that is annotated with **@Test**.

#warn(**runTests** should only execute methods where **@Test** is present and its property **enabled** is true.)

It should also display the name of the executed test.

#hint(Be smart and combine concepts you have seen up to this point.)





# Exercise 06 #hfill 2pts

**Files to hand in**: pool_java_d10/ex_06/Before.java
#space(2.9) pool_java_d10/ex_06/After.java
#space(2.9) pool_java_d10/ex_06/BeforeClass.java
#space(2.9) pool_java_d10/ex_06/AfterClass.java#br

Create four new annotations **before**, **after**, **beforeClass**, **afterClass**,  that must be available at runtime for methods only.

#hint(These annotations don't do anything so far.)






# Exercise 07 #hfill 4pts

**Files to hand in**: pool_java_d10/ex_07/Before.java
#space(2.9) pool_java_d10/ex_07/After.java
#space(2.9) pool_java_d10/ex_07/BeforeClass.java
#space(2.9) pool_java_d10/ex_07/AfterClass.java
#space(2.9) pool_java_d10/ex_07/Test.java
#space(2.9) pool_java_d10/ex_07/TestRunner.java#br

Change your public method **runTests** of **TestRunner** to add these features:

  - before each test, you must execute the method annotated with **@Before** (if such a method exists),
  - after each test, you must execute the method annotated with **@After** (if such a method exists),
  - before any test is executed, you must execute the method annotated with **@BeforeClass** (if such a method exists),
  - after any test is executed, you must execute the method annotated with **@AfterClass** (if such a method exists).

Congratulations, you have a way to launch test sequences.




# Exercise 08 #hfill 2pts

**Files to hand in**: pool_java_d10/ex_08/Before.java
#space(2.9) pool_java_d10/ex_08/After.java
#space(2.9) pool_java_d10/ex_08/BeforeClass.java
#space(2.9) pool_java_d10/ex_08/AfterClass.java
#space(2.9) pool_java_d10/ex_08/Test.java
#space(2.9) pool_java_d10/ex_08/TestRunner.java#br

Add some minor adjustements to your previous code to tell if a test is successful or not.
Pretty-print all the results from the tests execution.