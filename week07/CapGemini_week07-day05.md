---
module:			Cap Gemini
title:			day 11
subtitle:		Java Pool

repository: 	pool_java_d11
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ
version:		1.6
---


You already know a lot about programming.
Let's put it all together today to create a simple program to manage a shop.
A backery.
Everybody likes pastries.


#newpage

# Exercise 01 #hfill 2pts

**Files to hand in**: pool_java_d11/ex_01/IFood.java#br
#space(2.9) pool_java_d11/ex_01/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java#br

Create a **Food** interface.
Add the *getPrice* and *getCalories* public methods to your interface. 
Both return an int and take no parameter.#br

Create a **Bread** abstract class which inherits from **Food**.
This class must have a *price* and a *calories* attributes, set by default to 0.#br

These two attributes must be instanciated at the construction of the class. 
The constructor's first parameter is thus the price and the second one is the calories.#br

Your class must also have an integer *bakingTime* attribute.
By default, it is set to 0.#br

Every attribute has a getter but no setter.#br

```java
	public class Example {
		public static void main(String[] args) {
			Food bread = new Bread(2, 50);

			System.out.println("The bread costs " + bread.getPrice() + 
					" euros and contains " + bread.getCalories() + " calories.");
		}
	}
```

#terminal(java Example
The bread costs 2 euros and contains 50 calories.)

Handle errors using Exceptions.





#newpage

# Exercise 02 #hfill 2pts

**Files to hand in**: pool_java_d11/ex_02/IFood.java
#space(2.9) pool_java_d11/ex_02/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_02/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_02/SoftBread.java#br

Create a **FrenchBaguette** class and a **SoftBread** class which both inherit from **Bread**.
Their constructors take no parameter.

#twoColumns(.45,

	FrenchBaguette

	price: 		0.80
	calories: 	700
	bakingTime: 20
,

	SoftBread

	price: 		1.20
	calories: 	500
	bakingTime: 30
)

```java
	public class Example {
		public static void main(String[] args) {
			Food bread = new SoftBread();

			System.out.println("The softbread costs " + bread.getPrice() + 
					" euros and contains " + bread.getCalories() + " calories.");
		}
	}
```

#terminal(java Example
The softbread costs 1.20 euros and contains 500 calories.)





#newpage

# Exercise 03 #hfill 4pts

**Files to hand in**: pool_java_d11/ex_03/IFood.java
#space(2.9) pool_java_d11/ex_03/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_03/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_03/SoftBread.java
#space(2.9) pool_java_d11/ex_03/Drink.java
#space(2.9) pool_java_d11/ex_03/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_03/Coke.java
#space(2.9) pool_java_d11/ex_03/Sandwich.java
#space(2.9) pool_java_d11/ex_03/HamSandwich.java
#space(2.9) pool_java_d11/ex_03/Panini.java
#space(2.9) pool_java_d11/ex_03/Dessert.java
#space(2.9) pool_java_d11/ex_03/Cookie.java
#space(2.9) pool_java_d11/ex_03/CheeseCake.java#br

Create three abstract classes named **Drink**, **Sandwich** and **Dessert** inherited from **Food**, the same way you did on previous exercises.#br

The **Drink** class must have a boolean *can* attribute which is set by default as false.
The **Sandwich** class has a boolean attribute named *vegetarian* and set by default as false too. It also has a string array which describes the ingredients of the sandwich.#br

Now, create two classes named **AppleSmoothy** and **Coke** inherited from **Drink** with the following characteristics:

#twoColumns(.45,

	AppleSmoothy

	price: 1.50
	can: false
,

	Coke

	price: 1.20
	can: true
)#br

Create two more classes named **HamSandwich** and **Panini** which inherit from the **Sandwich** class.

#twoColumns(.45, 

	HamSandwich

	price: 4.00
	vegetarian: false
	ingredients: tomato\, salad\, cheese\,
	ham\, butter
,

	Panini

	price: 3.50
	vegetarian: true
	ingredients: tomato\, salad\, cucumber\,
	avocado\, cheese
)#br

Finally, create two classes named **Cookie** and **CheeseCake** inherited from **Dessert**.

#twoColumns(.45, 

	Cookie

	price: 0.90
,

	CheeseCake

	price: 2.10
)






# Exercise 04 #hfill 3pts

**Files to hand in**: pool_java_d11/ex_04/IFood.java
#space(2.9) pool_java_d11/ex_04/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_04/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_04/SoftBread.java
#space(2.9) pool_java_d11/ex_04/Drink.java
#space(2.9) pool_java_d11/ex_04/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_04/Coke.java
#space(2.9) pool_java_d11/ex_04/Sandwich.java
#space(2.9) pool_java_d11/ex_04/HamSandwich.java
#space(2.9) pool_java_d11/ex_04/Panini.java
#space(2.9) pool_java_d11/ex_04/Dessert.java
#space(2.9) pool_java_d11/ex_04/Cookie.java
#space(2.9) pool_java_d11/ex_04/CheeseCake.java
#space(2.9) pool_java_d11/ex_04/Menu.java#br

Add a **Menu** abstract class which must have three attributes: *drink* and *meal* of type **Food** and *price* which is a double.
Every attribute has a getter but no setter.#br

This class must be able to be used strictly the following way:

```java
	public class Example {
		public static void main(String[] args) {
			Food cocaCola 	= new Coke();
			Food panini 	= new Panini();

			Menu<Food, Food> menu = new Menu<>(cocaCola, Panini);
		}
	}
```

#warn(We only want to instanciate the class with objects that inherit from Food. Do not allow the following example:

```java
	public class Example {
		public static void main(String[] args) {
			Menu<Integer\, Integer> menu = 
						new Menu<>(new Integer(42)\, new Integer(24));
		}
	}
```
)




# Exercise 05 #hfill 3pts

**File to hand in**: pool_java_d11/ex_05/IFood.java
#space(2.9) pool_java_d11/ex_05/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_05/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_05/SoftBread.java
#space(2.9) pool_java_d11/ex_05/Drink.java
#space(2.9) pool_java_d11/ex_05/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_05/Coke.java
#space(2.9) pool_java_d11/ex_05/Sandwich.java
#space(2.9) pool_java_d11/ex_05/HamSandwich.java
#space(2.9) pool_java_d11/ex_05/Panini.java
#space(2.9) pool_java_d11/ex_05/Dessert.java
#space(2.9) pool_java_d11/ex_05/Cookie.java
#space(2.9) pool_java_d11/ex_05/CheeseCake.java
#space(2.9) pool_java_d11/ex_05/Menu.java
#space(2.9) pool_java_d11/ex_05/Breakfast.java
#space(2.9) pool_java_d11/ex_05/Lunch.java
#space(2.9) pool_java_d11/ex_05/AfternoonTea.java#br

Create three classes inherited from **Menu** and named *Breakfast*, *Lunch* and *AfternoonTea*.#br

The **Breakfast** menu can be instanciated only with a **Bread** and a **Drink** (in this very order). 
The **Lunch** menu with a **Sandwich** and a **Drink**.
The **AfternoonTea** with a **Dessert** and a **Drink**.




# Exercise 06 #hfill 3pts

**Files to hand in**: pool_java_d11/ex_06/IFood.java
#space(2.9) pool_java_d11/ex_06/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_06/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_06/SoftBread.java
#space(2.9) pool_java_d11/ex_06/Drink.java
#space(2.9) pool_java_d11/ex_06/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_06/Coke.java
#space(2.9) pool_java_d11/ex_06/Sandwich.java
#space(2.9) pool_java_d11/ex_06/HamSandwich.java
#space(2.9) pool_java_d11/ex_06/Panini.java
#space(2.9) pool_java_d11/ex_06/Dessert.java
#space(2.9) pool_java_d11/ex_06/Cookie.java
#space(2.9) pool_java_d11/ex_06/CheeseCake.java
#space(2.9) pool_java_d11/ex_06/Menu.java
#space(2.9) pool_java_d11/ex_06/Breakfast.java
#space(2.9) pool_java_d11/ex_06/Lunch.java
#space(2.9) pool_java_d11/ex_06/AfternoonTea.java
#space(2.9) pool_java_d11/ex_07/Window.java
#space(2.9) pool_java_d11/ex_07/CashRegister.java
#space(2.9) pool_java_d11/ex_07/Stock.java#br

Now you have your products to sell, you need a GUI to register the sales. 
In order to do this, you have to create a cash register application (to be installed on a tablet).#br

This application must contain at least:

- one button per product: **FrenchBaguette**, **SoftBread**, **AppleSmoothy**, **Coke**, **HamSandwich**, **Panini**, **Cookie**, **CheeseCake**,

- one button per menu: **Breakfast**, **Lunch**, **AfternoonTea**,

- a ticket button to validate the sale.

You are to choose the design on your own.
It must be user friendly, easy to use and respect the following constraints:

- The price must be output and updated live when the consumer takes his order,
- During this order you must also display which products he takes.
- Finally, if the consumer makes a mistake during the order, you have to be able to remove one thing on it without removing all of it.

Create a **CashRegister** class to handle the events the user is doing and update the stocks.#br

This class must include a **Window** class that contains the elements of your cash register. It's up to you to organize them so you can handle events in the **CashRegister** class.#br

Finally, you need a **Stock** class to register the stocks.
This class has one integer attribute per product and per menu (as well as the buttons of your cash register), and a getter for each one of them.
By default, each product of the stock is set to 100. 

#hint(You have to calculate how many menus you can do with this and update it every time a consumer buys something.)

On every button of your GUI, one must see the number of available items of the concerned product in stock.
If ever a product is not available anymore, the button must be disabled.




# Exercise 07 #hfill 4pts

**Files to hand in**: pool_java_d11/ex_07/IFood.java
#space(2.9) pool_java_d11/ex_07/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_07/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_07/SoftBread.java
#space(2.9) pool_java_d11/ex_07/Drink.java
#space(2.9) pool_java_d11/ex_07/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_07/Coke.java
#space(2.9) pool_java_d11/ex_07/Sandwich.java
#space(2.9) pool_java_d11/ex_07/HamSandwich.java
#space(2.9) pool_java_d11/ex_07/Panini.java
#space(2.9) pool_java_d11/ex_07/Dessert.java
#space(2.9) pool_java_d11/ex_07/Cookie.java
#space(2.9) pool_java_d11/ex_07/CheeseCake.java
#space(2.9) pool_java_d11/ex_07/Menu.java
#space(2.9) pool_java_d11/ex_07/Breakfast.java
#space(2.9) pool_java_d11/ex_07/Lunch.java
#space(2.9) pool_java_d11/ex_07/AfternoonTea.java
#space(2.9) pool_java_d11/ex_07/Window.java
#space(2.9) pool_java_d11/ex_07/CashRegister.java
#space(2.9) pool_java_d11/ex_07/Stock.java
#space(2.9) pool_java_d11/ex_07/XMLparser.java#br

Let's add an essential feature: automatically save the stock state in a file (in XML).
You first need to add some annotations in your classes:

- the **@XmlRootElement** associates a class with the root node of an XML document
- the **@XmlElement** annotation is used to associate an attribute to a node of an XML document. Example : @XmlElement(name="SoftBread")
- the **@XmlAccessorType** indicate how JAXB has to take care the class fields. You have to use **@XmlAccessType.FIELD** for your classes.
- the **@XmlElementWrapper**, if a field is of type list or array, this annotation allows you to specify that each element of the list is associated to a different node.

#hint(In order to do this\, you need to use javax.xml package.
**import javax.xml.bind.annotations.\\*;**
Find by yourself some more information and annotations.)

Create an XMLparser class.
Add a *save* method to save the stock state; it takes 2 parameters: the name of the data XML file (as a String) and a Stock class which contains the data to save.
This method doesn't return anything.

#hint(It is up to you to find the most practical way to format your XML files.)

Here is an example:

```java
	import java.util.List;
	import java.io.File;
	import javax.xml.bind.*;
	import javax.xml.annotations.*;

	@XmlRootElement(name="Information")
	@XmlAcessorType(XmlAccessType.FIEL)
	public class Information {

		@XmlElement(name="Identifier")
		private String id;

		@XmlElement(name="Number")
		private int    nb;

		@XmlElementWrapper(name="NamesList")
		@XmlElement(name="nameID")
		private List<String> namesList;
	}

	public class Example {

		public static void main(String[] args) throws Exception {
			JAXBContext jc = JAXBContext.newInstance(FosterHome.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			Information information = (Information) unmarshaller.unmarshal(new File("/files/example.xml"));

			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(information, System.out);
		}
	}
```

Add the auto-save functionnality: each time an order is validated, the stock state must be updated in a new file.

#hint(Organize your files as you want\, but I would suggest somthing like
*\\$YEAR/\\$MONTH/\\$DAY/stock_\\$NUMBER.xml*
Or similar.)





# Exercise 08 #hfill 1pt

**Files to hand in**: pool_java_d11/ex_08/IFood.java
#space(2.9) pool_java_d11/ex_08/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_08/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_08/SoftBread.java
#space(2.9) pool_java_d11/ex_08/Drink.java
#space(2.9) pool_java_d11/ex_08/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_08/Coke.java
#space(2.9) pool_java_d11/ex_08/Sandwich.java
#space(2.9) pool_java_d11/ex_08/HamSandwich.java
#space(2.9) pool_java_d11/ex_08/Panini.java
#space(2.9) pool_java_d11/ex_08/Dessert.java
#space(2.9) pool_java_d11/ex_08/Cookie.java
#space(2.9) pool_java_d11/ex_08/CheeseCake.java
#space(2.9) pool_java_d11/ex_08/Menu.java
#space(2.9) pool_java_d11/ex_08/Breakfast.java
#space(2.9) pool_java_d11/ex_08/Lunch.java
#space(2.9) pool_java_d11/ex_08/AfternoonTea.java
#space(2.9) pool_java_d11/ex_08/Window.java
#space(2.9) pool_java_d11/ex_08/CashRegister.java
#space(2.9) pool_java_d11/ex_08/Stock.java
#space(2.9) pool_java_d11/ex_08/XMLparser.java#br


Add a "load" button to your GUI.
When this button is clicked, the path to the XML file containing the products to add is asked.
Then, the stock state is updated via a *load* method from your *XMLparser* class.#br

This *load* method only takes a String as parameter; it is the file's path.
This method returns the Stock class which contains the products to add to the stock.

#hint(If this parameter is incorrect\, throw an exception and set the products with their default values.)





# Exercise 09 #hfill 8pts

**Files to hand in**: pool_java_d11/ex_09/IFood.java
#space(2.9) pool_java_d11/ex_09/Bread.java
#space(2.9) pool_java_d11/ex_01/BackeryExceptions.java
#space(2.9) pool_java_d11/ex_09/FrenchBaguette.java
#space(2.9) pool_java_d11/ex_09/SoftBread.java
#space(2.9) pool_java_d11/ex_09/Drink.java
#space(2.9) pool_java_d11/ex_09/AppleSmoothy.java
#space(2.9) pool_java_d11/ex_09/Coke.java
#space(2.9) pool_java_d11/ex_09/Sandwich.java
#space(2.9) pool_java_d11/ex_09/HamSandwich.java
#space(2.9) pool_java_d11/ex_09/Panini.java
#space(2.9) pool_java_d11/ex_09/Dessert.java
#space(2.9) pool_java_d11/ex_09/Cookie.java
#space(2.9) pool_java_d11/ex_09/CheeseCake.java
#space(2.9) pool_java_d11/ex_09/Menu.java
#space(2.9) pool_java_d11/ex_09/Breakfast.java
#space(2.9) pool_java_d11/ex_09/Lunch.java
#space(2.9) pool_java_d11/ex_09/AfternoonTea.java
#space(2.9) pool_java_d11/ex_09/Window.java
#space(2.9) pool_java_d11/ex_09/CashRegister.java
#space(2.9) pool_java_d11/ex_09/Stock.java
#space(2.9) pool_java_d11/ex_09/XMLparser.java
#space(2.9) pool_java_d11/ex_09/IInputStrategy.java
#space(2.9) pool_java_d11/ex_09/InputXML.java
#space(2.9) pool_java_d11/ex_09/InputKeyboard.java#br

Last but not least.
Adding new products via an XML file is quite a bit laborious and time-consuming when you only have a few products to declare.
It would be way easier to have a form in your GUI that allows you to manually enter the quantity of each product to add.
Well, add this feature to your GUI.#br

In the application code, you want to be able to switch cleverly between the 2 possibilities of entering data: whether via an XML file or via the keyboard input.
How lucky of you, there is a Design Pattern to do this!
It is called **Strategy**.
Implement it.