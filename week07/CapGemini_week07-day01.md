---
module:			Cap Gemini
title:			day 07
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d07
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Jonathan NAU & Pierre ROBERT
version:		1.2
---

Let's dwelve deeper into OOP by studying one of Java's specificities: the generics.#br

Generics allow a class or a method to support multiple type while keeping the compile-time type safety.
Without knowing it, you already used generics in day 01 by using the "ArrayList" type.#br

Let's have a look at the following block of Java code:

```java
	List v = new ArrayList();
	v.add("test");
	Integer i = (Integer) v.get(0); // runtime error
```

Although the code compiles without error, it throws a runtime exception when executing the third line (*java.lang.ClassCastException*).

#hint(Take some time to understand this example! Test it on your own machine.)

We would like this error to be detected at compilation time, and not at runtime, to avoid bad surprises...
To cope with this matter, we need to use generics; in fact, this is the primary motivation for generics.




#newpage

# Exercise 01 #hfill 4pts

**File to hand in**:  pool_java_d07/ex_01/Solo.java#br

Create a generic class named **Solo** that holds a value.
The class must have as attribute the value to hold, named *value*.
This attribute must have a getter (*getValue*) and a setter (*setValue*).#br

This class' constructor takes the value as parameter.#br

#hint(You probably need to make some research on Internet about generics' syntax...)

We should be able to use your class strictly like in the following example:

```java
	Solo<String> strSolo = new Solo<>("toto");
	String strValue = strSolo.getValue();
	strSolo.setValue("tata");

	Solo<Integer> intSolo = new Solo<>(new Integer(42));
	Integer intValue = intSolo.getValue();
	intSolo.setValue(new Integer(1337));
```





# Exercise 02 #hfill 2pts

**File to hand in**:  pool_java_d07/ex_02/Pair.java#br

Create a **Pair** generic class, that contains a pair of elements.
The types of both elements are a priori undefined.
The class must have the following attributes and one method:

- **first** is the first element of the pair, its type being mentioned as 'T',
- **second** is the second element of the pair, its type being mentioned as 'V',
- the **display** method that pretty prints the pair.

The attributes need a getter (**getFirst**, **getSecond**) but no setter.
This class' constructor takes respectively the first and second element.#br





# Exercise 03 #hfill 2pts

**Files to hand in**:  pool_java_d08/ex_03/Pair.java
#space(2.9) pool_java_d08/ex_03/Duet.java

Create a **Duet** class that inherits from the *Pair* class and has two public static generic methods (that's long to say!) called respectively **min** and **max**.#br

These methods should take two parameters of T type and compare them using the **compareTo** method.
The **min** method returns the smallest one, whereas the **max** method returns the highest one.

#warn(*smallest* and *highest* can apply to many different types (not only numerical values)\, according to the **compareTo** method\, which defines the ordering relation to be used.)

#hint(To use the **compareTo** method\, ensure that T extends from the Comparable interface.)




# Exercise 04 #hfill 2pts

**Files to hand in**:  pool_java_d08/ex_04/Pair.java
#space(2.9) pool_java_d08/ex_04/Duet.java#br

From the previous exercise, add a mechanism so that if the elements to be compared are numerous, the *compareTo* method is not called.
Instead, the numbers are directly compared using usual comparaison operators.





# Exercise 05 #hfill 3pts

**Files to hand in**:  pool_java_d08/ex_05/Character.java
#space(2.9) pool_java_d08/ex_05/Warrior.java
#space(2.9) pool_java_d08/ex_05/Mage.java
#space(2.9) pool_java_d08/ex_05/Battalion.java#br

Okay, now that you've started to understand how generics work, we're going to create battalions composed of Warrior and Mage (and potentially any other type of Character).#br

First, copy the *Character*, *Warrior* and *Mage* classes you wrote the previous day.
Then, create a **Battalion** class that has one attribute and two public methods:

  - The attribute, of type List, must be called **characters**. It must hold all characters composing the battalion.
  - A public method, **add**, which takes a List of Character objects (or any other object that inherits from Character) and add them to the battalion.
  - A public method **display**, which displays the name of every character in *characters*.

For example:

```java
	List<Mage> mages = new ArrayList<>();
	mages.add(new Mage("Merlin"));
	mages.add(new Mage("Mandrake"));
	mages.add(new Mage("Adele"));
	List<Warrior> warriors = new ArrayList<>();
	warriors.add(new Warrior("Achilles"));
	warriors.add(new Warrior("Spartacus"));
	warriors.add(new Warrior("Clovis"));

	Battalion battalion = new Battalion();
	battalion.add(mages);
	battalion.add(warriors);
	battalion.display();
```

#terminal(java Example
Merlin
Mandrake
Adele
Achilles
Spartacus
Clovis)




# Exercise 06 #hfill 1pt

**Files to hand in**:  pool_java_d08/ex_06/Character.java
#space(2.9) pool_java_d08/ex_06/Warrior.java
#space(2.9) pool_java_d08/ex_06/Mage.java#br

Add two new integer attributes to the Character class: **strength**, **magnetism**.
Add a new parameter to its constructor: an integer.
For Warriors, this parameter represents the strength (the magnetism being null).
For Mages, this parameter represents the magnetism (the strength being null).

For example:
```java
	Character merlin   = new Mage("Merlin", 12);
	Character mandrake = new Mage("Mandrake", 9);
	Character achilles = new Warrior("Achilles", 240);
```



# Exercise 07 #hfill 3pts

**Files to hand in**:  pool_java_d08/ex_07/Character.java
#space(2.9) pool_java_d08/ex_07/Warrior.java
#space(2.9) pool_java_d08/ex_07/Mage.java
#space(2.9) pool_java_d08/ex_07/Pair.java
#space(2.9) pool_java_d08/ex_07/Duet.java#br

Add a **fight** method to the *Duet* class.
If the elements of the duet are:

- both Warriors, the strongest one wins,
- both Mages, the one with the most magnestism wins,
- a Warrior and a Mage, the Mage wins unless the Warrior's strength is a multiple of the Mage's magnetism,
- two numerical values, the one returned by the *max* method wins.
- anything else, the one returned by the *min* method wins.

#hint(You need to be smart to have a Mage and a Warrior fight...)

For example:

- from the previous exercise's example, Merlin wins over Mandrake,
- from the previous exercise's example, Mandrake wins over Achilles,
- from the previous exercise's example, Achilles wins over Merlin,
- 45 wins over 34,
- "antilop" wins over "lion".


