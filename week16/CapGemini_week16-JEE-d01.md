---
module:			Cap Gemini
title:			week16-d01
subtitle:		JEE - MongoDB
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w16\_d01
repoRights:		gecko
language:		Java, XML, Spring, MongoDB
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

This week is a bit peculiar. First, we are going to use MongoDB instead of Sql. Thus, we'll use today and tomorrow to discover MongoDB, and you'll have a 3-day rush this week-end. Be mindful of what we do during those two days, it will help you a lot with the rush.

#newpage
#Introduction - MongoDB

As you might know, Sql is __Relational__. Mongo is not; it is __Document Oriented__. This means you won't be able to describe relations between objects as you are used to and that you will have to design and think your entire database differently.
So what are the main differences ?

- A 'Table' is called a 'Collection'.
- A 'Row' is called a 'Document'.
- A Document isn't required to adhere to a certain schema
    - You can't define a specific data model.
    - You can insert documents that have different forms in the same Collection.
- The Document is stored in a readable state, like JSON, BSON, XML ...
- Heavily relies on the underlying filesystem.

Those differences bring a lot of differences in thinking your database. For instance, __no predefined concept of relations exist!__. This means that you __can't__, just with the database driver, think of your data as several objects linked together by ids. Instead, you have to think in Documents, or __single entries__. Let's have a quick example: an address book.#br

Let's say our address book needs to store contacts. A contact contains a name, an optional image, one or more phone numbers, one or more mailing addresses, and one or more email addresses. With a relational database, we would create a Table for each of these fields: A Contact table, with the basic info, a PhoneNumber Table that contains the country code etc.. and an Address Table. PhoneNumber and Address would have some CONTACT\_ID foreign key to allow retrieval of relational data; hence, relational database.#br

In Mongo, we would treat a Contact as a __single__ document, containing __every piece of info we need__. Such a Document would be retrieved through an __id__ as well, but we would get __everything in a single query__, no other complexity is involved. This also means that two separate documents can have the 'same' field, which would be duplicated in each document, instead of being stored in a separate table as a unique entry with two 'links'. You should be starting to see why it is so dependent on the filesystem: it is prone to a lot of data duplication.

#warn(Although MongoDB\, or NoSql databases in general\, seems easier or cleaner\, it is not particularly performant. My point is __don't systematically go for MongoDB just because it's easier to manipulate__. MongoDB is great\, but you have to know __why__ you would use MongoDB instead of Sql. As mentionned before\, Mongo heavily relies on the filesystem\, which means that the performance cost is very high. To (grossly) simplify\, each access to a document is equivalent to an access to the filesystem itself\, wich is one of the most high-cost operations in computer sciences. Of course\, some systems are __made especially for that__\, and thus don't really care about this. Another point you have to take into account is that your data __isn't assured to always have the same form!__ For instance\, someone using the same database and collection than you could suddenly decide he wants to add a TAG field to his documents: Mongo would let him do it\, which in a way is great\, but also means that you could expect a specific form\, and have only a part of your documents that matches it. This is what I meant by __you have to think the whole database differently__. In short\, always ask yourself "Do I need to ensure data integrity at the database level ? Do I have to care about my hardware scaling ? Is my data mostly relational ?" If one of these answers is yes\, consider sticking with Sql.)

#hint(There are ways to simulate relations in MongoDB\, but they should not be used unless you know exactly why you are using them\, and even then\, try to limit their usage; Mongo is not designed for them.)

#warn(This week is really not about front-end development. You are\, as usual\, free to do whatever you like front-wise\, but very little to no points at all are allocated to it. In fact\, I recommend using Postman to test today and tomorrow's APIs; the rush is a specific exercise and will require you to develop some front-end though.)

#hint(Read [this](https://docs.mongodb.com/manual/core/data-modeling-introduction/) and the following topics in 'Data Models' to have a better understanding of when and where you should use references to other documents or just embed them.)

To illustrate those concepts, we are going to implement a very simple Cookbook website. The API will serve __JSON__ documents, as it is easy to read and manipulate.

#newpage
#Step 01 - First setup

Setup a Spring project (using Roo if you so wish) with persistence, but this time, use a MongoDB driver. As usual, start by implementing a Register / Login / Logout mechanism. Our user just needs a __username__, an __email__ and a __password__.#br
Do __NOT__ use Spring Mongo Repository to craft your requests. You will of course be allowed to use it later, but for now, we want you to understand MongoDB Query Language, which is completely different from Sql.
Here, Authorization will simply consist of differentiating logged users and 'visitors'.

#hint(Or better yet\, re-use one you've developed the past weeks and try to adapt it to Mongo with the least changes possible.)

#warn(Do not forget REST!)


#newpage
#Step 02 - Recipe

We'll start by implementing a Recipe. It should contain at least:

- A name
- A map of ingredients with the ingredient as the key and its amount as the value
- A preparation time
- An amount of persons

Create the associated Java classes and Mongo collections, and basic Create / Delete operations. Don't bother with updating a record for now, we'll implement it later.
A Recipe should be accessible in a REST manner (i.e /recipes/bestCakeEver [GET] or similar).

#hint(Here\, we have an example where the 'child' object (ingredient) is small enough that we can just __embed__ it in the Recipe. We do not need\, nor want\, an Ingredient collection: it would be too costly to query the ingredients every time we get a recipe.)

#newpage
#Step 03 - Manipulations

Let's add a way to pass some options to our Routes:

- Add an endpoint with a path variable that specifies the number of persons you wish to see the recipe for (i.e `/recipes/bestCakeEver/6p` or similar).
- Make sure the ingredient quantities match the new path variable:
    - If a recipe would require 1 unit of an ingredient for 1 person and I query it for 6, I should see '6' as the required amount of that ingredient.
    - Always round-up (ceil) the values if their are not integral.

Let's also add a way to filter out certain recipes based on whether or not they contain some ingredients:

- Add another endpoint that takes query params
  (i.e `/recipes/filtered?cheese=true&nutella=false`)
    - Each param key is an ingredient name, with 'true' or 'false' as a value, indicating whether or not you want that ingredient.
    - The route returns a list of recipes corresponding to the constraints passed as query params.
    - If an ingredient name doesn't exist, the param is ignored.

#hint(MongoDB [indexes](https://docs.mongodb.com/manual/indexes/index.html) __WILL__ make your queries much simpler! And faster too.)

#newpage
#Step 04 - What is food without images ?

Add a way to attach some images to a recipe. Be careful though! We want you to use __GridFS__, Mongo's own way of handling large binary files, and a __Multipart Request__ to handle the upload!

#newpage
#Conclusion

Today we saw another database model, the NoSql paradigm. As you might have understood, it is just another way of thinking your data. The same general rules apply, but you still have to think very differently. Tomorrow, we'll continue with MongoDB, learn a bit more about the query language it uses, and update documents instead of just getting them.
