---
module:			Cap Gemini
title:			week16-day02
subtitle:		JEE - REST API
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_w16\_d02
repoRights:		gecko
language:		Java, XML, Spring, MongoDB
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

You should now have a basic understanding of MongoDB query language and should be able to craft simple queries. Today, we are going to see how to craft more complicated queries, like updating part of a document.

#newpage
#Step 01 - Updates

For now, if we want to update a recipe, we have to delete and re-create it, which works, but involves a lot of operations.
MongoDB has a peculiar way of handling updates: you can chose what part of the document you want to update using a [specific syntax](https://docs.mongodb.com/manual/reference/method/db.collection.update/).#br

When you're done reading the docs, add a route with PUT method (remember REST ?) that updates only the part of the recipe you receive in the request body. The recipe ID is passed through a path variable (i.e `/recipes/15 [PUT]`)
For instance, if you receive just a value for the name in the body, only update the name of the recipe.

#hint(If the whole object should be changed\, but not its id, you can use the `save()` method.)

#newpage
#Step 02 - Projection

MongoDB query language allows you to specify which part of the object you want to retrieve. If you have a very large document containing several lists of data, but rarely use them as the same time (in the asame request), you don't need the whole object each time: the `find()` method in Mongo takes a map of values you want to include in the response. With it, implement a new route with GET method that retrieves only the ingredients of a recipe (i.e `/recipes/[:id|:name]/ingredients/:nbPers`). Also add a path variable for the number of person, just like we did yesterday.

#newpage
#Step 03 - Aggregation

The equivalent of Sql `group by` or `count(*)` is the `aggregate` function in MongoDB. Read the docs about aggregate, then:

- Add a way to 'star' a recipe. A PUT request on `/recipes/[:id|:name]/star` should be enough.
- Add a way to 'unstar' a recipe, either with the same request working as a switch (ON or OFF), or with a specific request like `/recipes/[:id|:name]/unstar`.
- Add a route that aggregates a user with his 5 most starred recipes. The response should contain at least the username and a list of recipes.

#newpage
#Step 04 - Indexing

MongoDB exposes ways to index document on a specific field. Add a Tags field to the Recipe class; this list of strings will just serve as an indexing point:

- Find a way to __index__ a recipe on its tags so that you can craft a query similar to this:
    `db.recipes.find({tags: "sweet"})`

#hint(This method is great when you need to browse a collection on a certain predicate\, but it has its limitations. Don't overuse it!)

#newpage
#Step 05 - Sorting

Last but not least, implement a route that returns all the recipes, ordered by their number of stars.


#newpage
#Conclusion

You now know how to use MongoDB. There is still a lot about Mongo we didn't discuss, but you'll have plenty of time to look by yourself. For instance, remeber when I was saying that Mongo needs another way of thinking ? Well, it even goes to the point that Mongo Database server farms are designed differently, hardware-wise, than Sql farms. Take a look at MongoDB replication and sharding.#br

Tomorrow, you'll start the rush! Brace yourselves, it's not that hard, but it requires you to understand the two past subjects perfectly!
