---
module:			Cap Gemini
title:			week16-RushBloggos
subtitle:		JEE - REST API
background:		'../background/EpitechCapBackground.png'

repository: 	JEE\_RushBloggos
repoRights:		gecko
language:		Java, XML, Spring
groupSize:		1

noErrorMess:	true
author:			Raphael Fourdrilis
version:		1.1
---

#newpage
#Foreword

Welcome! Today's subject is a rush, meaning you have little time to finish a fairly big program. Remember everything we taught you in terms of abstraction, design and code reusability; it might seem like a time-consumming task, but trust me when I say it __will save you much more time than you 'lost'__. Plus, it is always a good exercise.

Steps won't be guided, nor will they contain every piece of information you need to implement them. Be smart, and don't bypass what we taught you. If something seems blurry, re read the step, or the previous ones; we won't ask you anything impossible, and we didn't lay any traps along the way.

By the end of the week end, you should have an application similar to Twitter. You application __MUST__ be able to handle users, and allow them to send message streams, viewable by users that subscribed to their author.

##General rules

- The subject is presented in steps. Each step needs the previous one to be implemented; evaluation will stop at the first failing step.
- As usual, readable, maintainable reusable and extensible code are watchwords, not options.
- There is no 'norm' used here, but it goes without saying that you should still establish one, even just for youself, and __stick to it__.
- This might seem obvious, but (close to) identical code between students will be severely penalized.
- Don't re-invent the wheel! Use your Framework!
- Your app __MUST__ be a __SPA__ (Single Page Application).
- You __MUST__ use __MongoDB__ for persistence.
- You __MUST__ use __JSP__ or __Angular2__ for the front.

#newpage
#Step 01 - Users

Setup a Spring project (using Spring Roo if you so wish) and prepare the following features:

- Login / Register / Logout mechanism.
- User profile page, with editing possibilities.
- A search bar somewhere (maybe in you header ?) that lists users corresponding to the input.

#hint(You are free to implement whatever Authentication mechanism you want\, but only Basic Auth is required here.)

#newpage
#Step 02 - Feed

Once a user is connected (remember __Authorization__ ?), he can:

- Post short messages (140 chars max).
- See his own feed.
- Edit and delete his own messages.

#newpage
#Step 03 - Follow / Subscribe

Now that you have some basic functionalities, add these:

- Follow / Subscribe to another user:
    - This allows a user to see other users' messages, but only those of whom he subscribed to.
    - A message __MUST__ show its author one way or the other.
- Unfollow / Unsubscribe:
    - Roughly the opposite of the previous point.
- Block a user:
    - The blocked user __MUST NOT__ see subsequent messages from the blocking user in his feed.

#newpage
#Step 04 - Tags

Add a __hashtag system__, similar to the one Facebook uses:

- A user can add a tag in his message like this:
    Message message message #superMessage message ...
- When searching for a user, searching for a hashtag should list messages containing that tag.
- When a user clicks on a tag, a list of all the messages containing that tag is shown.

#hint(In this example\, the tag would be __superMessage__)


#newpage
#Boni

You are free to implement any bonus you'd like. Here is a non-exhaustive list:

- Avatar system
- Avatar system using Gravatar
- OAuth2 mechanism to connect through Facebook, Twitter ...
- Content sharing
- A __remember me__ option
- Admin space

If you decide to implement something else, just be sure it is as close as possible to the examples given, difficulty-wise. For instance, a 'chat' system would be too easy since it's already almost done.
