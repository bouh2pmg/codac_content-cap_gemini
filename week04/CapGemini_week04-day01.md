---
module:			Cap Gemini
title:			day 01
subtitle:		HTML/CSS pool
background:		'../background/EpitechCapBackground.png'

repository: 	Pool_HTMLCSS_Day_01
repoRights:		gecko
language:		HTML/CSS

noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

Today's exercises are not independent from each others.
Each exercise reuse the previous one.
They are to be done one after the other.

#hint(We advise you to systematically copy/paste the previous exercise before adding the requested functionalities.)

As a reminder, today exercises will not be corrected automatically.

#warn(All HTML pages must respect the basic HTML structure.)




#newpage

#Exercise 01 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_01/ex_01/index.html #br

Write a HTML file named __index.html__.
Display "Welcome to CodingPedia" on that page.#br







#Exercise 02 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_02/index.html #br

Put the sentence "Welcome to CodingPedia" in a level 1 title tag.
Create a paragraph describing the content of your site:

	CodingPedia is a collective encyclopedia project on the Internet aiming to be
	universal and multi-lingual which functions as a wiki.
	CodingPedia has for goal to offer freely reusable objective and verifiable content
	that anyone can modify and improve.

Add a small paragraph to introduce yourself.





#Exercise 03 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_03/index.html, php.html, sql.html #br

Create the pages __php.html__ and __sql.html__.
Add the two folowing links on your __index.html__ page:

1. PHP -> php.html
2. SQL -> sql.html




 

#Exercise 04 #hfill 3pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_04/index.html, php.html, sql.html

Add the following structure tags on your three pages:

- an header tag
- a menu tag
- a main content tag
- an aside content tag (this is a precise HTML tag that presents a content relative to the main content)
- a footer tag that contains an address tag with your coordinates : 'name' – 'first name' – ' address' – 'tel'

#warn(Each page of your site must have the same structure.)







#Exercise 05 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_05/index.html, php.html, sql.html

Create a list on your __index.html__ page that contains the links to the different pages (including index.html).
Add this list, that will act as a menu, on all your HTML pages.
 




#newpage

#Exercise 06 #hfill 3pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_06/index.html, php.html, sql.html #br

On your __php.html__ and __sql.html__ pages, create 4 articles in your main content tag:

- The first article will contain a description of the page's technology.
Use the HTML abbreviation tag.
Copy / paste the first Wikipedia paragraph about [PHP](http://www.wikiwand.com/en/PHP) and [SQL](http://www.wikiwand.com/en/SQL)

- A second empty article.

- A third article that contains a list of your comments on this technology.

- A fourth one that contain code examples.
Use the appropriate tag.

The aside content tag will contain an HTML citation tag:

#quote(Sith Code, Peace is a lie\, there is only passion.
Through passion\, I gain strength.
Through strength\, I gain power.
Through power\, I gain victory.
Through victory\, my chains are broken.
The Force shall free me.)






#Exercise 07 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_07/index.html, php.html, sql.html #br

Get back to the second article of the previous exercise.
Insert a new array. 
Its title will be "Course planning" and it must contain in its header the columns "Subject", "Description" and "Grade obtained".#br

Then you will fill this array each day of the course.





#Exercise 08 #hfill 3pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_08/index.html, php.html, sql.html  #br

Create a form in a new article of the __index.html__ page.
This form will be used to ask questions on the technologies described in other pages.
#br

It must contain the following fields:

- "Name" (required) 
- "First name" (required)
- "Email" (required)
- "Age" (minimum 3, maximum 99 years old)
- "Phone" (10 characters)
- "Sex" (1 choice possible out of two, male by default)
- "Hair color" (you will choose the color with the help of a color selector)
- "Date" (today's date by default, required)
- "Technology" (drop-down list of the technology in question)
- "Question" (required and no size limit – you must use an appropriate element for this kind of field, one that can contain a lot of text)

There must be a submit form button containing the text "Submit".

#warn(Each field must be have a label and each label must be associated to its field with an HTML tag.)

 





#Exercise 09 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_01/ex_09/index.html, php.html, sql.html #br

On each page (SQL and PHP), create a progress bar for yourself (this is a specific HTML element, and you are free to grade yourself as you wish) in a fifth article.#br

Create a sorted list of subjects to review or that you did not understand.