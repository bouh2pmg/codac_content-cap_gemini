---
module:			Cap Gemini
title:			day 02
subtitle:		HTML/CSS pool
background:		'../background/EpitechCapBackground.png'

repository: 		Pool_HTMLCSS_Day_02
repoRights:		gecko
language:		HTML/CSS

noErrorMess:		true
author:			Pauline FREY & Vincent TRUBESSET
version:		1.0
---

# Introduction

Today, exercises 1 through 4 follow each other and are not independent.
They are to be done in order. #br

We advise you to copy/paste everything between two exercises before adding the
requested functionalities. #br

In addition, exercises 5 and 6 depends on yesterday's exercises 7 and 9. #br

# Exercise 01 #hfill 1pt
**Turn in**: Pool_HTMLCSS_Day_02/ex_01/index.html, Pool_HTMLCSS_Day_02/ex_01/snow-white.css #br

Create the **index.html** and **snow-white.css** files then include **snow-white.css** in **index.html**.

# Exercise 02 #hfill 3pts
**Turn in**: Pool_HTMLCSS_Day_02/ex_02/index.html, Pool_HTMLCSS_Day_02/ex_02/snow-white.css #br

Add a little content to your HTML page so you can start formatting it. #br

Create a paragraph containing the following text:
"__*Once upon a time there lived a lovely princess with fair skin and blue eyes. She was so fair that she was named Snow White.*__" #br

Then create another paragraph containing:
"__*One day, she was knitting and pricked herself. Three drops of blood fell in the snow.*__" #br

Thanks to a single CSS class named **`a-great-style`**, apply to the two paragraphs a red color background and "Impact" as font.

#newpage

# Exercise 03 #hfill 3pts
**Turn in**: Pool_HTMLCSS_Day_02/ex_03/index.html, Pool_HTMLCSS_Day_02/ex_03/snow-white.css #br

Let's go further in the styling of your web page. #br

Make sure that:
- The value of CSS properties can be modified in the CSS file only.
- The text size of the first paragraph is 21 pixels
- The text size of the second paragraph is 27 pixels
- The background color of the HTML page is yellow
- The background color of the first paragraph is black
- The background color of the second paragraph is white
- The text color of the first paragraph is white
- The text color of the second paragraph is black
- The word "blood" in the second paragraph must have color red and be in italics. #br

#hint(Italics is done with a CSS property.)

#warn(Formatting the word "blood" must not trigger a line feed.)

#newpage

# Exercise 04 #hfill 3pts
**Turn in**: Pool_HTMLCSS_Day_02/ex_04/index.html, Pool_HTMLCSS_Day_02/ex_04/snow-white.css #br

Create a new class named **`left-bordered-and-big`** that will have the following consequences when applied to an element:
- The element floats on the left
- The element has a solid black border of 1 pixel
- The element width takes 60% of the available space in its parent element #br

Apply this class to the first paragraph of your page, without disabling the class applied in exercise 3. #br

Create another class name **`right-bordered-and-small`** that will have the following consequences when applied to an element:
- The element floats on the right
- The element has a solid black border of 1 pixel
- The element width takes 20% of the available space in its parent element #br

Apply this class to the second paragraph of your page, without disabling the class applied in exercise 3.

# Exercise 05 #hfill 6pts
**Turn in**: Pool_HTMLCSS_Day_02/ex_05/php.html, sql.html, codingpedia.css #br

In this exercise, you will need the HTML pages made yesterday.
Get the **php.html** and **sql.html** pages that you created up to exercise 7 included. #br

Create a CSS page named **codingpedia.css**.
Include this CSS page in your **php.html** and **sql.html** pages. #br
Reproduce the following element layout without modifying your HTML pages. #br

//todo image???

# Exercise 06 #hfill 4pts
**Turn in**: Pool_HTMLCSS_Day_02/ex_06/index.html, Pool_HTMLCSS_Day_02/ex_06/codingpedia.css #br

In this exercise, you will need the **index.html** page created yesterday.
Get the page that you had after exercise 8. #br
Include the **codingpedia.css** CSS file in your **index.html** page. #br
Reproduce the following element layout without modifying your HTML page. #br

//todo image²???