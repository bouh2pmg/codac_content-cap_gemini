---
module:			Cap Gemini
title:			day 02
subtitle:		Java Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_java_d02
repoRights:		gecko
language:		Java

groupSize:		1
noBonusDir:		true
noErrorMess:	true
author:			Hugo WALBECQ
version:		1.2
---


As of today, you’re going to learn Object Oriented Programming (OOP).
In some previous exercises you may have briefly used it, but from now on you will use it every day.

To help you with your research, here are today's concepts (in no particular order):

- Classes definition

- Objects and instantiations

- Constructors

- Methods and attributes

- New operator

- Methods and attributes visibility

- Optional parameters






#Exercise 01 #hfill 1pt

**File to hand in**: pool_java_d02/ex_01/Gecko.java#br

Create a new class "Gecko".
Every time a new Gecko is created, it will automatically display "Hello!" followed by a newline.#br

Here is a test example:
```java
	public class Example {
		public static void main(String[] args) {
			Gecko arthur = new Gecko();
			Gecko benjy = new Gecko();
	     }
      }
```

#terminal(java Example
Hello!
Hello!)





#Exercise 02 #hfill 1pt

**File to hand in**: pool_java_d02/ex_02/Gecko.java#br

Copy your "Gecko.java" from exercise 01.
Add a new constructor so that it is possible to pass a name as parameter for the newly-created Gecko.
If a name is specified, "Hello &lt;Name&gt;!" followed by a newline must be displayed.
This parameter will need to be stored in a public "name" attribute.#br

Here is a test example:
```java
	public class Example {
		public static void main(String[] args) {
			Gecko arthur = new Gecko("Arthur");
			Gecko benjy = new Gecko();

			System.out.println(arthur.name);
			System.out.println(benjy.name);
		}
	}
```
#terminal(java Example | cat -e
Hello Arthur!$
Hello!$
Arthur$
$)





#Exercise 03 #hfill 1pt

**File to hand in**: pool_java_d02/ex_03/Gecko.java#br

Once again, copy your "Gecko.java" from the previous exercise.#br

Ah, speaking of names... 
From now on, the "name" attribute should not be public. 

#hint(You have to find out for yourself what it should be.)

However, so that the name is available outside the object, you need to create your very first method! It must be called **getName** and return... the Gecko's name!#br

Here is a test example:
```java
	public class Example {
		public static void main(String[] args) {
			Gecko arthur = new Gecko("Arthur");
			Gecko benjy = new Gecko();

			System.out.println(arthur.getName());
			System.out.println(benjy.getName());
		}
	}
```
#terminal(java Example | cat -e
Hello Arthur!$
Hello!$
Arthur$
$)





#Exercise 04 #hfill 1pt

**File to hand in**: pool_java_d02/ex_04/Gecko.java#br

Copy your "Gecko.java" from the previous exercise.#br

Add a new "Age" attribute to your Gecko class.
It should be possible to set it as a second parameter during the construction of the object.
This attribute must have its own getter and setter, respectively **getAge** and **setAge**.#br

Also add a new "status" method to your Gecko.
This method must take no parameters and display a sentence according to the Gecko's age.

#warn(You must use a "switch" statement and you are not allowed to use the "if" keyword in this method.)

The method must display the following sentences:

- "Unborn Gecko", if the age is 0.

- "Baby Gecko", if the age is 1 or 2.

- "Adult Gecko", if the age is between 3 and 10.

- "Old Gecko", if the age is between 11 and 13.

- "Impossible Gecko", otherwise.

#warn(Each of these sentences must be followed by a newline.)





#newpage

#Exercise 05 #hfill 1pt

**File to hand in**: pool_java_d02/ex_05/Gecko.java#br

It is time to give the gift of gab to our Geckos!
First, copy your previous "Gecko.java".
Add a new public method to it, called **hello**.#br

When called with a string, it must display "Hello &lt;string&gt;, I'm &lt;Name&gt;!", 
with &lt;string&gt; being the string given as parameter 
and &lt;Name&gt; being the name of the Gecko.
If our Gecko doesn't have a name, it must only display "Hello &lt;string&gt;!".#br

However, if an integer is given as parameter, it must display "Hello, I'm &lt;Name&gt;!" as often as the number given as parameter. 
Once again, if our Gecko doesn't have a name, it must display "Hello!".#br

#warn(Every messages must be followed by a newline.)

In all other cases, the method does nothing.
```java
	public class Example {
		public static void main(String[] args) {
			Gecko mimi = new Gecko("mimi");
			mimi.hello("Titi");
			mimi.hello(2);
	     }
      }
```
#terminal(java Example | cat -e
Hello mimi!$
Hello Titi\, I'm mimi!$
Hello\, I'm mimi!$
Hello\, I'm mimi!$)






#Exercise 06 #hfill 1pt

**File to hand in**: pool_java_d02/ex_06/Gecko.java#br

Copy your "Gecko.java" from the previous exercise.
Add a new **eat** method to your Gecko. It takes a string as parameter and returns nothing.
If the value of the string given as parameter is equal to "Meat", the Gecko must display:

	Yummy!

If the value is equal to "Vegetable", your Gecko must say:

	Erk!

If the value is anything else, the Gecko must display:

	I can't eat this!

#warn(As usual\, every sentence will be followed by a newline.)

#warn(The 'eat' method must be case insensitive!)

Moreover, add a new "energy" attribute to our Gecko.
By default it is equal to 100.#br

Add a setter and a getter for this attribute (**getEnergy** and **setEnergy**).
Every time our Gecko eats something, he will win or lose some energy.#br

If he eats meat, he will win 10 Energy.
However, if he eats vegetables, he will lose 10 Energy (a Gecko is carnivorous).
In all other cases his energy will not be modified.#br

A gecko’s energy should always be between 0 and 100 (included).






#Exercise 07 #hfill 1pt

**File to hand in**: pool_java_d02/ex_07/Gecko.java#br

Copy your "Gecko.java" from the previous exercise.
Implement a new **work** method in our Gecko class that takes no parameter and returns nothing.#br

With every call, if the Gecko has at least 25 energy, it will display:

	I'm working T.T

Then the energy will decrease by 9 (he is working 8 hours a day so he needs enough energy to work the whole day).#br

If the method is called with less than 25 energy, it will display:

	"Heyyy I'm too sleepy, better take a nap!"

Then it will give your Gecko back 50 energy.

#warn(As usual\, every sentence will be followed by a newline.)




#newpage

# BONUS - Exercise 08 #hfill 1pt

**File to hand in**: pool_java_d02/ex_08/ex_08.java#br

Let’s implement a new **fraternize** method that takes one parameter.
If the parameter is a Gecko Object, our Gecko will be happy and go drink with his friend.
It will cost both of them 30 Energy and they will both say:

	I’m going to drink with <otherName>!

If one of them doesn't have enough energy, he will say:

	Sorry <otherName>, I'm too tired to go out tonight.

and the other will answer:

	Oh! That's too bad, another time then!

If both of them are too tired to go out, they will both say:

	Not today!

#hint(You don't have to handle the cases where one of the geckos is nameless.)

If the parameter is something other than a Gecko, your Gecko will say

	No way

Except, if this something else is a Snake, and if its energy is greater than or equal to 10, it must be set to 0 and he will say:

	LET'S RUN AWAY!!!

If its energy is less than 10, it will play dead and display:

	...

Finally, if your Gecko has successfully gone out, he will need to throw a 6-faced dice for EVERY action he will try to do until he takes a nap, and if the result is 1 he will not complete his action.
Instead, he will display:

	I'm too drunk for that. hic!

#hint(Feel free to add any method that you reckon necessary.)