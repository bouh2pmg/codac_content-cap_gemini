---
module:			Cap Gemini
title:			day 03
subtitle:		HTML/CSS pool
background:		'../background/EpitechCapBackground.png'

repository: 	Pool_HTMLCSS_Day_03
repoRights:		gecko
language:		HTML/CSS

noErrorMess:	true
author:			Pierre ROBERT & Vincent TRUBESSET
version:		1.0
---

#warn(You need to have complexted yesterday's exercises before starting this new day.)

As yesterday, today's exercises are not independent from each others .
Each exercise reuse the previous one, and the first exercises reuse yesterday's last exercise.
They are to be done one after the other.

#hint(We advise you to systematically copy/paste the previous #Exercise before adding the requested functionalities.)

As a reminder, today exercises will not be corrected automatically.

#warn(All HTML pages must respect the basic HTML structure.)




#newpage

#Exercise 01 – Responsive design #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_03/ex_01/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_01/codingpedia.css

#imageRight(res/portrait.png, 130px, 5)

$~$
Adapt your __index.html__ page so that it has a resolution of 400*600px.#br

#warn(Adapt your site to make each page responsive.)

$~$
$~$
$~$
$~$
$~$
$~$
$~$
Adapt your __index.html__ page so that it has a resolution of 1280*720px in a new stylesheet.
#imageCenter(res/landscape.png, 425px) 




 

#Exercise 02 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_02/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_02/codingpedia.css
**Restriction**:  use of the function calc() #br

In your _index.html_ page, integrate a "div" element that has a class "container".#br

Upon display, the size of the container must be smaller than the total width of the page.
You must apply a 20 pixels space on each side without using margins. 
Inside, you must implement a paragraph with the following text: "I have 40 pixels less in my container".







#Exercise 03 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_03/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_03/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
In this container, add a new element with an "id" equal to "lin".
This element must display a color gradient from light to dark blue in CSS only.
 





#Exercise 04 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_04/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_04/codingpedia.css #br

Integrate a paragraph in your __index.html__ page.
Select the first letter of the phrase in this paragraph and apply a grey background color to it.
In addition, you must raise its font size to 24.

#hint(selector)










#Exercise 05 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_03/ex_05/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_05/codingpedia.css #br


In your __index.html__ page, integrate a "div" element that has a class "container". 
Implement 3 elements in the container with a class named "block".
Two of them must have an attribute named foobar.
One of these attributes will have value "foo1" and the other "foo2".#br

The "block" class must represent a green square with 50 pixels’ sides. 
The element having attribute "foo1" must have a red background and the one with attribute "foo2" must have a blue background.










#Exercise 06 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_06/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_06/codingpedia.css #br


Integrate a paragraph containing the phrase "I am an uncommon font".
Apply the "Satisfy" font to this paragraph.






#Exercise 07 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_07/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_07/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
In this element, introduce another element that has a "mybutton" class. 
In this element of "mybutton" class, insert the text "Send".
This element must have a yellow border of 3 pixels width and rounded angles.






#Exercise 08 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_08/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_08/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
In this element, add an element of class "mybutton".
In the "mybutton" element, add the text "Send".#br

This text will have a border made with the 'border.png' image.
#hint(This asset will be given to you.)






#Exercise 09 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_09/index.html
#space(1.5)	Pool_HTMLCSS_Day_03/ex_09/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
This div must contain a paragraph with the following text: "I am a customized text".
This text must have a shadow.





#newpage

#Exercise 10 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_03/ex_10/index.html, Pool_HTMLCSS_Day_03/ex_10/codingpedia.css

In your __index.html__ page, integrate a "div" element that has a class "container". 
In this element, display three other square-shaped elements with an "inline-block" display type.
Each of these elements will be a square with 120 pixels sides and be aligned to their top side:

1. the first element must have id "block1" with an orange background color,
2. the second element must have id "block2" with a white background color, a 10 px yellow border and an 8 pixels padding,
3. the third element must have id "block3" with a black background color and a 32 px padding.

These three elements must have the same size when they are displayed.

#hint(box-sizing)






#Exercise 11 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_11/index.html
#space(1.5)	 Pool_HTMLCSS_Day_03/ex_11/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
This element must be 500 px wide.
In addition, this element must contain a paragraph in which a [lorem ipsum](http://www.faux-texte.com/lorem-ipsum-15.htm) must be displayed in three columns (in CSS only).







#Exercise 12 #hfill 1pt

**Turn in**: Pool_HTMLCSS_Day_03/ex_12/index.html
#space(1.5)	 Pool_HTMLCSS_Day_03/ex_12/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
Add two images in the background. Both images must be visible.
The first image, 'paper.gif', must cover all of the container and the second image, 'flower.gif', must be positioned in the bottom right corner of the container.
#hint(These assets will be given to you.)





#Exercise 13 #hfill 2pts

**Turn in**: Pool_HTMLCSS_Day_03/ex_13/index.html
#space(1.5)	 Pool_HTMLCSS_Day_03/ex_13/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
In this element, add a bulleted list containing an element of your choice.
By using a CSS "pseudo class", add "Element: " before each "li" element and "end of sentence" after each "li" element.






#newpage

#Exercise 14 #hfill 3pts

**Turn in**: Pool_HTMLCSS_Day_03/ex_14/index.html
#space(1.5)	 Pool_HTMLCSS_Day_03/ex_14/codingpedia.css #br

In your __index.html__ page, integrate a "div" element that has a class "container". 
This element will represent a red, 100px * 100px square.#br

You must now have the square move.
First, the square must move 300 pixels to the bottom and take on a green color.
Then, the square will move 300 pixels to the right and take on a blue color.
Then, the square will move up 300 pixels and take on a yellow color.
Finally, the square returns to its starting position and its color goes back to red.

#warn(This movement must be performed 5 times before stopping.
Each cycle must last 5 seconds.)
