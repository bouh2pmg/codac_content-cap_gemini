---
module:			Cap Gemini
title:			day 05
subtitle:		HTML/CSS pool
background:		'../background/EpitechCapBackground.png'

noFormalties:		true
author:			Pauline FREY & Vincent TRUBESSET
version:		1.0
---

# Mock-up

The design, usually in a PSD file, is static.
The mock-up is like a picture of what your website should look like.
It's a graphical proposition ready for integration. #br
You now have the keys to make this exercise. #br
You must realise an HTML page from the mock-ups below.
You decide which of Batman or Unicorns is the best. #br

You'll need to use the best HTML tags to be coherent with the content.
You can use Materialize and you can overload it in your own CSS files. #br

#hint(Refer to the mock-ups supplied with the subject on the intranet.)