---
module:			Cap Gemini
title:			day 04
subtitle:		HTML/CSS pool
background:		'../background/EpitechCapBackground.png'

repository: 		Pool_HTMLCSS_Day_04
repoRights:		gecko
language:		HTML/CSS

noErrorMess:		true
author:			Pauline FREY & Vincent TRUBESSET
version:		1.0
---

# Introduction

In the present subject we will address the responsive design, thought mobile first. #br
We have seen how to build an HTML page: now we will see how to make it pretty and fit all screens such as desktop, tablet and smartphone. #br

## Responsive Design

It is an approach used to develop sites that offer an optimal reading and browsing experience to the user, regardless of their devices: mobile phones, tablets, readers, desktop, printers, televisions, etc. #br
Traditionally, a web project that adopts a responsive design strategy will tend to think of the fixed version of the site as a starting point, and it will then be necessary to *reduce* this version as the size of the consultation materials decreases, according to a predefined grid. #br
This seems logical, since it feels like the majority of the web traffic comes from fixed supports when it actually comes from mobile devices and most particularly smatphones.

#newpage

However, if you take the contents of a bottle of water and pour it all into a glass of water...

#center(#hugeText(It's overflowing!))

#imageCenter(res/content.jpg, 442px)

## Mobile first

Another way of doing things has beeing talked about: the **mobile first** approach. #br
As the name suggests, this approach consists to think about navigation, ergonomics and site content for the smallest consultation materialsfirst, and then upscaling them. #br
This is called __progressive enhancement__ vs __graceful degradation__. #br
If this approach begins to have some success, it is because it has some undeniable advantages that do not only concern mobile versions of sites.

#newpage

# Prerogative

You must use **Flexbox** and the **`viewport` meta tag**. #br
For these exercises, you'll need to find the browser option - or a plug-in - that allows you to change the viewport.

# Exercise 01 #hfill 1pt
**Turn in**: Pool_HTMLCSS_Day_04/ex_01/index.html, Pool_HTMLCSS_Day_01/ex_02/style.css #br

Create the **index.html** and **style.css** files then include **style.css** in **index.html**.

# Exercise 02 – HTML #hfill 2pts
**Turn in**: Pool_HTMLCSS_Day_04/ex_02/index.html #br

Structure your HTML this way:
- A header with a nav.
- A first section.
- A section which contain six articles.
- A footer.

#newpage

# Exercise 03 – Smartphone #hfill 3pts
**Turn in**: Pool_HTMLCSS_Day_04/ex_03/index.html, Pool_HTMLCSS_Day_01/ex_03/style.css #br

You must reproduce this layout for a device with a maximum resolution width of 767px. #br

#imageCenter(res/responsive/phone.png, 242px)

#newpage

# Exercise 04 – Tablet #hfill 3pts
**Turn in**: Pool_HTMLCSS_Day_04/ex_04/index.html, Pool_HTMLCSS_Day_01/ex_04/style.css #br

You must reproduce this layout for a device with a resolution width between 768px and 1024px.
As you can see, two new articles appeared in the second section. #br

#imageCenter(res/responsive/tablet.png, 400px)

# Exercise 05 – Desktop #hfill 3pts
**Turn in**: Pool_HTMLCSS_Day_04/ex_05/index.html, Pool_HTMLCSS_Day_01/ex_05/style.css #br

You must reproduce this layout for a device with a minimum resolution width of 1024px.
Again, you can see that two new articles appeared in the second section. #br

#imageCenter(res/responsive/desktop.png, 400px)

#newpage

# Prerogative
For the next exercise, you must not create CSS rules: use only **Materialize** features. #br
The HTML Style attribute is forbidden. #br

Titles must be correctly implemented:
- LASTNAME FIRSTNAME
- Heading
- About
- Story
- Profil
- Skills
- Portfolio
- Last project
- Contact #br

Choose a color! #br

# Exercise 01
Create an HTML page **index.html** and put the **jQuery** and **Materialize**'s CDNs inside.

# Exercise 02
Apply a light grey background color to all pages. #br

Create a div taking the full width of the page. Use your chosen color as its background one. #br

Create a first title that will be your name in white.
Make a second title that will be your profile heading in grey with a white background.

# Exercise 03
Create a container div.
It must contain a row with a title "About me". #br

In this row you will put a first div which must be:
- 12 columns large for a small sized screen
- 4 columns large for a medium to large screen #br

Add to it a "Story" title, and a quick explanation about your professional background.

# Exercise 04
Inside the "About me" row, make a div of the same size as the "Story" one, with a title "Profil".
It's background color will be the color you choose. #br

Now make a card: the inner text must be white, with a responsive round profil picture next to it. #br

Add a div **`card-action`** after the card you just made, which will contain links to your **Github** and **Linkedin** profiles.

# Exercise 05

Finally, the last div in the "About me" row will be titled "My skills" and must have the same size as the 2 previous divs. #br

Inside it you must create a table with a grey row every 2 rows. #br

This table must contain the languages, frameworks you know with a small comment for each one.

# Exercise 06
You must create a new row, which will have "Portfolio" and "My last projects" as titles. #br
Iit must contain 6 photos with 3 photos for each row.

# Exercise 07
At last, you must create a row titled "Contact". #br

Make a new row inside it, which will contain a full column sized form.

In that form, make a first row, which will contain 2 inputs: "Lastname" and "Firstname".
Each one must be half the row size. #br

Make a second row containing a full column sized email input.
The last row will contain a full column sized text area, with a "Your message" placeholder. #br

Titles and buttons must be centered.
