---
module:		Cap Gemini
title:		day 01
subtitle:	     Java Pool
background:    '../background/EpitechCapBackground.png'

repository: 	pool_java_d01
repoRights:	gecko
language:		Java

groupSize:	1
noBonusDir:	true
noErrorMess:	true
author:		Hugo WALBECQ
version:		1.1
---

To run this pool, you need Java to be installed on your computer.
We are here talking about **JSE**, that you could easily find on Internet.#br

In Java, every line of code must be included into a class.
There must also be one file by class (and one class by file) named the same way.
Here is a very first example of Java code (necessary in a file name "HelloWorld.java"):
```java
     public class HelloWorld {
          public static void main(String[] args) {        
               System.out.println("Hello World!");
          }
     }
```
To compile it:

     javac HelloWorld.java

and to execute it:

     java HelloWorld

#warn(Make sure this code snippet displays "Hello World!" when you execute it.)

#hint(' seen the "main" function?)




# Exercise 01 #hfill 1pt

**File to hand in**: pool_java_d01/ex_01/ex_01.java
**Restrictions**: "System.out.print" is mandatory, and must be used only once.#br

Create a function named "myConcat" that takes two parameters.
The function must display the first parameter followed by a space followed by the second parameter.#br

**Prototype**
```java
	public static void myConcat(String str1, String str2);
```

**Example**
```java
     myConcat("Hello", "world");   // should display: "Hello world"
```





# Exercise 02 #hfill 2pts

**File to hand in**: pool_java_d01/ex_02/ex_02.java#br
**Restrictions**: the "for" keyword is mandatory.#br

Write a function that returns a string composed of as many "woof" as the value of the variable passed as parameter.#br

**Prototype**
```java
	public static String getAngryDog(int nbr);
```

**Example**
```java
     getAngryDog(3);     // returns "woofwoofwoof"
```





# Exercise 03 #hfill 2pts

**File to hand in**: pool_java_d01/ex_03/ex_03.java
**Restrictions**: the "for" keyword is mandatory.#br

Write a function that displays the values of the array passed as parameter.

#warn(Each value must be followed by a new line.)

**Prototype**
```java
     public static void printArray(ArrayList<String> myArray);
```




# Exercise 04 #hfill 2pts

**File to hand in**: pool_java_d01/ex_04/ex_04.java
**Restrictions**: the "switch" keyword is mandatory.#br

Write a function that takes an integer as parameter.
If the value is 3, it displays "The Three Brothers".
If the value is 6, it displays "The Sixth Sense".
For 23, it displays "The Number 23".
And for 28, it displays "28 Days Later".
For other values, it displays "I don't know.".

#warn(A new line will be called after each display.)

**Prototype**
```java
     public static void printMovieFromNbr(int nbr);
```

**Example**
```java
     printMovieFromNbr(23);
```





# Exercise 05 #hfill 2pts

**File to hand in**: pool_java_d01/ex_05/ex_05.java #br

Write a function that takes as parameter a variable number of arguments and returns these arguments in an array.#br

**Prototype**
```java
	public ArrayList<String> myGetArgs(String... var);
```






# Exercise 06 #hfill 2pts

**File to hand in** pool_java_d01/ex_06/ex_06.java#br

Write a function that outputs the following sequence to the n#high(th) iteration.
Your function should not print anything if you pass it a negative number.#br

Here is the beginning of the sequence:

     1
     11
     21
     1211
     111221
     312211

#warn(The first iteration is 0.)

**Prototype**
```java
	public static void sequence(int nbr);
```

**Example**
```java
	sequence(0);	// should output 1
```