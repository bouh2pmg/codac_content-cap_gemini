---
module:			Cap Gemini
title:			day 04
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_D04_01 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

During this first part, we will focus on the basics of jQuery.
For each exercise, you are asked to create a ".js" file.

#warn(In the following exercises\, the instructions must be executed after the page is fully loaded.
On each exercise you MUST provide the appropriate _index.html_ file.)

#hint(If you feel you are doing a bit of magic\, fear not!!! That’s because you are doing some magic!
Remember\, you are the master of all things\, a master among masters.
The DOM must bow before you.

Ps: Try not to get yourself hurt)




#newpage

#Exercise 01 #hfill 1pt
**Turn in**: ex_01/index.html, ex_01/selector.js#br

**At first there was something and then nothing**#br

This JS file must contain a function that selects all paragraphs on the page and makes them disappear. 




#Exercise 02 #hfill 1pt
**Turn in**: ex_02/index.html, ex_02/selector.js#br

**Try again**#br

This JS file must contain a function that selects an element with the id "test" and makes it disappear.
 




#Exercise 03 #hfill 1pt
**Turn in**: ex_03/index.html, ex_03/selector.js#br

**Third time’s the charm**#br

This JS file must contain a function that selects an element with class "test" and makes it disappear.






#Exercise 04 #hfill 1pt
**Turn in**: ex_04/index.html, ex_04/selector.js#br

**Half and half**#br

This JS file must contain a function that selects all elements of hyperlink type that do not have the target "_blank" attribute and makes them semi-transparent with 50% opacity. 






#Exercise 05 #hfill 2pts
**Turn in**: ex_05/index.html, ex_05/selector.js#br

**He was the first of his kind and then he was not**#br

This JS file must contain a function that selects the first element of the first unordered list and makes it disappear.
 





#Exercise 06 #hfill 1pt
**Turn in**: ex_06/index.html, ex_06/event.js#br

**Make them aware of your power**#br

This JS file must contain a function that assigns a "click" event on the first "button" element of the page.
The action of the event must make all paragraphs of the page disappear.






#Exercise 07 #hfill 2pts
**Turn in**: ex_07/index.html, ex_07/event.js#br

**Make them all pale and vanish as you pronounce the words**#br

This JS file must contain a function that assigns an "on" event to all paragraphs of the page.
3 actions must be assigned to this event:

1. when the mouse enters a paragraph, the background color of the paragraph must become light gray,
2.when the mouse leaves the paragraph, the background color must revert to white,
3.when you click on the paragraph, its text must disappear.






#Exercise 08 #hfill 1pt
**Turn in**: ex_08/index.html, ex_08/event.js#br

**As you wave your hand it’s here, then it’s not, then again it’s here**#br

This JS file must contain a function that assigns a "click" event on the "button" element of the page.
The action of the event must make the element with id "menu" appear or disappear.
 





#Exercise 9 #hfill 2pts
**Turn in**: ex_09/index.html, ex_09/animate.js#br

**One shall levitate as you change the scenery**#br

This JS file must contain a function that assigns a "click" event on the element with class "platypus."
The action of the event must move the element with class "platypus" 150 pixels to the right, 200 pixels to the bottom and set the background color to green.




#Exercise 10 #hfill 2pts
**Turn in**: ex_10/index.html, ex_10/callback.js#br

**Amber alert**#br

This JS file must contain a function that selects the element with class "test" in the page and makes it disappear.
In addition, an alert must be sent after the disappearance of the element with the message "The paragraph is now hidden."





#Exercise 11 #hfill 2pts
**Turn in**: ex_11/index.html, ex_11/append.js#br

**Fill the void**#br

Add an input with the id "listItem" in your page. And a button.
Add a function which will be called every time a click happens on this button with the input you just created as parameter.
This function must add a div after this element, that contains the value of the element passed as parameter.






#Exercise 12 #hfill 1pt
**Turn in**: ex_12/index.html, ex_12/insert.js#br

**In-between**#br

This JS file must contain a function that inserts the sentence "Wow, I precede the image!" before the first image and "Hey, I come in last" right after.





#Exercise 13 #hfill 1pt
**Turn in**: ex_13/index.html, ex_13/removed.js#br

**Outcasts**#br

This JS file must contain a function that removes from the page all paragraphs with classes "test" and "platypus."

#warn(You must call on a single function.)






#Exercise 14 #hfill 2pts
**Turn in**: ex_14/main.js#br

**I appoint you "blue class"!**#br

This JS file must contain a function that, when hovering over a paragraph, adds the "blue" class to it.
In addition, you should be able to include or remove a paragraph from the "highlighted" class with a single mouse click.
