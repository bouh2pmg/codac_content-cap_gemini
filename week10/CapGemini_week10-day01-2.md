---
module:			Cap Gemini
title:			day 04
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_D04_02 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

#warn(In the following exercises\, the instructions must be executed after the page is fully loaded.
On each exercise you MUST provide the appropriate _index.html_ file.)

#hint(Remember that you’re coding in JQuery\, so you must use the JQuery alternative to do something\, and not pure JS. )




#newpage


#Exercise 01 #hfill 3pts
**Turn in**: JavaScript_D04_02/ex_01/#br

Create an input allowing someone to enter a text.
When the text is considered valid, add it to a bulleted list displayed under the input.





#Exercise 02 #hfill 3pts
**Turn in**: JavaScript_D04_02/ex_02/#br

Define a few CSS classes:

* a __note__ class, putting the border of the element in blue,
* an __email__ class, putting the border of the element in green,
* a __todo___ class, putting the border of the element in red.

It should now be possible to precise one of this 3 types (note, email, todo) when validating the input.

#warn(You’re free on the way you implement this and you can add more style to your CSS.
However if an input of type "email" is created\, the element displayed with the content must possess the class CSS "email".
It must work the same way for the 2 other types.)

#hint(You may need to add some different check to validate the inputs depending on the type.)





#Exercise 03 #hfill 3pts
**Turn in**: JavaScript_D04_02/ex_03/#br

Implement a search features among the created elements.

#hint(As of now you only need to handle a search by types (you will implement a search by words in the following exercise).)

Searching for "email", only the elements being "email" must stay displayed.
The others must not be deleted and must be shown again when the search is over.

#hint(Once again\, you’re free concerning the implementation).






#Exercise 04 #hfill 4pts
**Turn in**: JavaScript_D04_02/ex_04/#br

Upgrade your search feature.
It should now be possible to search by a word or a part of word contained in the elements.
For example, searching for "rick" when there are a "patrick@something.com" email and a "Send a mail to Rick" todo, both should be displayed.#br

Add the possibility to combine your search with words and types, so in the previous example, one could choose to search only todos, which would only display "Send a mail to Rick".

#warn(Once again\, there should be no element destroyed\, and it should be possible to switch from a search to another without the need to reset everything.
For example\, if I selected "todo" and "rick" in my search but now decided I want all the types I should not need to erase everything and re-do my selection.)






#Exercise 05 #hfill 2pts
**Turn in**: JavaScript_D04_02/ex_05/#br

Let’s now upgrade your input.
Make it possible to add tags to an element.
The tags must also be displayed.
It should be possible to add multiple tags to an element, even after its creation.





#Exercise 06 #hfill 5pts
**Turn in**: JavaScript_D04_02/ex_06/#br

Let’s upgrade our search engine a little more.
It should now be possible to search for "tags", but also for two types of elements at the same time ("email" and "todo" for example).#br

Moreover, add the possibility to search for multiples tags at the same time.
It should also be possible to precise if the tags must all be included or not.#br

For example, if I have

* a "todo" element with the following tags: "work", "tired", "dailyLife".
* an "email" element with the following tags: "work", "boss".
* a "note" element with the followings tags: "boss", "money".

If I search for tags "work", the "todo" and "email" must be displayed. 
If I now search for "work" and "boss" (both being included), only the "email" must be displayed.
If I search for "dailyLife" or "money" (one or the other), the "todo" and "note" must be displayed.

#hint(We do not ask you to handle cases like: ("work" && "boss") || ( "boss" && "money"). Neither all the twisted cases you may imagine.
However if you do handle those cases\, you may be awarded bonus points.)




#Bonus

Here is a little idea for an interesting bonus.
Add a bit of PHP for the backend, and make it possible to save all the elements created with all the fields in a database by calling PHP functions.
It would now be possible to leave the page and come again to see all the elements previously saved be displayed again.
