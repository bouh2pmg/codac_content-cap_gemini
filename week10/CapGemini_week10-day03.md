---
module:			Cap Gemini
title:			rush
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_RUSH 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

#warn(All requested methods in the following exercises must be executed after complete loading of the page. )

There are 2 subjects. 
You are free to choose the one you prefer, or to do both:

* **2048**: it consists in a [2048 game](https://gabrielecirulli.github.io/2048/) in jQuery.
* **mindMap**: it consists in a [mind map application](http://www.mindmapping.com/) in jQuery.

Your project must encapsulated in a plugin that makes it easy to integrate within an HTML page.

#hint(For all the exercises\, you will need the code of the preceeding exercise to integrate the requested new functionalities.)

In each case, the file to hand in must be named _main.js_.

#hint(Most of the instructions are not strict but indicatives. If you think you have a better idea\, you can try it.)






#Exercise 01 #hfill 2pts
**Restriction**: the use of __$.fn__#br

Use the functionalities of __$.fn.extend__ to create an empty plugin named __mygame__ or __mymindmap__ with a function having a size as parameter.

#hint(Check [this](https://learn.jquery.com/plugins/basic-plugin-creation/).)




#Exercise 02 #hfill 2pts
Within this function, create the following variables:

* __gameObject__ or __mapObject__ that gets the id of the element to which the plugin is applied.
* __blockSize__ or __mindSize__ that records the size of the side of an atomic element (whether a square passed or an atomic element of the map, that we'll call _mind_).
* __boardSize__ or __mapSize__ which represents the size of the side of the game board or the map (both begin square). 

#hint(To calculate the board size\, take into account that the game board is a square in which are laid out 16 small identical squares\, for which the length of one side is worth __blockSize__.)

Once these 3 variables are created, call a method that introduces a div element in the element calling the plugin.
This div must have as id _board_ or _mind-master_ and as size the value specified by __boardSize__ or __mapSize__.

#hint(You also can use a class “board” or "mind-master" if you want to make a more generic plugin.)





#Exercise 03 #hfill 4pts#br

##2048

Our board is for now virtually divided into 16 sub squares.
Implement now a function inside your plugin which inserts 16 div in the parent div _board_.

#warn(All these div will have to fill the board div and make up a 4 x 4 div square.)

These div will have __blockSize__ as size length.#br

We will add the class __square-container__ to those div.
To display it properly, let’s add a margin to those div.
Finally, let’s order the squares properly. The goal is to obtain a 4x4 grid.

#hint(Use CSS (inside our plugin) for that...)

##Mindmap

Our board now contains a _mind-master_ div, which will be linked with all new minds.
Implement a function inside your plugin which inserts one new _mind_ div to the parent div _mind-master_.
This function has to take one string parameter that contains the text to display in the mind.
These div will have a width based on "mindSize".#br

Let's add the class __mind-container__ to those div (you can add a div inside to store the text if you want to).
Add a style sheet to these divs.
Finally, let’s order the squares properly. The goal is to obtain a readable mind map. 

#warn(The minds must be linked to the master.
Theses links must be visible.
The minds should surround the master\, but you are free to place them the way you want.)

#hint(You can draw the lines with canvas or CSS.)








#Exercise 04 #hfill 2pts

##2048

Now we have a grid, let’s create a function that randomly adds a tile to the grid.
The placement of the tile must be random among empty cells.
The value of the generated tile must be random between 2 and 4.

#hint(The first time this function is called\, 2 tiles must be created at the same time.)

##Mindmap

We now have a map, and can create new minds.
The next step is to have the user to modify the minds himself.
Bind the double left click to allow the user to edit the text of the div, changing it in an editable text field.
When he’s done and click left again somewhere else, the div should go back to its immutable state.

#hint(There is an event to do this.)





#newpage

#Exercise 05 #hfill 5pts

##2048

Let's implement the movement of tiles. 
You need to handle the four directions: when a arrow key is pressed (left, right, bottom, top), all tiles must move toward the direction of the key in the grid as far as they can go.#br

Moreover, every time a key is pressed a new tile must be generated.
You may choose yourself if you want those actions to happen as soon as the key is pressed or after the key has been released. 

#hint(One is easier than the other.)

#warn(It is more difficult than it seems.)


##Mindmap

Let's implement the addition of sub-minds.
If the user clicks on a mind, and then press "Enter", it should create a new mind, linked to the selected one.

#hint(You should also be able to create a new sub-mind to a sub-mind...)

#hint(You should just improve your function from exercise 03 with a new argument "parent".)




#Exercise 06 #hfill 2pts

##2048

The most important step is still missing: when a tile collides with another, they must melt if they have the same value.

#hint(When more than one collision happens at the same time in the same row / or column\, the priority is given to the tiles which are closer to the side where the movement is directed.)

For example: 

	0 0 0 0
	0 2 2 2
	0 0 0 0
	0 0 0 0

gives, by moving right: 

	0 0 0 0
	0 0 2 4
	0 0 0 0 
	0 0 0 0


##Mindmap

The last step is now to have the minds movable.
That means the user has to be able to drop-down any mind (except the master) and change its position (under / over another) and its parent:

* if the user puts a mind on another one, it should add it as its child,
* if he moves it elsewhere, you should just make it move it around its parent.



#Exercise 07 #hfill 3pts

Your project should now be completed, but plain looking and not very attracting.
To make it nices:

* use CSS or add some images to represent the elements,
* add some jQuery animations.
* make the elements expandable and reducable.


#Bonus

You've done with everything? 
Congratulations!
Hope you also checked that your code is clean and follow the jQuery plugin good practices...#br

Here are a few ideas as bonus for each project.

##2048

* Multiple skins of images which can be selected in a dropdown list (a skin Star Wars would be great!),
* a page and a grid more beautiful,
* undo button,
* score,
* skin of image directly "uploaded" from your webpage (i.e images can be specified manually for each tiles)...

##Mindmap

* Create a new kind of links between any mind,
* add options to change the colors, fonts,...,
* make the font adapted to the div size,
* build a collaborative application. Every user can modify the same mindmap and see the other’s modifications.