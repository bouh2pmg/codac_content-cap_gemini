---
module:			Cap Gemini
title:			day 05
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_D05 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

##What is AJAX
AJAX is for __A__synchronous __JA__vascript and __X__ML.
It is a set of technologies which enable communication between a client (your web browser) and a server (apache/nginx) without refreshing your web page.

##Asynchronous and callbacks
So far, you always programmed is in a synchronous way. It means that each statement is carried out in the order you specified.
The __asynchronous__ way uses callbacks.
It is a function, called only when a particular event is triggered.
A simple example: you want to make an AJAX request to a laggy server.
This request takes time. If it is synchronous, you will wait for the request to complete before doing anything else. It may take time.
With asynchronous programming, you can tell "execute this function when you will end, even if it takes 30 seconds, it doesn't matter" and you can continue to do your things aside.

##Some precisions:
We will use the "old school" way of doing things for server side code (this way to create responses is not used in frameworks such as Symfony).
You'll be asked to work with echo and PHP's JSON encoder.

#warn(You will see with Symfony that this way is totally out of the picture.
Instead of echoing JSON data\, Symfony and number of other frameworks allow you to create objects such as a JsonResponse object that override the practice that I'm going to ask you to use.
Feel free to go and check these out to see what I am talking about.)





#Exercise 00
**You've Got Mail**#br

First thing first.
Create a skeleton of an html page you will be using this whole day with jQuery.
You need to use the plugin __Bootstrap__.

#hint(Bootstrap is going to make notification appear for us.)

Include the JavaScript file in your following html page: _bootstrap/js/bootstrap.min.js_.

#hint(Feel free to check the [documentation](http://getbootstrap.com/) to learn how to use it.)


Look at [this page](http://getbootstrap.com/javascript/) and use some of the javascripts components in an new javascript file (for instance _js/main.js_) that you will include in your html page too.






#Exercise 01
**I said it hundred times and I will say it even more**#br

1. Read the jQuery documentation about __$.ajax()__.
2. Read it twice... Really, I mean it!






#Exercise 02 #hfill 3pts
**Let's do some AJAX**#br

**Turn in**: ex02.html, ex02.php#br

The PHP script contains an 'echo' of an array containing __name__ as key and your name as value.
The whole array must be encoded with Json.

#hint([php.net](http://www.php.net) may help you for the encode thing...)

#warn(You MUST specify in the header that you are going to communicate JSON data.)

Your script MUST be in script tags.
It is a tiny code, so you don't need a file for it.#br

In the HTML code, create a button. 
When pressed, an Ajax call using a GET method is done in the PHP file.
Upon success you will create a notification containing your name.
Either way (successful or not), when the Ajax call ends, create a notification too.






#Exercise 03 #hfill 5pts
**Hello Server**#br

**Turn in**: ex03.html, ex03.php#br

In your HTML, add a form containing an input __email__.#br

When the form is submitted, it sends a POST request to the server containing the serialized data of the form.
In your PHP script, check the email validity. 

#hint(In case of an error or if the mail is not valid\, change the HTTP status code to 400.)

You must display a notification in those three situations:

1. if the mail is valid,
2. if the mail is not,
3. in either case.






#Exercise 04 #hfill 2pts
**Get that?!?!**#br

Now, load script on the fly with __$.getScript()__.

#hint(Read jQuery's documentation.)

You must modify the HTML skeleton you created earlier, just remove the line where you included the Bootstrap script.
Create a button __load bootstrap__ that loads the Bootstrap file script and launches a notification when pressed.






#Exercise 05 #hfill 2pts
**Loading...**#br

We gave you a file _countries.json_.#br

We are going to make use of this file.
In your HTML page create an empty table which contains 2 columns one for the __country-code__ and another one for the __name__.#br

The page mut have a "load countries" button, that fills the table with the data contained in the _countries.json_ file.

#warn(In order to make this feature you MUST use __$.getJson()__ function.)




#newpage

#Exercise 06 #hfill 3pts

Create a simple _formName.html_ page with 2 fields that sends 2 strings.#br

The goal is to display if a brand can be added in the database.
The database must be created with _ajax_products.sql_.

#hint(You must always have two validation messages displayed\, one for each field.
You need to display these validation messages in real-time when you have a modification in fields (dynamic display).)

**Requirements**

* **Type** allows only alphabetical characters and '-'characters. Its length MUST be between 3 and 10 included.
* **Brand** field allows alphanumeric characters, '-' and '&'. Its length MUST be between 2 and 20 included.
* Both fields are case insensitive.
* The request to your database needs to be a GET request, made using Ajax and jQuery in a getProducts.js file.
* The PHP file must be named _checkProduct.php_.


**Validation messages**

* $type: this type does not have enough characters.
* $type: this type has too much characters.
* $type: this type has non-alphabetical characters (different from '-').
* $type: this type doesn't exist in our shop.
* $type: this type exists in databases.
* No type sent yet!
* $brand: this brand does not have enough characters.
* $brand: this brand has too much characters.
* $brand: this brand has invalid characters.
* $brand: this brand already exists in databases.
* $brand: this brand is valid for the type \\$type.
* No brand sent yet!

#warn(These messages MUST be highlighted with red color for an error message and green when it's valid.)





#Exercise 07 #hfill 3pts

Using the database created in the previous exercise, create a _checkProduct.html_ page with four input fields (type, brand, price, number) and a submit button to submit this form.#br

**Requirements**

* __Type__ field allows only alphabetical characters and '-'characters.
Its length MUST be between 3 and 10 included.
* __Brand__ field allows only alphanumeric characters, '-'and '&' are valid.
Its length MUST be between 2 and 20 included.
* __Price__ field needs to be protected with only numeric characters and '>', '<' or '='. The length needs to be between 2 and 5. (e.g.: '>100', '<42' or '=42').
* __Number__ field only allows positive numbers.
* These fields will all be case insensitive.
* The request MUST be a POST request made with Ajax and jQuery in product.js file.
* The PHP file name is checkProduct.php.
* The goal is to display the result of your research in an array.

#warn(You have to display one error message if the request doesn't succeed after having clicked on the button.)

Display the proper error message among:

* Error ($type): this type does not have enough characters.
* Error ($type): this type has too much characters.
* Error ($type): this type has non-alphabetical characters (different from '-').
* Error ($type): this type doesn't exist in our shop.
* Error ($brand): this brand does not have enough characters.
* Error ($brand): this brand has too much characters.
* Error ($brand): this brand has invalid characters.
* Error ($brand): this brand doesn't exist in databases.
* Error ($price): this price does not have enough characters.
* Error ($price): this price has too much characters.
* Error ($price): we cannot define a price - string invalid.
* Error ($price): No products found for this price.
* Error ($number): Sorry, we haven't enough stock, we have only \\$stock in stock.
* Error ($number): It's not a positive number.

If the request is valid, display the product in a 5-column-array (type, brand, price, number, stock) without refreshing the HTML page.

#warn(The content of the array must be cleared when a new search is done. 
The response MUST not contain HTML but JSON.)







#Exercise 08 #hfill 4pts
**Chit-Chat**#br

Let's create a mini-chat with Ajax.
Your chat will contain 2 text boxes: one for the name, and another one for the message.
You must display the older messages and the sender's name.

#warn(Messages must be refreshed every 4 seconds.)

#hint(Be creative\, you are free to use everything you've just seen.)