---
module:			Cap Gemini
title:			day 02
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_D02 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

For the next 10 exercises, you will have to use the JavaScript language in the browser.

#hint(The exercises are independent one from another; therefore\, you can solve them in the order that best suits you.)

The exercises are provided as HTML pages.
You have to interact with the HTML code given to solve the exercises.
These very HTML files will be used to correct your exercises, so make sure that your JavaScript files do not require to edit the HTML code.
You only have to hand in the JavaScript files.

#warn(You have to respect the indicated paths in the HTML files to create your javascript files.)

#warn(No edition of HTML code is necessary nor acceptable...
Never edit the HTML code of the files\, under any circumstances!)

#hint(If you don’t see any white box\, make sure the 'style.css' file is loaded.)

#warn(The use of external libraries or any code that you may have copy/pasted from the Internet is forbidden.)