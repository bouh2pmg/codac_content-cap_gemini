---
module:			Cap Gemini
title:			day 01
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_D01 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---

During the days to come, you will have a series of exercises to get familiar with the JavaScript language. 

#hint(The exercises are independent; therefore\, you can solve them in the order that best suits you\, but we recommend you to do it in the proposed order.)

#warn(We will use node to launch each of your JavaScript files.)



#newpage

#Exercise 01

**Turn in**: ex_01/drawTriangle.js#br

**Draw me a triangle**#br

Write a function _drawTriangle_ that takes a height as parameter and draws a triangle on the standard output.
Obviously the height corresponds to the triangle height (see below).#br

**Prototype**:	drawTriangle(height)  

#terminal(cat drawTrinagle.js

	//
	// your code here
	//
	drawTriangle(6);
#prompt node drawTriangle.js

	$
	$$
	$$$
	$$$$
	$$$$$
	$$$$$$
)





#newpage

#Exercise 02

**Turn in**: ex_02/arrayIsEqual.js#br

**An array and another array**#br

Write a function named _arrayIsEqual_ that returns true if both arrays passed as parameters are equal, false otherwise.#br

**Prototype**: arrayIsEqual(arr1, arr2)#br

#terminal(cat arrayIsEqual.js

	//
	// obvisouly this is done to show a small test
	//
	var a = [1\, 2];
	var b = [3\, 4];

	if (arrayIsEqual(a\, b)) {
		console.log("True");
	} else {
		console.log("False");
	}

#prompt node arrayIsEqual.js
False)




#newpage

#Exercise 03

**Turn in**: ex_03/countGs.js#br

**Keep good Count**#br

Write a _countGs_ function that takes a string as parameter and returns the number of uppercase 'G' characters it contains.#br

**Prototype**: countGs(str)







#Exercise 04

**Turn in**: ex_03/fizzBuzz.js  #br

**Baby steps in the dojo arena**#br

Write a _fizzBuzz_ function that prints all the numbers from 1 to 20.
Three requirements:

1. For numbers divisible by 3, print “Fizz” instead of the number.
2. For numbers divisible by 5 (and not 3), print “Buzz” instead of the number.
3. For numbers that are divisible by both 3 and 5 print “FizzBuzz”.

#warn(Every ouutput (be it a number or a string) should be comma separated.)

**Prototype**: fizzBuzz()#br

#terminal(node fizzBuzz.js | cat -e
1\, 2\, Fizz\, 4\, Buzz\, Fizz\, 7\, 8\, Fizz\, Buzz\, 11\, Fizz\, 13\, 14\, FizzBuzz\, 16\, 17\, Fizz\, 19\, Buzz$)






#Exercise 05

**Turn in**: ex_05/range.js
**Restriction**: only ES5 is allowed #br

**A good range**#br

Write a _range_ function that takes three arguments (start, end and step) and returns an array containing all the numbers from start up to (and including) end.
The third argument indicating the step value used to build up the array is optional. If not provided, the array elements go up by increments of one, corresponding to the classic stepping behavior.

#hint(Make sure it also works with negative step values so that range.)

**Prototype**: range(start, end, step)#br

#terminal(cat range.js

	//
	// your code is here
	//
	console.log(range(1\, 10\, 2));
	console.log(range(19\, 22));
	console.log(range(5\, 2\, -1));
#prompt node range.js

	[1\, 3\, 5\, 7\, 9]
	[19\, 20\, 21\, 22]
	[5\, 4\, 3\, 2]
)	





#newpage

#Exercise 06

**Turn in**: ex_06/objectsDeeplyEqual.js#br

**Equality is complex**#br

Write an _objectsDeeplyEqual_ function that takes two values and returns true only if they are the same value or are objects with the same properties whose values are also equal when compared with a recursive call to _objectsDeeplyEqual_. 

#hint(The word is out: you will be using recursion.)

#hint(Your function should find out whether to compare two things by identity or by looking at their properties.)

#hint('null' is also an "object".)

If your function passes the tests below your gecko will be happy. Your function is not supposed to be too complex\, keep in mind that this is only the first day.#br

**Prototype**: objectsDeeplyEqual(cmp1, cmp2)#br

#terminal(cat objectsDeeplyEqual.js

	//
	// your code here
	//
	var obj = {here: {is: "an"}\, object: 2};
	console.log(objectsDeeplyEqual(obj\, obj));
	console.log(objectsDeeplyEqual(obj\, {here: 1\, object: 2}));
	console.log(objectsDeeplyEqual(obj\, {here: {is: "an"}\, object: 2}));
#prompt node objectsDeeplyEqual.js
true
false
true)




#newpage

#Exercise 07
**Turn in**: arrayFiltering.js#br

**Iterating is key**#br

Write a _filter_ function that takes two arguments : an array and a _test_ function.
The argument named test is a function taht returns a boolean. You dont have to care about this function implementation.
This function must be called for each element contained in the array given as parameter. The return values determine whether an element is included in the returned array or not (if the test succeed, it should be included in the returning array).#br

The arrayFiltering function returns a new array containing filtered values.#br

**Prototype**: arrayFiltering(array, test)#br

#terminal(cat arrayFiltering.js

	//
	// your code here
	//
	var toFilter = [1\, 2\, 3\, 4\, 5\, 6\, 7\, 8\, 9];
	var passed = filter(toFilter, function (value) {
		return value % 3 === 0;
	});
	console.log(passed);
#prompt node arrayFiltering
[3\,6\,9])