---
module:			Cap Gemini
title:			day 03
subtitle:		Javascript
background:		'../background/EpitechCapBackground.png'

repository: 	JavaScript_D03 
repoRights:		gecko
language:		javascript
groupSize:		1

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.0
---



Today's goal is to __debug and achieve__ an already-starting project (available in the resources of this subject) from someone else.#br

It is heavily advised to know and understand the entire code (there is multiple JS files).
It may also be smart to inform yourself about the libraries used.
The previous developer, even though he left you with an unachieved project, tried to use good practice and/or code tricks, inspire yourself from it! 
Moreover, he also left a bit of documentation and comments in the code... Sadly he didn’t follow proper etiquette and left the commentaries in his native language... Anyway, use of all this information to succeed.

#hint(We also recommend you to regularly commit\, as usual\, it could allow you to turn back in an easier way if necessary.)

You are only allowed to use the libraries already included in the project (except for the last #Exercise).

#warn(All of the code that you will turn-in (even if it wasn’t yours to begin with) must be [JSLint](http://www.jslint.com/) valid\, without changing the directives present at the top of the files (if you need to do it\, you must be able to justify it during your defense).)

#warn(You must be able to explain every part of the code\, even if you didn’t code it initially.)

#hint(#Exercises don’t always have a link between them\, it is however heavily suggested to do them in the given order.)









#Exercise 00
Open your navigator’s web console (developers tools) and __correct the JS error__ displayed ("TypeError : player.setGame...").

#hint(For this exercise\, you do no need to modify the "game.js" file.)



#Exercise 01
As you probably guessed, the project is about a battleship...
Once ships have been fully placed, they must be correctly __erased from the big map__. Make it happen.

#hint(For this exercise\, you do no need to modify the "game.js" file)




#Exercise 02
Implement the "renderMiniMap" function.
It must __color the mini map cells__ (on the left) to correspond to the ships placements chosen by the player, with the color of the ship occupying it.

#hint(Ships positions are registered when the player clicks on the big grid.)




#Exercise 03
__Prevent ships to collide__ (that is to say, prevent 2 ships to be on the same cells), or to __go out of the field__.
If one of these conditions is not respected, it must not be possible to valid this ship position (we must stay on the ship placement and not continue to the next one).




#Exercise 04
Implement the possibility to __place ships vertically__, using a right click during the positioning phase to change the ship orientation.




#Exercise 05
Implement the __computer ships placement__.
The computer must not cheat, it has to follow the same rules as the player (ships can’t collide and can’t go outside of the map).

#hint(For this exercise\, you do not necessarily have to create new functions.)




#Exercise 06
After a shot, the cell shot must now __change color__ on the bigger map: red for a hit, and grey for a miss.
 



#Exercise 07
If a player chose a cell where he already shot, the __message must be different__ from the classical one.
The information (miss or hit) must not change and be identical to the first shot that happened on this cell.




#Exercise 08
Our computer is kind of stupid and always shoot in the top left corner.
__Upgrade it__ a little.

#hint(Obviously\, it must not cheat and its object must never access the grid property of the player.)




#Exercise 09
After the computer played, if the player has been hit (and only in this case) the __mini map must be updated__ to reflect this lost.
If a ship has been totally sunk, the CSS class "sunk" must be attributed to the matching ship icon (above the mini map).




#Exercise 10
After a computer turn, it should be possible for the player to start __playing his next turn__.

#warn(It is not possible as of now because of a bug...)
 



#Exercise 11
Implement the __end of game__ detection (function "gameIsOver").




#Exercise 12
Implement the possibility to choose __who starts the game__ (among human player, computer or random).
 



#Exercise 13
Add __a sound for each shot__: a normal sound played after every shot, followed by a second sound depending on the result (one for a successful hit, and another one for a missed hit).




#Exercise 14
Add __animations__ in the grid cells, following the player’s shot: one for a successful shot and one for a missed one. 




#Exercise 15
Implement __multiple difficulty levels__ for the AI (for example: _easy_ could be random shots, and _hard_ could be a real algorithm).




#Exercise 16
Implement the possibility to have a __hint__ (if the player desire it, the computer must suggest a cell to play).
 



#Exercise 17
Implement a __multiplayer__ version (a network one, not local).

#warn(For this exercise\, and only for this exercise\, the _socket.io_ library is allowed.)