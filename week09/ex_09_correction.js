window.onload = function(){
	var canvas = document.getElementsByTagName("canvas")[0]; // Canvas that will be dragged
	var dragged = false; // Boolean to know if we're dragging or not
	
	canvas.style.position = "absolute";
	var parentNode = canvas.parentNode.getBoundingClientRect(); // Recover the position of the parent node in the PAGE
		
	// Variable containing the positions of the parent node for an absolute positioning	
	// In our case "Absolute" position are calculated from the top left corner of the wrapper div
	var parentPositions = (function(){
		var computedStyleElement = window.getComputedStyle(canvas);		
		var parentStyle = window.getComputedStyle(canvas.parentNode);
		var marginElement = {
			left: parseFloat(computedStyleElement.marginLeft),
			top: parseFloat(computedStyleElement.marginTop) // Should be 0
		};

		var ret = {
			minimum: {
				x: canvas.parentNode.offsetLeft - marginElement.left, // Our canvas has a margin on the left so we need to substract it for our minimum X
				y: canvas.parentNode.offsetTop - marginElement.top
			},
			
			maximum: {
				x: 0,
				y: 0
			}			
		};
		// Remember that we are always placing the top left point of our canvas, but here we are calculating its bottom right corner
		// That's why we have to substract its width and height too.
		ret.maximum.x = ret.minimum.x + parseInt(parentStyle.width) - parseInt(computedStyleElement.width); 
		ret.maximum.y = ret.minimum.y + parseInt(parentStyle.height) - (parseInt(computedStyleElement.height));
		return ret;
	})();
	
	// Variable used to store the position of the mouse inside the canvas when it is clicked
	var eventOffset;
	// Function handling the mouse movements when we are dragging our canvas
	// Note that declaring here instead of inside the ".mousedown" event (l.58) is actually an optimization
	// I will let you think about why it's an optimization
	function handleMouseMove(e){			
			// event offsetX and Y are substracted so we move the canvas from the point clicked and not the top left one
			// event.offsetX and Y are basically the click position inside the canvas
			var newPos = {
				//x: parentPositions.minimum.x + e.pageX - parentNode.left, // Try to uncomment those 2 lines and comments the 2 next ones ;)
				//y: parentPositions.minimum.y + e.pageY - parentNode.top 
				x: parentPositions.minimum.x + e.pageX - parentNode.left - eventOffset.x,
				y: parentPositions.minimum.y + e.pageY - parentNode.top - eventOffset.y 
			};
			if (newPos.x < parentPositions.minimum.x) newPos.x = parentPositions.minimum.x;
			if (newPos.y < parentPositions.minimum.y) newPos.y = parentPositions.minimum.y;
			if (newPos.x > parentPositions.maximum.x) newPos.x = parentPositions.maximum.x;
			if (newPos.y > parentPositions.maximum.y) newPos.y = parentPositions.maximum.y;
			canvas.style.left = newPos.x + "px"; // Don't forget to add the unit or the CSS property will not work
			canvas.style.top = newPos.y + "px";			
	}

	canvas.onmousedown = function(event){
		dragged = true; // We clicked on the canvas, so we activate the drag
		eventOffset = {x: event.offsetX, y: event.offsetY}; // we store the actual position of the mouse inside the canvas to use it later inside "handleMouseMove"		
		document.onmousemove = handleMouseMove; // set event handler
	};
	
	document.onmouseup = function(){
		dragged = false; // mouse button has been release, so we stop dragging
		document.onmousemove = null; // unset event handler
	};
};