---
module:			Cap Gemini
title:			day 06
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d06
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process and you WILL lose points if your code isn't up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn n your main function unless it has been explicitly asked for.)

#warn(From this day forward\, you are allowed to use the __malloc__ function. 
You are only allowed to use the __printf__ function when it is explicitly asked for. 
The other functions are prohibited.)

#hint(Today we will not be giving you the functions' prototypes.
You will have to read the subject carefully in order to find the function prototype by yourself.  
In today's exercises\, you will also need to write: 
```c
	\#include <stdlib.h>
```
at the top of some files. Today's videos talked about this briefly and you will see more about it tomorrow.)





#newpage


#Exercise 01 – my_strcpy #hfill 3pts

**Turn in**: pool_c_d06/ex_01/ex_01.c#br

Write a __my_strcpy__ function that takes two pointers towards a char as parameter. The first one is the destination and the second one is the source. 
Your function must copy the source into the destination, including the terminating `\\0`. 
The function must return a pointer to char, which is the destination parameter.

#hint(The parameter used as destination will always be large enough to copy the source into it in our tests. You don't have to handle this case in your code.)





#Exercise 02 – my_show_address #hfill 2pts

**Turn in**: pool_c_d06/ex_02/ex_02.c
**Function allowed for this exercise**: printf#br

Write a __my_show_address__ function that takes a number's pointer as parameter and returns nothing. 
Using __printf__, your function must display the variable's memory address in hexadecimal, followed by a newline.





#newpage

#Interlude 1

This is not an exercise that you need to turn in, just something that you need to do and understand.
If you do not understand the result, call an assistant; you need to understand this.#br

Before doing the exercise, read the subject carefully and think about the result before testing it.
Go back to your "my_swap" exercise from a few days ago.
Use your newly-created "my_show_address" function to display both address' variables before calling the function. Why is the result like this?#br

After doing that, create a new function that takes an integer as parameter. This function must only print the parameter's address by using your  "my_show_address" function. 
You must call it by giving an integer that you must have created beforehand. 
Before and after calling the function, you must also display the integer's address, which you will pass as parameter.#br

Once again think about the results before testing it. 
Will the displayed address be the same before and after? 
Will it be the same inside and outside the function? Why?

Finally, go back to your "my_swap", but display both pointers' addresses inside the function (yes, it is different from the first time; if you don't understand why, call an assistant...).





#Exercise 03 – my_up #hfill 4pts

**Turn in**: pool_c_d06/ex_03/ex_03.c#br

Write a "my_up"  function that takes an integer as parameter and returns an allocated two-cell array.
The first cell must contain the number given as parameter, and the second one must contain twice this number.





#Interlude 2

As in the previous interlude, you do not need to turn in anything for this exercise.
You must go back to the "my_up" function that you have just written and print (then return) the address of your newly allocated array inside the function, thanks to your "my_show_address" function.
Once more, think before testing it. 
Will it have the same addresses? Why?





#Exercise 04 – my_strdup #hfill 2pts

**Turn in**: pool_c_d06/ex_04/ex_04.c#br

Write a "my_strdup" function that takes a string in parameter and returns a copy of the string in a newly-allocated memory space.




#newpage

#Exercise 05 – my_show_str #hfill 2pts

**Turn in**: pool_c_d06/ex_05/ex_05.c#br

Write a "my_show_str" function that takes a double array of chars as parameter and returns nothing. 
Your function must display each string of chars contained in the array, followed by a newline. 
The array will be terminated by a null value.

#terminal(cat main.c

	\#include <stdlib.h>

	void 		my_show_str(char **);
	int 		main()
	{
		char 	*tab[] = {"Hello"\,	"Students"\, NULL};
		my_show_str(tab);
		return (0);
	}

#prompt cc *.c -o exo05
#prompt ./exo05
Hello
Students)




#newpage

#Exercise 06 – my_concat_str #hfill 3pts

**Turn in**: pool_c_d06/ex_06/ex_06.c#br

Write a "my_concat_str" function that takes a double array of char as parameter and returns a allocated string of chars terminated by a `\\0` and contains each word of the array concatenated all together. The array must be terminated by a null value.

#terminal(cat main.c

	\#include <stdlib.h>

	void 		my_putstr(char *);
	void 		my_concat_str(char **);
	int 		main()
	{
		char 	*tab[] = {"Hello"\,	"Students"\, NULL};
		char 	*str;
		str = my_concat_str(tab);
		my_putstr(str)
		return (0);
	}

#prompt cc *.c -o exo06
#prompt ./exo06
HelloStudents)







#Exercise 07 – my_reset_ptr #hfill 4pts

**Turn in**: pool_c_d06/ex_07/ex_07.c#br

Write a function "my_reset_ptr" taking a pointer to a pointer of char as parameter. This function must not return anything and but must free the pointer pointed by the parameter. 
After freeing it you must set this pointer to NULL.