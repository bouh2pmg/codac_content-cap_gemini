---
module:			Cap Gemini
title:			day 07
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d07
repoRights:		gecko
language:		C

noBonusDir:		true
noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
Read the norm documentation carefully.
You should type "alt+i" instead of "tab".)

#warn(Do not turn in your main function unless it has been explicitly asked for.)

<!-- 
#warn(From this day forward\, you should turn in a Makefile for each exercise that asks you to turn in a software with a main. 
Your Makefile must have the following rules: all\, re\, clean and fclean.) -->




#newpage

#Exercise 01 – my_show_args #hfill 4pts
**Turn in**: pool_c_d07/ex_01/
**Allowed function**: printf#br

Write a __my_show_args__ software that displays the input parameters, followed by a newline, on the standard output  
If no parameter is given, only print a newline.

#terminal(cc *.c -o my_show_args
#prompt ./my_show_args
#prompt ./my_show_args geckos can Help
geckos
can
Help)





#Exercise 02 – my_hello #hfill 4pts 

**Turn in**: pool_c_d07/ex_02/
**Allowed function**: atoi, printf#br

Write a __my_hello__ software that takes a number in parameter.
This function must display "Hello", followed by a newline as many times as the number passed in parameter.
If nothing is given, if the number given is negative or null, or if the given parameter is not a number, you should print "Error.", followed by a newline.

#terminal(cc *.c -o my_hello
#prompt ./my_hello 5
Hello
Hello
Hello
Hello
Hello
#prompt ./my_hello
Error.
#prompt ./my_hello a
Error.)





#newpage

#Exercise 03 – my_write_file #hfill 4pts 

**Turn in**: pool_c_d07/ex_03/
**Allowed function**: read, write, open, malloc, free#br

Write a __my_write_file__ software that takes multiple strings as parameters.
The first argument is a file path. You must open it in write mode and erase its contents. 
Then, write every other parameter in this file, each followed by a newline.
If the file does not exist, it must be created.

#warn(You must close the file at the end of the operation.)

If an error occurs, display "Error.", followed by a newline on the standard output, and do not create the file. 
If no line to write is given, then it is an error.

#terminal(cc *.c -o my_write_file
#prompt ./my_write_file gecko.txt hello my friends
#prompt cat gecko.txt
hello
my
friends
#prompt ./my_write_file gecko.txt
Error.)





#newpage

#Exercise 04 – my_read_file #hfill 4pts 

**Turn in**: pool_c_d07/ex_04/ex_04.c
**Allowed function**: read, write, open, malloc, free#br

Write a __my_read_file__ function that takes a file name as parameter and returns a char string. 
It must open the file in read mode and return its contents in a newly-allocated memory space.
If an error occurs, the function must return NULL.#br

It must be prototyped the following way:

	char *my_read_file(const char *file);

#terminal(cc *.c -o my_read_file
#prompt cat gecko.txt
hello
my
friends
#prompt ./my_read_file gecko.txt
hello
my
friends)



#newpage

#Exercise 05 – my_connect_four #hfill 4pts 

**Turn in**: pool_c_d07/ex_05/ex_05.c
**Allowed function**: read, write, open, malloc, free#br

Write a "my\_connect\_four" software that implements the connect four game.#br

This is a two player game. The goal is to align four identical symbols.
The first player plays with 'X' and the second one with 'O'.
An empty case is fill with '-'.
At each turn, you must dislay a game board with 6 rows and 7 columns and ask a player with a prompt to choose in which column he wants to play.
An element then drops on the chosen column and falls at the bottom of it.#br

If an error occurs, ask the player to play again.
Use the following functions:

```c
int 		gecko_read(char *str)
{
	return (read(0, str, 2));
}

void 		show\_game (char **game_array)
{
	int 	counter_y ;
	counter_y = 0;
	printf("| 1 | 2 | 3 | 4 | 5 | 6 | 7 |\\n");
	printf("=============================\\n");
	while (counter_y < 6)
	{
		printf("| %c | %c | %c | %c | %c | %c | %c |\\n",
			game_array[counter_y][0], game_array[counter_y][1], game_array[counter_y][2],
			game_array[counter_y][3], game_array[counter_y][4], game_array[counter_y][5],
			game_array[counter_y][6]);
		counter_y++;
	}
}
```

#hint(You should implement a "my\_check\_winner"\ function that takes the board and the last dropped element as parameters and returns 0 if there is no winner\, 1 if the first player wins and 2 if the second player wins.)

#terminal(cc *.c -o my_connect_four
#prompt ./my_connect_four

	| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
	=============================
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |

	Player \#1's turn : 4
	| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
	=============================
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | X | - | - | - |

	Player \#2's turn : 1
	| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
	=============================
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| O | - | - | X | - | - | - |

	[...]
	| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
	=============================
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | - | - | - | - | - | - |
	| - | O | O | - | - | - | - |
	| - | O | X | O | - | - | - |
	| O | X | X | X | X | - | - |

	Congratulation Player \#1\, you win !
)