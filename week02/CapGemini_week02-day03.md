---
module:			Cap Gemini
title:			day 08
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_d08
repoRights:		gecko
language:		C

noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process\, and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn in your main function unless it has been explicitly asked for.)





#Exercise 01 - structure #hfill 3pts 

**Turn in**: pool_c_d08/ex_01/struct.h
**Allowed function**: -#br

Write a "struct.h" file that is properly protected against multiple inclusions.
It must contain a structure named "s_my_struct" that contains an integer named "id" and a char pointer named "str".





#Exercise 02 - my_init #hfill 3pts 

**Turn in**: pool_c_d08/ex_02/struct.h, pool_c_d08/ex_02/ex_02.c
**Allowed function**: -#br

Write a "my_init" function in a file named "ex_02.c". 
Your function must take an s_my_struct pointer as parameter and use the structure from exercise 1.
You should set the structure's "id" field to 0 and the "str" field to NULL.#br

The function must be prototyped as follows:

	void my_init(t_my_struct *);






#Exercise 03 - my_abs #hfill 4pts 

**Turn in**: pool_c_d08/ex_03/abs.h
**Allowed function**: -#br

Create a "MY_ABS" macro in a file named "abs.h" that is properly protected against multiple inclusions.
Your macro must take a number in parameter and return its absolute value.





#newpage

#Exercise 04 - combination #hfill 5pts 

**Turn in**: pool_c_d08/ex_04/abs.h, pool_c_d08/ex_04/struct.h, pool_c_d08/ex_04/ex_04.c
**Allowed function**: strdup#br

Go back to your files from exercise 04 and modify the "my_init" function. 
It must now take three parameters: the same pointer on structure as before, an integer, and a char pointer.
The "id" field must be initialized with the number given in parameter. Apply your "MY_ABS" macro to this number beforehand.
The "str" field must be a copy of the char pointer in a newly-allocated memory space within your function.#br

The function must be prototyped as follows: 

	void my_init(t_my_struct *, int, const char *);





#Exercise 05 - my_power_it #hfill 2pts
**Turn in**: pool_c_d08/ex_05/ex_05.c
**Allowed function**: -#br

Using an iterative algorithm, write a my_power_it function that takes two integers as parameters, and returns an integer worth the first parameter raised to the power of the second one ($\\ge 0$).#br

The function must be prototyped as follows: 

	int my_power_it(int, int);






#Exercise 06 - my_power_rec #hfill 3pts 

**Turn in**: pool_c_d08/ex_06/ex_06.c
**Allowed function**: -#br

Write the same function as the one in the previous exercise, but using a recursive algorithm.#br

The function must be prototyped as follows: 

	int my_power_rec(int, int);







#Exercise Bonus - 8 Queens #hfill 5pts

**Turn in**: pool_c_d08/bonus/#br

#imageRight(8queens.png, 190px, 10)

Write a "queens" program that displays every possibility of placing 8 queens on a chessboard without them being able to run into each other in a single move.#br



#terminal(cc *.c -o queens
./queens
d8\,g7\,c6\,b4\,e3\,a2\,f1
[...])

In this example, only one solution to the "8 queens" problem is shown.
You need to find and display all of the possibilities in order to successfully complete this exercise.

#hint(There is a line break after each solution of the 8 queens' problem.
The order in which the solutions are displayed is not important.)