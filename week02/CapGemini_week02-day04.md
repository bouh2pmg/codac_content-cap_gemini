---
module:			Cap Gemini
title:			sapin
subtitle:		C Pool
background:		'../background/EpitechCapBackground.png'

repository: 	pool_c_sapin
repoRights:		gecko
language:		C

noErrorMess:	true
author:			Pierre ROBERT
version:		1.1
---

#hint(Complying with  the norm takes time\, but it is good for you.
This way your code will comply with the norm from the first written line to the last.
The norm is taken into account in the grading process\, and you WILL lose points if your code is not up to the norm.
Read the norm documentation carefully.)

#warn(Do not turn in your main function!)
 
#warn(You are only allowed to use the write function to do the following exercise.)




#newpage

#Sapin

Write a function that displays a fir tree on the screen, according to a given size.
Fir trees must be similar to examples shown below.
A size 0 fir tree is empty.#br

**Prototype**:
```c
	void sapin(int size);
```

 #warn(The only file collected by the robot is __sapin.c__.)

Your file will be compiled with our own main function, using the __cc__ binary, as follows:

#terminal(cc *.c -o sapin)

#hint(You will find a binary called __sapin__ on the intranet along with the subject.)





#newpage

#Examples

#terminal(./sapin 1

	   *
	  ***
	 *****
	*******
	   |
#prompt ./sapin 5

	                   *
	                  ***
	                 *****
	                *******
	                 *****
	                *******
	               *********
	              ***********
	             *************
	              ***********
	             *************
	            ***************
	           *****************
	          *******************
	         *********************
	           *****************
	          *******************
	         *********************
	        ***********************
	       *************************
	      ***************************
	     *****************************
	       *************************
	      ***************************
	     *****************************
	    *******************************
	   *********************************
	  ***********************************
	 *************************************
	***************************************
	                 |||||
	                 |||||
	                 |||||
	                 |||||
	                 |||||
)





#newpage

#Bonus

**Turn in**: pool_c_sapin/bonus/ 
**Allowed functions**: write, malloc, free#br

In this bonus you must create a software named __division__ that makes an infinite division (a division with a number that is possibly greater than an Integer).
Your software must take two parameters and print the result of the first parameter's Euclidian division by the second parameter, followed by a newline.

#hint(The tests will always be provided with correct arguments\, you don't have to check them.)

Here is an example:
#terminal(cc *.c -o division
 #prompt ./divison 84 2
42
#prompt ./division 451567164125 5
90313432825)